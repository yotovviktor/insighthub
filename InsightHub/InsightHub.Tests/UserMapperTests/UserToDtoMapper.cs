﻿using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserMappersTests
{
    [TestClass]
    public class UserToDtoMapper
    {
        [TestMethod]
        public void MapperShouldReturnNullWhenUserIsNull()
        {
            //Arrange
            User testUser = null;

            //Act
            var UserDto = testUser.ToDto();
            //Assert
            Assert.IsNull(UserDto);
        }
        [TestMethod]
        public void MapperReturnsAnInstanceOfTypeUserDto()
        {
            //Arrange
            var users = ModelGenerator.ReturnUsers();
            //Act&Assert
            Assert.IsInstanceOfType(users[0].ToDto(), typeof(UserDto));
            
        }
        [TestMethod]
        public void MapperMapsThePropertiesCorrect()
        {
            //Arrange
            var users = ModelGenerator.ReturnUsers();
            var expectedFirstName = users[0].FirstName;
            var expectedLastName = users[0].LastName;
            var expectedId = users[0].Id;
            var expectedPassword = users[0].PasswordHash;
            var expectedEmail = users[0].Email;
            var expectedPhoneNumber = users[0].PhoneNumber;
            
            //Act
            var actual = users[0].ToDto();
            //Assert
            Assert.AreEqual(expectedFirstName, actual.FirstName);
            Assert.AreEqual(expectedLastName, actual.LastName);
            Assert.AreEqual(expectedEmail, actual.Email);
            Assert.AreEqual(expectedPhoneNumber, actual.PhoneNumber);
            Assert.AreEqual(expectedId, actual.Id);
        }

        [TestMethod]
        public void MapperMapsTheCollectionCorect()
        {
            //Arrange
            var collectionOfUsers = ModelGenerator.ReturnUsers();
            var expectedId1 = 1;
            var expectedId2 = 2;
            
            //Act
            var actualCollectionOfDtos = collectionOfUsers.ToDtos().ToArray();
            //Assert
            Assert.AreEqual(expectedId1, actualCollectionOfDtos[0].Id);
            Assert.AreEqual(expectedId2, actualCollectionOfDtos[1].Id);
        }

    }
   
}
