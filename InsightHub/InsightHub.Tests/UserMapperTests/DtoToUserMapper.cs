﻿using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserMapperTests
{
    [TestClass]
    public class DtoToUserMapper_Should
    {
        [TestMethod]
        public void ReturnsNullWhenDtoIsNull()
        {
          
            //Arrange
            UserDto userDto = null;
            
            //Act
            var actual = userDto.ToEntity();
            
            //Assert
            Assert.IsNull(actual);
        }
        [TestMethod]
        public void ReturnsInstanceOfTypeUser()
        {
            //Arrange
            UserDto userDto = new UserDto();
            
            //Act
            var actual = userDto.ToEntity();
            
            //Assert
            Assert.IsInstanceOfType(actual, typeof(User));
        }
        [TestMethod]
        public void MapperMapsPropertiesCorrect()
        {
            //Arrange
            var userDto = ModelGenerator.ReturnUsers().First().ToDto();
            var expectedFIrstName = userDto.FirstName;
            var expectedLastName = userDto.LastName;
            var expectedEmail = userDto.Email;

            //Act
            var actual = userDto.ToEntity();
            
            //Assert
            Assert.AreEqual(actual.LastName, expectedLastName);
            Assert.AreEqual(actual.Email, expectedEmail);
            Assert.AreEqual(actual.FirstName, expectedFIrstName);
        }
        [TestMethod]
        public void MapperMapsCollectionCorect()
        {
            //Arrange
            var userDtos = ModelGenerator.ReturnUsers().ToDtos();

            //Act
            var actual = userDtos.ToEntities().ToArray();

            //Assert
            Assert.AreEqual(actual[0].Id, 1);
            Assert.AreEqual(actual[1].Id, 2);
        }
    }

}
