﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserMapperTests
{
    [TestClass]
    public class UserToDtoBriefMapperTests_Should
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public void MapTheProperties(int id)
        {
            //Arrange
            var users = ModelGenerator.ReturnUsers();
            
            //Act
            var actual = users[id - 1].ToBriefDto();
            var expected = users[id - 1];
            
            //Assert
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.Email, actual.Email);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Status, actual.Status);
        }

        [TestMethod]
        public void ThrowsException_When_EntityIsNull()
        {
            //Arrange
            User user = null;
            
            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => user.ToBriefDto());
            Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
        }

        [TestMethod]
        public void MapsThePropertiesOfACollection()
        {
            //Arrange
            var users = ModelGenerator.ReturnUsers();
            
            //Act
            var actual = users.ToBriefDtos().ToArray();
            var expected = users;
            
            //Assert
            Assert.AreEqual(expected[0].FirstName, actual[0].FirstName);
            Assert.AreEqual(expected[0].LastName, actual[0].LastName);
            Assert.AreEqual(expected[0].Email, actual[0].Email);
            Assert.AreEqual(expected[0].Id, actual[0].Id);
            Assert.AreEqual(expected[0].Status, actual[0].Status);

            Assert.AreEqual(expected[1].FirstName, actual[1].FirstName);
            Assert.AreEqual(expected[1].LastName, actual[1].LastName);
            Assert.AreEqual(expected[1].Email, actual[1].Email);
            Assert.AreEqual(expected[1].Id, actual[1].Id);
            Assert.AreEqual(expected[1].Status, actual[1].Status);
        }

        [TestMethod]
        public void ThrowsException_When_EntityCollectionIsNull()
        {
            //Arrange
            List<User> user = null;
           
            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => user.ToBriefDtos());
            Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
        }

    }
}
