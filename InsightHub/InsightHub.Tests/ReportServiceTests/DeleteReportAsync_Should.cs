﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class DeleteReportAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task DeleteReport_When_ParamsAreValid()
        {
            // Arrange
            var report = reports.First();

            // Act
            using (var actContext = new InsightHubContext(options))
            {
                var sut = new ReportService(actContext, azureService, subscriptionService);
                await sut.DeleteReportAsync(report.Id);
            }

            // Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var result = assertContext.Reports.Find(report.Id);

                Assert.AreEqual(true, result.IsDeleted);
                Assert.IsTrue(DateTime.UtcNow > result.DeletedOn);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteReportAsync(NOT_EXISTING_REPORT_ID));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportIsAlreadyDeleted()
        {
            // Arrange
            var report = reports.First();

            // Act
            using (var actContext = new InsightHubContext(options))
            {
                var sut = new ReportService(actContext, azureService, subscriptionService);
                await sut.DeleteReportAsync(report.Id);
            }

            // Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteReportAsync(report.Id));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
