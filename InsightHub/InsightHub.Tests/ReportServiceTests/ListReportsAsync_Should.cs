﻿using InsightHub.Common.Enums;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using InsightHub.Common.Models;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class ListReportsAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ReturnFeaturedReports_When_Filteded_By_IsFeature()
        {
            // Arrange
            var filter = new FilterCriteria() { IsFeatured = true };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = assertContext.Reports.Where(report => report.IsFeatured == true).ToList();

                Assert.AreEqual(expected.Count(), result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DataRow(Status.Pending)]
        [DataRow(Status.Rejected)]
        [DataRow(Status.Approved)]
        public async Task ReturnFilterReports_by_Status(Status status)
        {
            // Arrange
            var filter = new FilterCriteria() { Status = status };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = assertContext.Reports.Where(report => report.Status == status).ToList();

                Assert.AreEqual(expected.Count, result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DynamicData(nameof(GetUser), DynamicDataSourceType.Method)]
        public async Task ReturnFilteredReports_by_AuthorID(User user)
        {          
            // Arrange
            var filter = new FilterCriteria() { AuthorId = user.Id };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = assertContext.Reports.Where(report => report.AuthorId == user.Id).ToList();

                Assert.AreEqual(expected.Count(), result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        public static IEnumerable<object[]> GetUser()
        {
            var industries = ModelGenerator.ReturnUsers();

            for (int i = 0; i < industries.Count; i++)
            {
                yield return new object[] { ModelGenerator.ReturnUsers().ToList()[i] };
            }
        }

        [TestMethod]
        [DynamicData(nameof(GetIndustry), DynamicDataSourceType.Method)]
        public async Task ReturnFilteredReports_by_IndustryID(Industry industry)
        {
            // Arrange
            var filter = new FilterCriteria() { IndustryId = industry.Id };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = assertContext.Reports.Where(report => report.IndustryId == industry.Id).ToList();

                Assert.AreEqual(expected.Count, result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        public static IEnumerable<object[]> GetIndustry()
        {
            var industries = ModelGenerator.ReturnIndustries();

            for (int i = 0; i < industries.Count; i++)
            {
                yield return new object[] { ModelGenerator.ReturnIndustries().ToList()[i] };
            }
        }

        [TestMethod]
        [DataRow("Blockchain")]
        [DataRow("Internet")]
        [DataRow("Telerik")]
        public async Task ReturnFilteredReports_by_Name(string name)
        {
            // Arrange
            var filter = new FilterCriteria() { Name = name };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = assertContext.Reports.Where(report => report.Name.ToLower()
                                                    .Contains(filter.Name.ToLower())).ToList();
                Assert.AreEqual(expected.Count(), result.Count);

                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DataRow(Order.Asc)]
        [DataRow(Order.Desc)]
        public async Task ReturnReportsSortered_by_Name(Order order)
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(sortBy: ReportSortBy.Name, order: order);

                var expected = reports.OrderBy(report => report.Name).ToList();

                if (order == Order.Desc)
                {
                    expected.Reverse();
                }

                Assert.AreEqual(expected.Count, result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DataRow(Order.Asc)]
        [DataRow(Order.Desc)]
        public async Task ReturnReportsSortered_by_Downloads(Order order)
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(sortBy: ReportSortBy.Downloads, order: order);

                List<Report> expected;
                var query = reports.OrderBy(report => report.DownloadCount);

                if (order == Order.Desc)
                {
                    expected = query.ThenByDescending(report => report.Id).ToList();
                    expected.Reverse();
                }
                else
                {
                    expected = query.ToList();
                }

                Assert.AreEqual(expected.Count, result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DataRow(Order.Asc)]
        [DataRow(Order.Desc)]
        public async Task ReturnReportsSortered_by_UpaloadDate(Order order)
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(sortBy: ReportSortBy.Date, order: order);

                var expected = reports.OrderBy(report => report.CreatedOn).ToList();

                if (order == Order.Desc)
                {
                    expected.Reverse();
                }

                Assert.AreEqual(expected.Count, result.Count);
                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        [DynamicData(nameof(GetTagIds), DynamicDataSourceType.Method)]
        public async Task ReturnFilteredReports_by_Tags(int[] tagIds)
        {
            // Arrange
            var filter = new FilterCriteria() { TagIds = tagIds };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(filter);

                var expected = reports.Where(r => r.Tags.Where(t => filter.TagIds.Contains(t.TagId)).Count() == filter.TagIds.Length).ToList();
                Assert.AreEqual(expected.Count, result.Count);
            }
        }

        public static IEnumerable<object[]> GetTagIds()
        {
            yield return new object[] { new int[] { 1, 3 } };
            yield return new object[] { new int[] { 2 } };
            yield return new object[] { new int[] { int.MaxValue} };
            yield return new object[] { new int[] { int.MinValue } };
        }

        [TestMethod]
        public async Task ReturnPaginatedReports()
        {
            // Arrange
            var paging = new Paging() { Limit = 3, Offset = 2, TotalCount = null };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync(paging: paging);

                var expected = reports.Skip(paging.Offset.Value).Take(paging.Limit.Value).ToList();

                Assert.AreEqual(expected.Count, result.Count);
                Assert.AreEqual(reports.Count, paging.TotalCount);

                for (int i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        public async Task NotReturn_DeletedReports()
        {
            // Arrange
            var report = reports.First();
            report.IsDeleted = true;

            using (var arrangeContext = new InsightHubContext(options))
            {
                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.ListReportsAsync();

                Assert.IsFalse(result.Any(report => report.IsDeleted == true));
            }
        }
    }
}
