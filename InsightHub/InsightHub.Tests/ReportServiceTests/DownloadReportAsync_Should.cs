﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.Enums;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class DownloadReportAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ThrowApiExceptionWhen_ReportNotFound()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DownloadReportAsync(NOT_EXISTING_REPORT_ID, user.Id));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiExceptionWhen_UserNotFound()
        {
            // Arrange
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_USER_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DownloadReportAsync(report.Id, NOT_EXISTING_USER_ID));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }

        [TestMethod]
        [DataRow(Status.Pending)]
        [DataRow(Status.Rejected)]
        public async Task ThrowApiExceptionWhen_UserIsNotApproved(Status status)
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().Last();
            user.Status = status;

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(user);
                await arrangeContext.SaveChangesAsync();
            }

            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DownloadReportAsync(report.Id, user.Id));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ReturnReportWithContent_When_ParamsAreValid()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var result = await sut.DownloadReportAsync(report.Id, user.Id);

                Assert.AreEqual(report.Name, result.Name);
                Assert.AreEqual(report.Summary, result.Summary);
                Assert.AreEqual(report.Status, result.Status);
                Assert.AreEqual(report.AuthorId, result.AuthorId);
                Assert.AreEqual(report.IndustryId, result.IndustryId);

                Assert.AreEqual(content.Data, result.Content);
                Assert.AreEqual(content.ContentType, result.ContentType);
            }
        }
    }
}
