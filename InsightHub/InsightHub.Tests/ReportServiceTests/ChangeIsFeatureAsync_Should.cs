﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class ChangeIsFeaturedAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeIsFeaturedAsync(NOT_EXISTING_REPORT_ID, true));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportHasAlreadySameState()
        {
            // Arrange
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeIsFeaturedAsync(report.Id, report.IsFeatured));

                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ChangeTheState_When_ParamsAreValid()
        {
            // Arrange
            var report = reports.First();

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.IsFeatured = false;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.ChangeIsFeaturedAsync(report.Id, true);

                var result = await assertContext.Reports.FindAsync(report.Id);
                Assert.AreEqual(true, result.IsFeatured);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportIsDeleted()
        {
            // Arrange
            var report = reports.First();

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.IsDeleted = true;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeIsFeaturedAsync(report.Id, true));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
