﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.Enums;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class ChangeReportStatusAsync_Should : ReportBaseTest
    {

        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Arrange
            var status = Status.Pending;

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeReportStatusAsync(NOT_EXISTING_REPORT_ID, status));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportIsDeleted()
        {
            // Arrange
            var report = reports.First();
            var status = Status.Approved;

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.IsDeleted = true;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeReportStatusAsync(report.Id, status));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_StatusIsTheSameAsOldOne()
        {
            // Arrange
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ChangeReportStatusAsync(report.Id, report.Status));

                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ChangeTheStatus_When_ParamsAreValid()
        {
            // Arrange
            var report = reports.First();
            var status = Status.Rejected;

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.Status = Status.Pending;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.ChangeReportStatusAsync(report.Id, status);

                var result = await assertContext.Reports.FindAsync(report.Id);
                Assert.AreEqual(status, result.Status);
            }
        }

        [TestMethod]
        public async Task CallSubscriptionService_When_StatusIsApproved()
        {
            // Arrange
            var report = reports.First();
            var status = Status.Approved;

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.Status = Status.Pending;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.ChangeReportStatusAsync(report.Id, status);

                subscriptionMock.Verify(x => x.NotifySubscribers(It.IsAny<int>(), It.IsAny<int>()), Times.Once());
            }
        }
    }
}
