﻿using InsightHub.Common.Models;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.Contracts;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests.Abstract
{
    [TestClass]
    public abstract class ReportBaseTest
    {
        protected DbContextOptions<InsightHubContext> options;
        protected ISubscriptionService subscriptionService;
        protected Mock<ISubscriptionService> subscriptionMock = new Mock<ISubscriptionService>();
        protected IContentService azureService;
        protected IContent content;
        protected string contentPath = "test.pdf";
        protected List<Report> reports = ModelGenerator.ReturnReports();

        public ReportBaseTest()
        {          
            var azureContentMock = new Mock<IContentService>();
            azureContentMock.Setup(x => x.UploadContentAsync(It.IsAny<IContent>())).Returns(Task.FromResult(contentPath));

            content = new Content("plain/text", new MemoryStream());
            azureContentMock.Setup(x => x.DownloadContentAsync(It.IsAny<string>())).Returns(Task.FromResult(content));

            azureContentMock.Setup(x => x.DeleteContentAsync(It.IsAny<string>())).Returns(Task.CompletedTask);
            azureService = azureContentMock.Object;

            subscriptionMock.Setup(x => x.NotifySubscribers(It.IsAny<int>(), It.IsAny<int>())).Returns(Task.CompletedTask);
            subscriptionService = subscriptionMock.Object;
        }

        [TestInitialize()]
        public async Task Initialize()
        {
            // Arrange
            options = Options.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(ModelGenerator.ReturnUsers().First());
                await arrangeContext.Industries.AddRangeAsync(ModelGenerator.ReturnIndustries());
                await arrangeContext.Tags.AddRangeAsync(ModelGenerator.ReturnTags());
                await arrangeContext.Reports.AddRangeAsync(reports);
                await arrangeContext.ReportTags.AddRangeAsync(ModelGenerator.ReturnReportTags());

                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestCleanup()]
        public async Task Cleanup()
        {
            var context = new InsightHubContext(options);
            await context.Database.EnsureDeletedAsync();
        }
    }
}
