﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class ReportServiceConstructor_Should : ReportBaseTest
    {
        [TestMethod]
        public void CreateInstance()
        {          
            using (var assertContext = new InsightHubContext(options))
            {
                // Act 
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                // Assert
                Assert.IsNotNull(sut);
            }
        }

        [TestMethod]
        public void ThrowApiException_When_ContextIsNull()
        {            
            using (var assertContext = new InsightHubContext(options))
            {
                // Act & Assert
                var ex = Assert.ThrowsException<ApiException>(() => new ReportService(null, azureService, subscriptionService));
                Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
            }
        }


        [TestMethod]
        public void ThrowApiException_When_AzureServiceIsNull()
        {
            using (var assertContext = new InsightHubContext(options))
            {
                // Act & Assert
                var ex = Assert.ThrowsException<ApiException>(() => new ReportService(assertContext, null, subscriptionService));
                Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
            }
        }


        [TestMethod]
        public void ThrowApiException_When_SubscriptionServiceIsNull()
        {
            using (var assertContext = new InsightHubContext(options))
            {
                // Act & Assert
                var ex = Assert.ThrowsException<ApiException>(() => new ReportService(assertContext, azureService, null));
                Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
            }
        }
    }
}
