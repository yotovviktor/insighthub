﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class GetReportAsync_Should : ReportBaseTest
    {
        [TestMethod]
        [DynamicData(nameof(GetReport), DynamicDataSourceType.Method)]
        public async Task ReturnCorrectReport_When_ParamsAreValid(Report report)
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.GetReportAsync(report.Id);

                Assert.AreEqual(report.Name, result.Name);
                Assert.AreEqual(report.Summary, result.Summary);
                Assert.AreEqual(report.Status, result.Status);
                Assert.AreEqual(report.IsDeleted, result.IsDeleted);
                Assert.AreEqual(report.IsFeatured, result.IsFeatured);
                Assert.AreEqual(report.AuthorId, result.AuthorId);
                Assert.AreEqual(report.IndustryId, result.IndustryId);
            }
        }

        public static IEnumerable<object[]> GetReport()
        {
            yield return new object[] { ModelGenerator.ReturnReports().First() };
            yield return new object[] { ModelGenerator.ReturnReports().Last() };
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.GetReportAsync(NOT_EXISTING_REPORT_ID));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
