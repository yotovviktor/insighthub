﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class ListUserDownloadsAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ReturnCollectionofUserDownloads_When_ParamsAreValid()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();
            var report1 = reports.First();
            var report2 = reports.Last();

            var downloadedReports = new List<DownloadedReports>()
            {
                 new DownloadedReports { UserId = user.Id, ReportId = report1.Id },
                 new DownloadedReports { UserId = user.Id, ReportId = report2.Id }
            };           

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.DownloadedReports.AddRangeAsync(downloadedReports);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = (await sut.ListUserDownloadsAsync(user.Id)).ToList();

                Assert.AreEqual(downloadedReports.Count, result.Count);

                for (int i = 0; i < result.Count; i++)
                {
                    Assert.AreEqual(downloadedReports[i].ReportId, result[i].Id);
                }            
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_UserDidntDownloadAny()
        {
            // Arrange
            var user1 = ModelGenerator.ReturnUsers().First();
            var user2 = ModelGenerator.ReturnUsers().Last ();

            var report1 = reports.First();
            var report2 = reports.Last();

            var downloadedReports = new List<DownloadedReports>()
            {
                 new DownloadedReports { UserId = user2.Id, ReportId = report1.Id },
                 new DownloadedReports { UserId = user2.Id, ReportId = report2.Id }
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.DownloadedReports.AddRangeAsync(downloadedReports);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = (await sut.ListUserDownloadsAsync(user1.Id)).ToList();

                Assert.AreEqual(0, result.Count);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_UserNotFound()
        {          
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_USER_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.ListUserDownloadsAsync(NOT_EXISTING_USER_ID));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
