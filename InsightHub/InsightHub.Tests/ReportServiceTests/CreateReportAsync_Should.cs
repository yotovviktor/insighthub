﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.Enums;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class CreateReportAsync_Should : ReportBaseTest
    {
        [TestMethod]
        [DataRow(Status.Pending)]
        [DataRow(Status.Rejected)]
        public async Task ThrowApiException_When_UserIsNotApproved(Status status)
        {
            // Arrange
            var author = ModelGenerator.ReturnUsers().Last();
            var industry = ModelGenerator.ReturnIndustries().First();
            author.Status = status;

            using (var arrangeContext = new InsightHubContext(options))
            {
                arrangeContext.Users.Add(author);
                await arrangeContext.SaveChangesAsync();
            }

            var reportDto = new ReportDto
            {
                Name = "Report",
                Summary = "Summary",
                AuthorId = author.Id,
                IndustryId = industry.Id,
                Content = new MemoryStream(),
                ContentType = "plain/text",
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }
        
        [TestMethod]
        public async Task ThrowApiException_When_UserNotFound()
        {
            // Arrange
            const int NOT_EXISTING_USER_ID = int.MaxValue;
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Name = "Report",
                Summary = "Summary",
                AuthorId = NOT_EXISTING_USER_ID,
                IndustryId = industry.Id,
                Content = new MemoryStream(),
                ContentType = "plain/text",
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_PassedDtoIsNull()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateReportAsync(null));
                Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_IndustryNotFound()
        {
            // Arrange
            const int NOT_EXISTING_INDUSTRY_ID = int.MaxValue;
            var author = ModelGenerator.ReturnUsers().First();

            var reportDto = new ReportDto
            {
                Name = "Report",
                Summary = "Summary",
                AuthorId = author.Id,
                IndustryId = NOT_EXISTING_INDUSTRY_ID,
                Content = new MemoryStream(),
                ContentType = "plain/text",
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_TagsNotExist()
        {
            // Arrange
            const int NOT_EXISTING_TAG_ID = int.MaxValue;
            var author = ModelGenerator.ReturnUsers().First();
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Name = "Report",
                Summary = "Summary",
                AuthorId = author.Id,
                IndustryId = industry.Id,
                Content = new MemoryStream(),
                ContentType = "plain/text",
                TagIds = new List<int> { NOT_EXISTING_TAG_ID }
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task CreateReport_When_ParamsAreValid()
        {
            // Arrange
            var author = ModelGenerator.ReturnUsers().First();
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Name = "Report",
                Summary = "Summary",
                AuthorId = author.Id,
                IndustryId = industry.Id,
                Content = new MemoryStream(),
                ContentType = "plain/text",
            };          

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var result = await sut.CreateReportAsync(reportDto);

                Assert.AreEqual(reportDto.Name, result.Name);
                Assert.AreEqual(reportDto.Summary, result.Summary);
                Assert.AreEqual(reportDto.Status, result.Status);
                Assert.AreEqual(reportDto.AuthorId, result.AuthorId);
                Assert.AreEqual(reportDto.IndustryId, result.IndustryId);

                var createdReport = await assertContext.Reports.FindAsync(result.Id);

                Assert.AreEqual(createdReport.Name, result.Name);
                Assert.AreEqual(createdReport.Summary, result.Summary);
                Assert.AreEqual(createdReport.Status, result.Status);
                Assert.AreEqual(createdReport.AuthorId, result.AuthorId);
                Assert.AreEqual(createdReport.IndustryId, result.IndustryId);
                Assert.AreEqual(createdReport.Status, Status.Pending);
                Assert.IsTrue(DateTime.UtcNow > result.CreatedOn);

                Assert.AreEqual(createdReport.BlobPath, contentPath);
            }
        }
    }
}
