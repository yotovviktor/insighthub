﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.Enums;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class MarkReportAsDownloadedAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_REPORT_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.MarkReportAsDownloadedAsync(NOT_EXISTING_REPORT_ID, user.Id));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_UserNotFound()
        {
            // Arrange
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                const int NOT_EXISTING_USER_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.MarkReportAsDownloadedAsync(report.Id, NOT_EXISTING_USER_ID));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }

        [TestMethod]
        [DataRow(Status.Pending)]
        [DataRow(Status.Rejected)]
        public async Task ThrowApiException_When_UserIsNotApproved(Status status)
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().Last();
            user.Status = status;

            using (var arrangeContext = new InsightHubContext(options))
            {
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
            }

            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.MarkReportAsDownloadedAsync(report.Id, user.Id));
                Assert.AreEqual(HttpStatusCode.Unauthorized, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task IncreaseReportDownloadsCount()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                await sut.MarkReportAsDownloadedAsync(report.Id, user.Id);

                var downloadedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreEqual(report.DownloadCount + 1, downloadedReport.DownloadCount);
            }
        }

        [TestMethod]
        public async Task AddTheReportToUserDownloads()
        {
            // Arrange
            var user = ModelGenerator.ReturnUsers().First();
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.MarkReportAsDownloadedAsync(report.Id, user.Id);

                bool result = assertContext.DownloadedReports.Any(dr => dr.ReportId == report.Id && dr.UserId == user.Id);

                Assert.IsTrue(result);
            }
        }
    }
}
