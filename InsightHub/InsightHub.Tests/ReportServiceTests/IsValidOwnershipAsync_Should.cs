﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class IsValidOwnershipAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ReturnTrue_When_UserIsTheAuthor()
        {
            // Arrange
            var report = reports.First();
            var user = ModelGenerator.ReturnUsers().First();

            report.AuthorId = user.Id;

            using (var arrangeContext = new InsightHubContext(options))
            {
                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.IsValidOwnershipAsync(user.Id, report.Id);

                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_UserIsNotTheAuthor()
        {
            // Arrange
            var report = reports.First();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                var result = await sut.IsValidOwnershipAsync(int.MaxValue, report.Id);

                Assert.IsFalse(result);
            }
        }
    }
}
