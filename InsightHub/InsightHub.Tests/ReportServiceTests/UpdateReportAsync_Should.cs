﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.ReportServices;
using InsightHub.Tests.ReportServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Data.EntityModels;
using System.Collections.Generic;
using InsightHub.Data.Enums;
using System;

namespace InsightHub.Tests.ReportServiceTests
{
    [TestClass]
    public class UpdateReportAsync_Should : ReportBaseTest
    {
        [TestMethod]
        public async Task ThrowApiExeption_When_PassedDtoIsNull()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateReportAsync(null));
                Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_ReportNotFound()
        {
            // Arrange            
            const int NOT_EXISTING_REPORT_ID = int.MaxValue;
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Id = NOT_EXISTING_REPORT_ID,
                Name = "Report",
                Summary = "Summary",
                IndustryId = industry.Id,
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task KeepThePath_When_ContentIsNotChanged()
        {
            // Arrange
            var report = reports.First();
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                Name = "EditedReport",
                Summary = "EditedSummary",
                IndustryId = industry.Id
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreEqual(report.BlobPath, updatedReport.BlobPath);
            }
        }

        [TestMethod]
        public async Task UpdateReport_When_ParamsAreValid()
        {
            // Arrange
            var report = reports.First();
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                Name = "EditedReport",
                Summary = "EditedSummary",
                IndustryId = industry.Id,
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreEqual(reportDto.Name, updatedReport.Name);
                Assert.AreEqual(reportDto.Summary, updatedReport.Summary);
                Assert.AreEqual(reportDto.IndustryId, updatedReport.IndustryId);
            }
        }

        [TestMethod]
        public async Task NotChangeTheAuthor()
        {
            // Arrange
            var report = reports.First();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                AuthorId = int.MaxValue,
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreNotEqual(reportDto.AuthorId, updatedReport.AuthorId);
            }
        }

        [TestMethod]
        public async Task NotChangeDownloadsCount()
        {
            // Arrange
            var report = reports.First();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                DownloadCount = int.MaxValue
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreNotEqual(reportDto.DownloadCount, updatedReport.DownloadCount);
            }
        }

        [TestMethod]
        public async Task SetStatusToPendingAgain()
        {
            // Arrange
            var report = reports.First();
            var industry = ModelGenerator.ReturnIndustries().First();

            using (var arrangeContext = new InsightHubContext(options))
            {
                report.Status = Status.Approved;

                arrangeContext.Reports.Update(report);
                await arrangeContext.SaveChangesAsync();
            }

            var reportDto = new ReportDto
            {
                Id = report.Id,
                Name = "EditedReport",
                Summary = "EditedSummary",
                IndustryId = industry.Id
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.AreEqual(Status.Pending, updatedReport.Status);
            }
        }

        [TestMethod]
        public async Task ChangeModifiedOnTime()
        {
            // Arrange
            var report = reports.First();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                Name = "EditedReport",
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);

                Assert.IsTrue(DateTime.UtcNow > updatedReport.ModifiedOn);
            }
        }

        [TestMethod]
        public async Task AddTagsCorrectly()
        {
            // Arrange
            var report = reports.First();

            var newTag = new Tag { Id = ModelGenerator.ReturnTags().Last().Id, Name = "Transform" };
                       
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(newTag);
            }          

            var reportDto = new ReportDto
            {
                Id = report.Id,
                TagIds = new List<int>() { newTag.Id }
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);
                Assert.AreEqual(reportDto.TagIds.Count, updatedReport.Tags.Count);
            }
        }

        [TestMethod]
        public async Task RemoveTagsCorrectly()
        {
            // Arrange
            var report = reports.Last();
            var tags = ModelGenerator.ReturnTags();

            var newTag = new Tag { Id = int.MaxValue, Name = "Transform" };

            //to be ensured that the report has tags
            var reportTags = new List<ReportTags>()
            {
                new ReportTags {ReportId = report.Id, TagId = tags.First().Id},
                new ReportTags {ReportId = report.Id, TagId = tags.Last().Id},
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(newTag);
                await arrangeContext.AddRangeAsync(reportTags);
                await arrangeContext.SaveChangesAsync();
            }

            reportTags.Add(new ReportTags { ReportId = report.Id, TagId = int.MaxValue });

            var reportDto = new ReportDto
            {
                Id = report.Id,
                TagIds = new List<int>() { newTag.Id }
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);
                Assert.AreEqual(reportDto.TagIds.Count, updatedReport.Tags.Count);
            }
        }

        [TestMethod]
        public async Task NotChangeTheTags_When_TagIdsIsNull()
        {
            // Arrange
            var report = reports.First();

            var reportsCount = report.Tags.Count();

            var reportDto = new ReportDto
            {
                Id = report.Id,
                TagIds = null
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);
                await sut.UpdateReportAsync(reportDto);

                var updatedReport = await assertContext.Reports.FindAsync(report.Id);
                Assert.AreEqual(reportsCount, updatedReport.Tags.Count);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_IndustryNotFound()
        {
            // Arrange
            const int NOT_EXISTING_INDUSTRY_ID = int.MaxValue;
            var author = ModelGenerator.ReturnUsers().First();

            var reportDto = new ReportDto
            {
                IndustryId = NOT_EXISTING_INDUSTRY_ID
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_TagsNotExist()
        {
            // Arrange
            const int NOT_EXISTING_TAG_ID = int.MaxValue;

            var reportDto = new ReportDto
            {                
                TagIds = new List<int> { NOT_EXISTING_TAG_ID }
            };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, azureService, subscriptionService);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateReportAsync(reportDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
