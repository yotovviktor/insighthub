﻿using InsightHub.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace InsightHub.Tests.Utils
{
    public class Options
    {
        public static DbContextOptions<InsightHubContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<InsightHubContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
