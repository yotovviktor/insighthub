﻿using System;
using System.Collections.Generic;
using InsightHub.Data.Enums;
using InsightHub.Data.EntityModels;

namespace InsightHub.Tests.Utils
{
    public static class ModelGenerator
    {
        public static List<User> ReturnUsers()
        {
            List<User> users = new List<User>();
            users.AddRange(new List<User>{
            new User
            {
                 Id = 1,
                 FirstName = "Viktor",
                 LastName = "Yotov",
                 Email = "yotovviktor@gmail.com",
                 PhoneNumber = "0898484555",
                 PasswordHash = "Viktor@123",
                 Status = Status.Approved,
                 RegisteredOn = DateTime.UtcNow,
                 UserName = "yotovviktor",
                 Role = Data.Enums.Role.admin
            },
            new User
            {
                Id = 2,
                FirstName = "Greta",
                LastName = "Pavlova",
                Email = "greta.pavlova@gmail.com",
                PhoneNumber = "0898484542",
                PasswordHash = "Greta@123",
                Status = Status.Approved,
                RegisteredOn = DateTime.UtcNow,
                UserName = "greta.pavlova",
                Role = Data.Enums.Role.customer

            },
            new User
            {
                Id = 3,
                FirstName = "Tor",
                LastName = "TheGod",
                Email = "Tor.God@avengers.international",
                PhoneNumber = "0898484542",
                PasswordHash = "TorGod@123",
                Status = Status.Approved,
                RegisteredOn = DateTime.UtcNow,
                UserName = "TorGod",
                Role = Data.Enums.Role.author

            }
            });
            return users;
        }

        public static List<Report> ReturnReports()
        {
            List<Report> reports = new List<Report>();
            reports.AddRange(new List<Report>
            {
                new Report
                {
                    Id = 1,
                    Name = "Never Underestimate The Influence Of Internet.",
                    Summary = "Never Underestimate The Influence Of Internet.",
                    AuthorId = 1,
                    IndustryId = 1,
                    Status = Status.Approved,
                    BlobPath = "61978b1f-c77c-4e2e-9098-f7a569090662.pdf",
                    DownloadCount = 88,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 2,
                    Name = "The Modern Rules Of Internet",
                    Summary = "The Modern Rules Of Internet",
                    AuthorId = 2,
                    IndustryId = 2,
                    Status = Status.Approved,
                    BlobPath = "763c5818-efa7-4fe1-8802-487881accd1c.pdf",
                    DownloadCount = 43,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 3,
                    Name = "Small But Important Things To Observe In Internet.",
                    Summary = "Small But Important Things To Observe In Internet.",
                    AuthorId = 3,
                    IndustryId = 3,
                    Status = Status.Rejected,
                    BlobPath = "07812b9b-042e-4a7a-9e1b-c3c38aa4a845.pdf",
                    DownloadCount = 0,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 4,
                    Name = "This Is How Internet Will Look Like In 10 Years Time.",
                    Summary = "This Is How Internet Will Look Like In 10 Years Time.",
                    AuthorId = 1,
                    IndustryId = 1,
                    Status = Status.Pending,
                    BlobPath = "6e63bff9-4230-4341-9df6-6a6835230e86.pdf",
                    DownloadCount = 0,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 5,
                    Name = "Seven Common Myths About Internet.",
                    Summary = "Seven Common Myths About Internet.",
                    AuthorId = 2,
                    IndustryId = 5,
                    Status = Status.Approved,
                    BlobPath = "650c1cb1-a1c1-462a-ae8b-7ef1b68cd702.pdf",
                    DownloadCount = 3,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 6,
                    Name = "The Modern Rules Of Internet",
                    Summary = "The Modern Rules Of Internet",
                    AuthorId = 3,
                    IndustryId = 2,
                    Status = Status.Approved,
                    BlobPath = "5d521bff-b7f0-49a6-a165-387e3a5faece.pdf",
                    DownloadCount = 7,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 7,
                    Name = "Use This 3-Phase Framework to Navigate the COVID-19 Downturn",
                    Summary = "Use This 3-Phase Framework to Navigate the COVID-19 Downturn",
                    AuthorId = 1,
                    IndustryId = 2,
                    Status = Status.Approved,
                    BlobPath = "59e3eeb3-9630-4e71-957d-51719b65f3bb.pdf",
                    DownloadCount = 5,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 8,
                    Name = "Blockchain: What’s Ahead?",
                    Summary = "Blockchain: What’s Ahead?",
                    AuthorId = 2,
                    IndustryId = 5,
                    Status = Status.Pending,
                    BlobPath = "5391ce95-475e-48d8-ba66-1dfe321efe5a.pdf",
                    DownloadCount = 0,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 9,
                    Name = "The CIO’s Executive Communication Guide",
                    Summary = "The CIO’s Executive Communication Guide",
                    AuthorId = 3,
                    IndustryId = 4,
                    Status = Status.Approved,
                    BlobPath = "7cbd1b08-9be2-4a1e-a684-2602c5a3d6f7.pdf",
                    DownloadCount = 123,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 10,
                    Name = "IT Talent Drives Digitalization",
                    Summary = "IT Talent Drives Digitalization",
                    AuthorId = 1,
                    IndustryId = 4,
                    Status = Status.Pending,
                    BlobPath = "38c70736-485e-475a-a775-71cbb83d8c68.pdf",
                    DownloadCount = 0,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 11,
                    Name = "Digital Business Requires Cybersecurity",
                    Summary = "Digital Business Requires Cybersecurity",
                    AuthorId = 2,
                    IndustryId = 2,
                    Status = Status.Approved,
                    BlobPath = "47a1f82f-c136-45a9-86cd-c024d09303c1.pdf",
                    DownloadCount = 11,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 12,
                    Name = "Master Today’s Technology Trends",
                    Summary = "Master Today’s Technology Trends",
                    AuthorId = 3,
                    IndustryId = 1,
                    Status = Status.Pending,
                    BlobPath = "84712bba-eb9c-46eb-812b-e407fd2ec919.pdf",
                    DownloadCount = 0,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 13,
                    Name = "Communicate IT’s Business Value",
                    Summary = "Communicate IT’s Business Value",
                    AuthorId = 3,
                    IndustryId = 4,
                    Status = Status.Approved,
                    BlobPath = "a91041d0-c11d-428d-a8ed-78744d41f9d4.pdf",
                    DownloadCount = 2,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 14,
                    Name = "Define Your Artificial Intelligence Strategy",
                    Summary = "Define Your Artificial Intelligence Strategy",
                    AuthorId = 1,
                    IndustryId = 4,
                    Status = Status.Approved,
                    BlobPath = "3013e43a-be59-4ab2-80f6-df7f8f73e156.pdf",
                    DownloadCount = 13,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 15,
                    Name = "Accelerate Digital Business Transformation",
                    Summary = "Accelerate Digital Business Transformation",
                    AuthorId = 2,
                    IndustryId = 5,
                    Status = Status.Approved,
                    BlobPath = "b23a2883-8c92-4399-b2c3-f0dc9db10be4.pdf",
                    DownloadCount = 44,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 16,
                    Name = "Advance Cloud Technology",
                    Summary = "Advance Cloud Technology",
                    AuthorId = 3,
                    IndustryId = 1,
                    Status = Status.Approved,
                    BlobPath = "f56b2135-7b29-43ff-8737-e5ff0a4bc43f.pdf",
                    DownloadCount = 323,
                    IsDeleted = false,
                    IsFeatured = false,
                    CreatedOn = DateTime.UtcNow
                },
                new Report
                {
                    Id = 17,
                    Name = "New-to-Role CIOs: The First 100 Days",
                    Summary = "New-to-Role CIOs: The First 100 Days",
                    AuthorId = 1,
                    IndustryId = 5,
                    Status = Status.Approved,
                    BlobPath = "674b8eab-c8ea-4793-b403-ba82c99b7ff.pdf",
                    DownloadCount = 6,
                    IsDeleted = false,
                    IsFeatured = true,
                    CreatedOn = DateTime.UtcNow
                }
            });
            return reports;
        }

        public static List<Industry> ReturnIndustries()
        {
            List<Industry> industries = new List<Industry>();
            industries.AddRange(new List<Industry>
            {
                new Industry
                {
                    Id = 1,
                    Name = "Education"
                },
                new Industry
                {
                    Id = 2,
                    Name = "Healthcare"
                },
                new Industry
                {
                    Id = 3,
                    Name = "Energy and Utilities"
                },
                new Industry
                {
                    Id = 4,
                    Name = "Manufacturing"
                },
                new Industry
                {
                    Id = 5,
                    Name = "Financial services"
                },
            });
            return industries;
        }

        public static List<Tag> ReturnTags()
        {
            List<Tag> tags = new List<Tag>();
            tags.AddRange(new List<Tag>
            {
                new Tag
                {
                    Id = 1,
                    Name = "Sales"
                },
                new Tag
                {
                    Id = 2,
                    Name = "Human Resources"
                },
                new Tag
                {
                    Id = 3,
                    Name = "Marketing"
                },
            });
            return tags;
        }

        public static List<Subscriptions> ReturnSubscriptions()
        {
            List<Subscriptions> subscriptions = new List<Subscriptions>();
            subscriptions.AddRange(new List<Subscriptions>
            {
                new Subscriptions
                {
                    UserId = 1,
                    IndustryId = 1
                },
                new Subscriptions
                {
                    UserId = 1,
                    IndustryId = 2
                },
                new Subscriptions
                {
                    UserId = 2,
                    IndustryId = 1
                },
                new Subscriptions
                {
                    UserId = 2,
                    IndustryId = 2
                },
            });
            return subscriptions;
        }

        public static List<ReportTags> ReturnReportTags()
        {
            List<ReportTags> reportTags = new List<ReportTags>();
            reportTags.AddRange(new List<ReportTags>
            {
                new ReportTags
                {
                    TagId = 1,
                    ReportId = 2
                },
                new ReportTags
                {
                    TagId = 1,
                    ReportId = 1
                },
                new ReportTags
                {
                    TagId = 1,
                    ReportId = 8
                },
                 new ReportTags
                {
                    TagId = 2,
                    ReportId = 8
                },
                  new ReportTags
                {
                    TagId = 3,
                    ReportId = 1
                },
                   new ReportTags
                {
                    TagId = 2,
                    ReportId = 11
                }
            });

            return reportTags;
        }
    }
}

