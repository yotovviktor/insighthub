﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class IsTheUserSubscribedForIndustry_Should : SubscriptionBaseTest
    {
        [DataTestMethod]
        [DataRow(1, 1, true)]
        [DataRow(1, 2, false)]
        public async Task RetursnTrue_When_ParamsAreValid(int userId, int industryId, bool expected)
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = 1, IndustryId = 1 };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var actual = await sut.IsTheUserSubscribedForIndustryAsync(userId,industryId);

                Assert.AreEqual(expected, actual);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_UserIdIsOutOfRange()
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = 1, IndustryId = 1 };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.IsTheUserSubscribedForIndustryAsync(7, 1));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_IndustryIdIsOutOfRange()
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = 1, IndustryId = 1 };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.IsTheUserSubscribedForIndustryAsync(1, 7));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
