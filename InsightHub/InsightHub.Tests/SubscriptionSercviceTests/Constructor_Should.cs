﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class Constructor_Should
    {

        [TestMethod]
        public void CreateInstance()
        {
            // Arrange
            var options = Options.GetOptions(nameof(CreateInstance));

            using (var arrangeContext = new InsightHubContext(options))
            {
                // Act 
                var sut = new SubscriptionService(arrangeContext);

                // Assert
                Assert.IsNotNull(sut);
            }
        }

        [TestMethod]
        public void ThrowApiException_When_ContextIsNull()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_ContextIsNull));

            using (var arrangeContext = new InsightHubContext(options))
            {
                // Act & Assert
                var ex = Assert.ThrowsException<ApiException>(() => new SubscriptionService(null));
                Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
            }
        }
    }
}

