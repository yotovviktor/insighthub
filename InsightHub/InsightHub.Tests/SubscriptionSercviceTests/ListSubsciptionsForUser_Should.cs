﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class ListSubsciptionsForUser_Should : SubscriptionBaseTest
    {
        [TestMethod]
        public async Task ReturnsInstanceOfAType_IenumerableOfIndustryDto()
        {
            //Arrange
            var subsciptions = ModelGenerator.ReturnSubscriptions();
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subsciptions);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var actual = await sut.ListSubsciptionsOfUserAsync(1);

                Assert.IsInstanceOfType(actual, typeof(IEnumerable<IndustryDto>));
            }
        }


        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ReturnCorrectIndustries(int userId)
        {
            //Arrange
            var subsciptions = ModelGenerator.ReturnSubscriptions();
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subsciptions);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var actual = (await sut.ListSubsciptionsOfUserAsync(userId)).ToArray();

                Assert.AreEqual(1, actual[0].Id);
                Assert.AreEqual(2, actual[1].Id);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_UserIdIsOutOfRange()
        {
            //Arrange
            var subsciptions = ModelGenerator.ReturnSubscriptions();
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subsciptions);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ListSubsciptionsOfUserAsync(int.MaxValue));
            }
        }
    }
}
