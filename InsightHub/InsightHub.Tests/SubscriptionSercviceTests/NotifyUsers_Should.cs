﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.EmailSenderService;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class NotifyUsers_Should : SubscriptionBaseTest
    {
        [TestMethod]
        public async Task InvokeTheEmailSender_With_theRightIds_When_ParametersAreCorrect()
        {
            //Arrange
            var subscriptions = ModelGenerator.ReturnSubscriptions();
            var reports = ModelGenerator.ReturnReports();
           
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subscriptions);
                await arrangeContext.Reports.AddRangeAsync(reports);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                Mock<IEmailSender> EmailMoq = new Mock<IEmailSender>();
                
                EmailMoq.Setup(x => x.NotifyEmailProvider(It.IsAny<IEnumerable<int>>(), It.IsAny<int>()))
                    .Returns(Task.CompletedTask);

                var sut = new SubscriptionService(assertContext, EmailMoq.Object);
                await sut.NotifySubscribers(1, 1);
               
                EmailMoq.Verify(m => m.NotifyEmailProvider(new List<int> { 1, 2 }, 1), Times.AtLeastOnce());
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_ReportIsOutOfRange()
        {
            //Arrange
            var subscriptions = ModelGenerator.ReturnSubscriptions();
            var reports = ModelGenerator.ReturnReports();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subscriptions);
                await arrangeContext.Reports.AddRangeAsync(reports);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                Mock<IEmailSender> EmailMoq = new Mock<IEmailSender>();

                EmailMoq.Setup(x => x.NotifyEmailProvider(It.IsAny<IEnumerable<int>>(), It.IsAny<int>()))
                    .Returns(Task.CompletedTask);

                var sut = new SubscriptionService(assertContext, EmailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.NotifySubscribers(1, int.MaxValue));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
           

        [TestMethod]
        public async Task ThrowApiException_When_IndustryIsOutOfRange()
        {
            //Arrange
            var subscriptions = ModelGenerator.ReturnSubscriptions();
            var reports = ModelGenerator.ReturnReports();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Subscriptions.AddRangeAsync(subscriptions);
                await arrangeContext.Reports.AddRangeAsync(reports);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                Mock<IEmailSender> EmailMoq = new Mock<IEmailSender>();

                EmailMoq.Setup(x => x.NotifyEmailProvider(It.IsAny<IEnumerable<int>>(), It.IsAny<int>()))
                    .Returns(Task.CompletedTask);

                var sut = new SubscriptionService(assertContext, EmailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.NotifySubscribers(10, 1));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
