﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests.Abstract
{
    [TestClass]
    public class SubscriptionBaseTest
    {
       
        protected DbContextOptions<InsightHubContext> options;
        protected List<User> users = ModelGenerator.ReturnUsers();
        protected List<Industry> industries = ModelGenerator.ReturnIndustries();

        [TestInitialize()]
        public async Task Initialize()
        {
            // Arrange
            options = Options.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(industries);
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestCleanup()]
        public async Task Cleanup()
        {
            var context = new InsightHubContext(options);
            await context.Database.EnsureDeletedAsync();
        }
    }
}

