﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class SubscribeForIndustry_Should : SubscriptionBaseTest
    {
        [DataTestMethod]
        [DataRow(1, 1)]
        [DataRow(1, 2)]

        public async Task AddsSubscription_When_ArgumentsAreInRange(int userId, int industryId)
        {
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                await sut.SubscribeForIndustryAsync(userId, industryId);
                var actual = await assertContext.Subscriptions
                    .FirstOrDefaultAsync(s => s.UserId == userId && s.IndustryId == industryId);

                Assert.AreEqual(userId, actual.UserId);
                Assert.AreEqual(industryId, actual.IndustryId);

            }
        }


        [DataTestMethod]
        [DataRow(5, 1)]
        [DataRow(6, 2)]

        public async Task ThrowApiException_When_UserIdIsOutOfRange(int userId, int industryId)
        {
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.SubscribeForIndustryAsync(userId, industryId));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);

            }
        }


        [DataTestMethod]
        [DataRow(1, 8)]
        [DataRow(1, 9)]
        public async Task ThrowApiException_When_IndustryIdIsOutOfRange(int userId, int industryId)
        {
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.SubscribeForIndustryAsync(userId, industryId));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);

            }
        }


        [DataTestMethod]
        [DataRow(1, 1)]
        
        public async Task ThrowApiException_When_ThereAlreadyIsASubscribtion_Whit_ThatId(int userId, int industryId)
        {
            //Arrange
            var subscription = new Subscriptions
            {
                UserId = 1,
                IndustryId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.SubscribeForIndustryAsync(userId, industryId));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);

            }
        }


    }
}
