﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.SubscriptionSercviceTests.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionSercviceTests
{
    [TestClass]
    public class DeleteSubscription_Should : SubscriptionBaseTest
    {
        [DataTestMethod]
        [DataRow(1, 1)]
        [DataRow(1, 2)]
        [DataRow(2, 2)]
        [DataRow(2, 1)]
        public async Task RemovesTheSubscription_WhenParametersAreValid(int userId, int industryId)
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = userId, IndustryId = industryId };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                await sut.DeleteSubscriptionAsync(userId, industryId);
                var IsDeleted = !(await assertContext.Subscriptions.AnyAsync(s => s.UserId == userId && s.IndustryId == industryId));

                Assert.IsTrue(IsDeleted);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_UserIdIsOutOfRange()
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = 1, IndustryId = 1 };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteSubscriptionAsync(7, 1));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_IndustryIdIsOutOfRange()
        {
            //Arrange 
            var subscription = new Subscriptions { UserId = 1, IndustryId = 1 };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteSubscriptionAsync(1, 7));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_ThereIsNoSubscriptionWithThatIds()
        {
            //Arrange 

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new SubscriptionService(assertContext);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteSubscriptionAsync(1, 1));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
