﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class DisableUser_Should : UserBaseTest
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task DisableUserDisablesTheUser(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(DisableUserDisablesTheUser));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                await sut.DisableUserAsync(userId, DateTimeOffset.Now.AddDays(14));
                var actualUser = await assertContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
                Assert.IsTrue(actualUser.LockoutEnabled);
            }

        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task SetTheLockOutEnd(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(SetTheLockOutEnd));
            var users = ModelGenerator.ReturnUsers();
            var lockoutEndDate = DateTimeOffset.UtcNow.AddDays(14);
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
           
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                await sut.DisableUserAsync(userId, lockoutEndDate);
                var actualUser = await assertContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
               
                Assert.IsTrue(actualUser.LockoutEnabled);
                Assert.AreEqual(lockoutEndDate, actualUser.LockoutEnd);
            }

        }
        [DataTestMethod]
        [DataRow(5)]
        [DataRow(6)]
        public async Task ThrowsApiException_When_UserIdIsOutOfRange(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_UserIdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();
            var lockoutEndDate = DateTimeOffset.UtcNow.AddDays(14);
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DisableUserAsync(userId, DateTimeOffset.Now.AddDays(2)));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        public async Task ThrowsApiException_When_LockOutEndIsInThePast(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_LockOutEndIsInThePast));
            var users = ModelGenerator.ReturnUsers();
            var lockoutEndDate = DateTimeOffset.UtcNow.AddDays(-14);
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
           
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DisableUserAsync(userId, lockoutEndDate));
              
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
