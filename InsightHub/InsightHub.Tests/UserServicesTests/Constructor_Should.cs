﻿using System.Net;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class Constructor_Should : UserBaseTest
    {
        [TestMethod]
        public  void ThrowApiException_When_ContextParameterIsNullCtor()
        {
            //Arrange&Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => new UserService(null,subscriptionMoq.Object,emailMoq.Object));
            Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
        }
        
        
        [TestMethod]
        public void ThrowApiException_When_EmailSenderParameterIsNullCtor()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_EmailSenderParameterIsNullCtor));
            Mock<InsightHubContext> mock = new Mock<InsightHubContext>(options);
            
            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => new UserService(mock.Object,subscriptionMoq.Object,null));
            Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
        }


        [TestMethod]
        public void ThrowApiException_When_SubscriptionParameterIsNullCtor()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_SubscriptionParameterIsNullCtor));
            Mock<InsightHubContext> mock = new Mock<InsightHubContext>(options);

            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => new UserService(mock.Object, null, emailMoq.Object));
            Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
        }
    }
}
