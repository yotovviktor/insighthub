﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class GetUserById_Should : UserBaseTest
    {
        [TestMethod]
        public void ReturnsInstanceOfATypeUserDto_Should()
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ReturnsInstanceOfATypeUserDto_Should));
            var users = ModelGenerator.ReturnUsers();
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = sut.GetUserByIdAsync(1).Result;
                Assert.IsInstanceOfType(actual, typeof(UserDto));
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public void ReturnsCorrectUserWhenIdIsInRange_Should(int id)
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ReturnsCorrectUserWhenIdIsInRange_Should));
            var users = ModelGenerator.ReturnUsers();
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }
            var expectedFirstName = users[id - 1].FirstName;
            var expectedLastName = users[id - 1].LastName;
            var expectedEmail = users[id - 1].Email;

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = sut.GetUserByIdAsync(id).Result;
                Assert.AreEqual(expectedFirstName, actual.FirstName);
                Assert.AreEqual(expectedLastName, actual.LastName);
                Assert.AreEqual(expectedEmail, actual.Email);
                Assert.AreEqual(id, actual.Id);
            }
        }
        [TestMethod]
        public async Task ThrowsApiException_When_IdIsOutOfRange()
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ThrowsApiException_When_IdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();
            var expectedApiExceptionStatusCode = HttpStatusCode.NotFound;
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.GetUserByIdAsync(5));
                Assert.AreEqual(expectedApiExceptionStatusCode, ex.StatusCode);
            }
        }

    }
}
