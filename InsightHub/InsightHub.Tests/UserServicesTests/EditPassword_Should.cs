﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class EditPassword_Should : UserBaseTest
    {

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ChangesThePassword(int id)
        {
            //Arrange 
            var options = Options.GetOptions(nameof(ChangesThePassword));
            var users = ModelGenerator.ReturnUsers();
           
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
           
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var expected = "newPasswordTest";
                await sut.EditPasswordAsync(id, expected);
                var actual = assertContext.Users.Find(id).PasswordHash;
                
                Assert.AreEqual(actual, expected);
            }
        }

        [TestMethod]
        public async Task ThrownsApiException_When_NewPasswordIsTheSameAsTheOldOne()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrownsApiException_When_NewPasswordIsTheSameAsTheOldOne));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditPasswordAsync(1, users.First().PasswordHash));
               
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowsApiException_When_IdIsOutOfRange()
        {
            //Arrange 
            var options = Options.GetOptions(nameof(ThrowsApiException_When_IdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditPasswordAsync(10, "NewPasswordTest"));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
