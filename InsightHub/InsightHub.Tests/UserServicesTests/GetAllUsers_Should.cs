﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class GetAllUsers_Should : UserBaseTest
    {
        [TestMethod]
        public async Task ReturnsInstanceOfATypeIEnumerable_UserDto()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnsInstanceOfATypeIEnumerable_UserDto));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = sut.GetAllUsersAsync().Result;

                Assert.IsInstanceOfType(actual, typeof(IEnumerable<UserDto>));
            }
        }

        [TestMethod]
        public async Task ReturnsAllUsersCorrect()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnsAllUsersCorrect));
            var users = ModelGenerator.ReturnUsers();
            var expectedIds = new int[] { 1, 2 };
            var expectedFirstNames = new string[] { users[0].FirstName, users[1].FirstName };
            var expectedLastNames = new string[] { users[0].LastName, users[1].LastName };
            var expectedEmails = new string[] { users[0].Email, users[1].Email };

            using (var arrangeContext = new InsightHubContext(options))

            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.GetAllUsersAsync()).ToList();

                Assert.AreEqual(expectedIds[0], actual.Select(x => x.Id).ToArray()[0]);
                Assert.AreEqual(expectedIds[1], actual.Select(x => x.Id).ToArray()[1]);
                Assert.AreEqual(expectedFirstNames[0], actual.Select(x => x.FirstName).ToArray()[0]);
                Assert.AreEqual(expectedFirstNames[1], actual.Select(x => x.FirstName).ToArray()[1]);
                Assert.AreEqual(expectedLastNames[0], actual.Select(x => x.LastName).ToArray()[0]);
                Assert.AreEqual(expectedLastNames[1], actual.Select(x => x.LastName).ToArray()[1]);
                Assert.AreEqual(expectedEmails[0], actual.Select(x => x.Email).ToArray()[0]);
                Assert.AreEqual(expectedEmails[1], actual.Select(x => x.Email).ToArray()[1]);
            }

        }
    }
}
