﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class EditTheLastLogInDate_Should : UserBaseTest
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task UpdateTheDate(int id)
        {
            //Arrange 
            var options = Options.GetOptions(nameof(UpdateTheDate));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                
                await sut.EditTheLastLogInDateAsync(id);
                var actual = (await assertContext.Users.FindAsync(id)).LastLogedIn;

                Assert.IsNotNull(actual);
            }
        }

        [TestMethod]
        public async Task ThrowsApiException_When_IdIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_IdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditTheLastLogInDateAsync(int.MaxValue));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
