﻿using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.Enums;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class ChangeUserStatus_Should : UserBaseTest
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ChangesTheStatus(int id)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ChangesTheStatus));
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    var users = ModelGenerator.ReturnUsers();
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
           
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {

                IUserService sut = new UserService(assertContext,subscriptionMoq.Object,emailMoq.Object);
                var expected = Status.Rejected;
                await sut.ChangeUserStatusAsync(id, expected);
                var actual = (await assertContext.Users.FindAsync(id)).Status;
                Assert.AreEqual(expected, actual);
            }
        }


        [DataTestMethod]
        [DataRow(70)]
        [DataRow(80)]
        public async Task ThrowsApiException_When_UserIdIsOutOfRange(int id)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_UserIdIsOutOfRange));
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    var users = ModelGenerator.ReturnUsers();
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {

                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ChangeUserStatusAsync(id, Status.Approved));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ThrowsApiException_When_UserStatusIsTheSameAsTheNewOne(int id)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_UserStatusIsTheSameAsTheNewOne));
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    var users = ModelGenerator.ReturnUsers();
                    users[0].Status = Status.Approved;
                    users[1].Status = Status.Approved;
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {

                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ChangeUserStatusAsync(id, Status.Approved));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}

