﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Enums;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class FilterAsync_Should : UserBaseTest
    {

        [DataTestMethod]
        [DataRow(UserSortBy.FirstName)]
        [DataRow(UserSortBy.LastName)]
        public async Task ReturnUsersInTheCorrectOrder_When_SortByIsNotNull(UserSortBy sortBy)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnUsersInTheCorrectOrder_When_SortByIsNotNull));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.SaveChangesAsync();
            }

            //ActAndAssert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.FilterAsync(null, sortBy, null, null, null)).ToList();

                var expected = new List<User>();

                if (sortBy == UserSortBy.FirstName)
                {
                    expected = await assertContext.Users.OrderByDescending(x => x.FirstName).ToListAsync();
                }
                else if (sortBy == UserSortBy.LastName)
                {
                    expected = await assertContext.Users.OrderByDescending(x => x.LastName).ToListAsync();
                }

                await assertContext.Database.EnsureDeletedAsync();

                Assert.AreEqual(expected.Count, actual.Count);

                for (int i = 0; i < actual.Count; i++)
                {
                    Assert.AreEqual(expected[i].Id, actual[i].Id);
                }
            }
        }

        [DataTestMethod]
        [DataRow(Order.Asc)]
        [DataRow(Order.Desc)]
        public async Task ReturnUsersInTheCorrectOrder_When_OrderByIsNotNull(Order orderBy)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnUsersInTheCorrectOrder_When_OrderByIsNotNull));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.SaveChangesAsync();
            }

            //ActAndAssert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.FilterAsync(null, null, null, null, orderBy)).ToList();

                var expected = new List<User>();

                if (orderBy == Order.Asc)
                {
                    expected = await assertContext.Users.OrderBy(x => x.LastName).ToListAsync();
                }
                else
                {
                    expected = await assertContext.Users.OrderByDescending(x => x.LastName).ToListAsync();
                }

                await assertContext.Database.EnsureDeletedAsync();

                Assert.AreEqual(expected.Count, actual.Count);

                for (int i = 0; i < actual.Count; i++)
                {
                    Assert.AreEqual(expected[i].Id, actual[i].Id);
                }
            }
        }

        [DataTestMethod]
        [DataRow("viktor")]
        [DataRow("greta")]
        [DataRow("tor")]
        public async Task ReturnUsersInTheCorrectOrder_When_KeyWordIsNotNull(string keyWord)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnUsersInTheCorrectOrder_When_KeyWordIsNotNull));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.SaveChangesAsync();
            }

            //ActAndAssert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.FilterAsync(null, null, keyWord, null, null)).ToList();

                var expected = await assertContext.Users
                                      .Where(u =>
                                        u.FirstName.ToLower().Contains(keyWord.ToLower()) == true ||
                                        u.LastName.ToLower().Contains(keyWord.ToLower()) == true ||
                                        u.Email.ToLower().Contains(keyWord.ToLower()) == true)
                                      .OrderByDescending(x => x.LastName)
                                      .ToListAsync();


                await assertContext.Database.EnsureDeletedAsync();

                Assert.AreEqual(expected.Count, actual.Count);

                for (int i = 0; i < actual.Count; i++)
                {
                    Assert.AreEqual(expected[i].Id, actual[i].Id);
                }
            }
        }

        [DataTestMethod]
        [DataRow(Status.Pending, new int[] { 1, 2 })]
        [DataRow(Status.Approved, new int[] { 3 })]
        public async Task ReturnUsersInTheCorrectOrder_When_statusIsNotNull(Status? status, int[] expected)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnUsersInTheCorrectOrder_When_KeyWordIsNotNull));
            var users = ModelGenerator.ReturnUsers().ToArray();

            users[0].Status = Data.Enums.Status.Pending;
            users[1].Status = Data.Enums.Status.Pending;
            users[2].Status = Data.Enums.Status.Approved;

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.SaveChangesAsync();
            }

            //ActAndAssert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.FilterAsync(null, null, null, status, null)).ToList();

                await assertContext.Database.EnsureDeletedAsync();

                Assert.AreEqual(expected.Length, actual.Count);

                for (int i = 0; i < actual.Count; i++)
                {
                    Assert.AreEqual(expected[i], actual[i].Id);
                }
            }
        }


        [DataTestMethod]
        [DataRow(Data.Enums.Role.admin,1)]
        [DataRow(Data.Enums.Role.customer,2)]
        [DataRow(Data.Enums.Role.author,3)]
        public async Task ReturnUsersInTheCorrectOrder_When_RoleIsNotNull(Data.Enums.Role? role, int expected)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnUsersInTheCorrectOrder_When_RoleIsNotNull));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {

                await arrangeContext.Roles.AddAsync(new Data.EntityModels.Role
                {
                    Id = 1,
                    Name = "customer",
                    NormalizedName = "CUSTOMER"
                });

                await arrangeContext.Roles.AddAsync(new Data.EntityModels.Role
                {
                    Id = 2,
                    Name = "admin",
                    NormalizedName = "ADMIN"
                });

                await arrangeContext.Roles.AddAsync(new Data.EntityModels.Role
                {
                    Id = 3,
                    Name = "author",
                    NormalizedName = "AUTHOR"
                });
                await arrangeContext.Users.AddRangeAsync(users);
                await arrangeContext.UserRoles.AddAsync(new Microsoft.AspNetCore.Identity.IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = 1
                });

                await arrangeContext.UserRoles.AddAsync(new Microsoft.AspNetCore.Identity.IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = 2
                });

                await arrangeContext.UserRoles.AddAsync(new Microsoft.AspNetCore.Identity.IdentityUserRole<int>
                {
                    RoleId = 3,
                    UserId = 3
                });

                await arrangeContext.SaveChangesAsync();
            }

            //ActAndAssert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = (await sut.FilterAsync(role, null, null, null, null)).ToList().FirstOrDefault();

                await assertContext.Database.EnsureDeletedAsync();

                Assert.AreEqual(expected, actual.Id);
            }
        }
    }
}



