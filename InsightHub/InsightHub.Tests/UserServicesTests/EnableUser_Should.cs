﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class EnableUser_Should :UserBaseTest
    {
        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task EnableUserSetsTheLockoutEnableToFalse(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(EnableUserSetsTheLockoutEnableToFalse));
            var users = ModelGenerator.ReturnUsers();
            users[0].LockoutEnabled = true;
            users[1].LockoutEnabled = true;
           
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                { 
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                await sut.EnableUserAsync(userId);
                var actual = await assertContext.Users
                    .FirstOrDefaultAsync(x => x.Id == userId);

                Assert.IsFalse(actual.LockoutEnabled);
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task EnableUserSetsTheLockoutEndDateToDefault(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(EnableUserSetsTheLockoutEndDateToDefault));
            var users = ModelGenerator.ReturnUsers();
            users[0].LockoutEnabled = true;
            users[1].LockoutEnabled = true;
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                await sut.EnableUserAsync(userId);
                var actualUser = await assertContext.Users
                    .FirstOrDefaultAsync(x => x.Id == userId);

                Assert.IsFalse(actualUser.LockoutEnabled);
                Assert.AreEqual(default(DateTimeOffset?), actualUser.LockoutEnd);
            }

        }
        [DataTestMethod]
        [DataRow(5)]
        [DataRow(6)]
        public async Task ThrowsApiException_When_UserIdIsOutOfRangeEnableUser(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_UserIdIsOutOfRangeEnableUser));
            var users = ModelGenerator.ReturnUsers();
            users[0].LockoutEnabled = true;
            users[1].LockoutEnabled = true;

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EnableUserAsync(userId));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [DataTestMethod]
        [DataRow(1)]
        public async Task ThrowsApiException_When_UserLockoutEnableISAlreadyFalse(int userId)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowsApiException_When_UserLockoutEnableISAlreadyFalse));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
           
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EnableUserAsync(userId));
                
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
