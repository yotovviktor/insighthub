﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class EditEmail_Should :UserBaseTest
    {
       
        [DataTestMethod]
        [DataRow(1)]
        public async Task ChangeEmailWhenIdIsInRange(int id)
        {
            //Arrange
            var options = Options.GetOptions(nameof(ChangeEmailWhenIdIsInRange));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                await sut.EditEmailAsync(id, "User@gmail.com");
             
                Assert.AreEqual("User@gmail.com", (await assertContext.Users.FindAsync(id)).Email);
            }
        }

        [TestMethod]
        public async Task ThrownsApiException_When_IdIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrownsApiException_When_IdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditEmailAsync(4, "TestMail@gmail.com"));
                
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrownsApiException_When_NewEmailIsTheSameAsTheOldOne()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrownsApiException_When_NewEmailIsTheSameAsTheOldOne));
            var users = ModelGenerator.ReturnUsers();
            
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditEmailAsync(1,users.First().Email));
                
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
