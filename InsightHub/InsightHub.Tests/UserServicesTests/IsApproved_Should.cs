﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class IsApproved_Should : UserBaseTest
    {
        [TestMethod]
        public async Task ReturnsTrue_When_StatusIsApproved()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnsTrue_When_StatusIsApproved));
            var users = ModelGenerator.ReturnUsers();

            foreach (var user in users)
            {
                user.Status = Data.Enums.Status.Approved;
            }

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = await sut.IsUserApprovedAsync(users.First().Email);
                Assert.IsTrue(actual);

            }
        }

        [TestMethod]
        public async Task ReturnsFalse_When_StatusIsNotApproved()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ReturnsFalse_When_StatusIsNotApproved));
            var users = ModelGenerator.ReturnUsers();

            foreach (var user in users)
            {
                user.Status = Data.Enums.Status.Pending;
            }

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var actual = await sut.IsUserApprovedAsync(users.First().Email);
                Assert.IsFalse(actual);

            }
        }
        [TestMethod]
        public async Task ThrowApiException_When_UserIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_UserIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();

            foreach (var user in users)
            {
                user.Status = Data.Enums.Status.Approved;
            }

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    arrangeContext.SaveChanges();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.IsUserApprovedAsync("email"));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
