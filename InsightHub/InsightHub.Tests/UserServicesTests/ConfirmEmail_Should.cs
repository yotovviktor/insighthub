﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class ConfirmEmail_Should :UserBaseTest
    {


        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ChangeThePropertyToTrue(int userId)
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ChangeThePropertyToTrue));
            var users = ModelGenerator.ReturnUsers();
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert

            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);

                await sut.ConfirmEmailAsync(userId);

                var actual = (await assertContext.Users.FirstOrDefaultAsync(x => x.Id == userId)).EmailConfirmed;

                Assert.IsTrue(actual);
            }
        }


        [TestMethod]
        public async Task ThrowsApiException_WhenUserIdIsOutOfRange()
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ThrowsApiException_WhenUserIdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert

            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ConfirmEmailAsync(int.MaxValue));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }


        [TestMethod]
        public async Task ThrowsApiException_WhenEmailHasAlreadyBeenConfirmed()
        {
            //Arrange
            var options = Utils.Options.GetOptions(nameof(ThrowsApiException_WhenEmailHasAlreadyBeenConfirmed));
            var users = ModelGenerator.ReturnUsers();
            users.First().EmailConfirmed = true;
            using (var arrangeContext = new InsightHubContext(options))
            {
                if (await arrangeContext.Users.CountAsync() == 0)
                {
                    await arrangeContext.Users.AddRangeAsync(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert

            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ConfirmEmailAsync(users.First().Id));

                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
