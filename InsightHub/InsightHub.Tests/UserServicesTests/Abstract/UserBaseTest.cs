﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.EmailSenderService;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.UserServicesTests.Abstract
{
    [TestClass]
    public class UserBaseTest
    {
        protected Mock<IEmailSender> emailMoq = new Mock<IEmailSender>();
        protected Mock<ISubscriptionService> subscriptionMoq = new Mock<ISubscriptionService>();  
    }
}
