﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.UserServices;
using InsightHub.Tests.UserServicesTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.UserServicesTests
{
    [TestClass]
    public class EditFirstName_Should : UserBaseTest
    {

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public async Task ChangesTheFirstName(int id)
        {
            //Arrange 
            var options = Options.GetOptions(nameof(ChangesTheFirstName));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var expected = "NewFirstName";
                await sut.EditFirstNameAsync(id, expected);
                var actual = (await assertContext.Users.FindAsync(id)).FirstName;

                Assert.AreEqual(actual, expected);
            }
        }

        [TestMethod]
        public async Task ThrowsApiException_When_IdIsOutOfRange()
        {
            //Arrange 
            var options = Options.GetOptions(nameof(ThrowsApiException_When_IdIsOutOfRange));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                arrangeContext.AddRange(users);
                await arrangeContext.SaveChangesAsync();
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditFirstNameAsync(10, "NewFirstName"));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrownsApiException_When_NewFirstNameIsTheSameAsTheOldOne()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrownsApiException_When_NewFirstNameIsTheSameAsTheOldOne));
            var users = ModelGenerator.ReturnUsers();

            using (var arrangeContext = new InsightHubContext(options))
            {
                if (arrangeContext.Users.Count() == 0)
                {
                    arrangeContext.Users.AddRange(users);
                    await arrangeContext.SaveChangesAsync();
                }
            }

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                IUserService sut = new UserService(assertContext, subscriptionMoq.Object, emailMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.EditFirstNameAsync(1, users.First().FirstName));

                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
