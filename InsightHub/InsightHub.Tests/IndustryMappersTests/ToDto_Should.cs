﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.IndustryMappersTests
{
    [TestClass]
    public class ToDto_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_IndustryDto()
        {
            // Arrange
            var industry = new Industry
            {
                Id = 1,
                Name = "High-Tech",
            };

            // Act
            var industryDto = industry.ToDto();

            // Assert
            Assert.IsInstanceOfType(industryDto, typeof(IndustryDto));

            Assert.AreEqual(industry.Id, industryDto.Id);
            Assert.AreEqual(industry.Name, industryDto.Name);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_IndustryIsNull()
        {
            // Arrange
            Industry industry = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => industry.ToDto());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
