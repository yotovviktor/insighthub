﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.IndustryMappersTests
{
    [TestClass]
    public class ToEntity_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_Industry()
        {
            // Arrange
            var industryDto = new IndustryDto
            {
                Id = 1,
                Name = "High-Tech",
            };

            // Act
            var industry = industryDto.ToEntity();

            // Assert
            Assert.IsInstanceOfType(industry, typeof(Industry));

            Assert.AreEqual(industryDto.Id, industry.Id);
            Assert.AreEqual(industryDto.Name, industry.Name);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_IndustryDtoIsNull()
        {
            // Arrange
            IndustryDto industryDto = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => industryDto.ToEntity());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
