﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.TagMappersTests
{
    [TestClass]
    public class ToDto_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_TagDto()
        {
            // Arrange
            var tag = new Tag
            {
                Id = 1,
                Name = "Audit",
            };

            // Act
            var tagDto = tag.ToDto();

            // Assert
            Assert.IsInstanceOfType(tagDto, typeof(TagDto));

            Assert.AreEqual(tag.Id, tagDto.Id);
            Assert.AreEqual(tag.Name, tagDto.Name);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_TagIsNull()
        {
            // Arrange
            Tag tag = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => tag.ToDto());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
