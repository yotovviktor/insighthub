﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.TagMappersTests
{
    [TestClass]
    public class ToEntity_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_Tag()
        {
            // Arrange
            var tagDto = new TagDto
            {
                Id = 1,
                Name = "Sales",
            };

            // Act
            var tag = tagDto.ToEntity();

            // Assert
            Assert.IsInstanceOfType(tag, typeof(Tag));

            Assert.AreEqual(tagDto.Id, tag.Id);
            Assert.AreEqual(tagDto.Name, tag.Name);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_TagDtoIsNull()
        {
            // Arrange
            TagDto tagDto = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => tagDto.ToEntity());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
