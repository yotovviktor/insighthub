﻿using InsightHub.Common.DtoModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InsightHub.Common.Extensions.MappersToEntity;
using System.Collections.Generic;
using System.Linq;
using InsightHub.Data.EntityModels;
using InsightHub.Common.Exceptions;
using System.Net;

namespace InsightHub.Tests.TagMappersTests
{
    [TestClass]
    public class CollectionToEntities_Should
    {
        [TestMethod]
        public void ReturnCorrectCollectionOf_Tags()
        {
            // Arrange
            var tagDto1 = new TagDto
            {
                Id = 1,
                Name = "Audit",
            };
            var tagDto2 = new TagDto
            {
                Id = 2,
                Name = "Future",
            };

            List<TagDto> tagDtos = new List<TagDto> { tagDto1, tagDto2 };

            // Act
            var tags = tagDtos.ToEntities().ToList();

            // Assert
            Assert.AreEqual(tags.Count, tagDtos.Count);

            for (int i = 0; i < tags.Count; i++)
            {
                Assert.IsInstanceOfType(tags[i], typeof(Tag));

                Assert.AreEqual(tagDtos[i].Id, tags[i].Id);
                Assert.AreEqual(tagDtos[i].Name, tags[i].Name);
            }
        }

        [TestMethod]
        public void ThrowThrowApiException_When_AnyOftheTagsIsNull()
        {
            // Arrange
            var tagDto1 = new TagDto
            {
                Id = 1,
                Name = "Audit",
            };

            List<TagDto> tagDtos = new List<TagDto> { tagDto1, null };

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => tagDtos.ToEntities());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}