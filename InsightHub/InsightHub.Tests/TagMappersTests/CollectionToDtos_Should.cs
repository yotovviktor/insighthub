﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace InsightHub.Tests.TagMappersTests
{
    [TestClass]
    public class CollectionToDtos_Should
    {
        [TestMethod]
        public void ReturnCorrectCollectionOf_TagDtos()
        {
            // Arrange
            var tag1 = new Tag
            {
                Id = 1,
                Name = "Audit",
            };
            var tag2 = new Tag
            {
                Id = 2,
                Name = "Future",
            };

            List<Tag> tags = new List<Tag> { tag1, tag2 };

            // Act
            var tagDtos = tags.ToDtos().ToList();

            // Assert
            Assert.AreEqual(tags.Count, tagDtos.Count);

            for (int i = 0; i < tags.Count; i++)
            {
                Assert.IsInstanceOfType(tagDtos[i], typeof(TagDto));

                Assert.AreEqual(tags[i].Id, tagDtos[i].Id);
                Assert.AreEqual(tags[i].Name, tagDtos[i].Name);
            }
        }

        [TestMethod]
        public void ThrowThrowApiException_When_AnyOftheTagsIsNull()
        {
            // Arrange          
            var tag1 = new Tag
            {
                Id = 1,
                Name = "Audit",
            };

            List<Tag> tags = new List<Tag> { tag1, null };

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => tags.ToDtos());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
