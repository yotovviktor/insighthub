﻿using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Extensions.MappersToEntity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionMapperTests
{
    [TestClass]
    public class SubscriptionDtoToEntity_Should
    {
        [TestMethod]
        public void ReturnsNullWhenSubscriptionDtoIsNull()
        {
            //Arrange
            SubscriptionDto subscriptionDto = null;
            
            //Act 
            var actual = subscriptionDto.ToEntity();
            
            //Assert
            Assert.IsNull(actual);
        }
        [TestMethod]
        public void MapperMapsPropertiesCorrect()
        {
            //Arrange
            SubscriptionDto subscriptionDto = new SubscriptionDto
            {
                UserId = 1,
                IndustryId = 1
            };
            
            //Act 
            var actual = subscriptionDto.ToEntity();
            var expectedUserId = 1;
            var expectedIndustryId = 1;
            
            //Assert
            Assert.AreEqual(expectedUserId,actual.UserId);
            Assert.AreEqual(expectedIndustryId,actual.IndustryId);
        }
        [TestMethod]
        public void MapperMapsPropertiesCorrectCollection()
        {
            //Arrange
            SubscriptionDto subscriptionDto1 = new SubscriptionDto
            {
                UserId = 1,
                IndustryId = 1
            };
            
            SubscriptionDto subscriptionDto2 = new SubscriptionDto
            {
                UserId = 1,
                IndustryId = 2
            };
            var subscriptionDtos = new List<SubscriptionDto> { subscriptionDto1, subscriptionDto2 };
            
            //Arrange
            var expectedUserId1 = 1;
            var expectedUserId2 = 1;
            var expectedIndustryId1 = 1;
            var expectedIndustryId2 = 2;
            
            //Act 
            var actual = subscriptionDtos.ToEntities().ToArray();
            
            //Assert
            Assert.AreEqual(expectedUserId1, actual[0].UserId);
            Assert.AreEqual(expectedUserId2, actual[1].UserId);
            Assert.AreEqual(expectedIndustryId1, actual[0].IndustryId);
            Assert.AreEqual(expectedIndustryId2, actual[1].IndustryId);
        }
    }
}
