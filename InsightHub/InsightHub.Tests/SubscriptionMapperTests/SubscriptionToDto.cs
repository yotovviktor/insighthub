﻿using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InsightHub.Tests.SubscriptionMapperTests
{
    [TestClass]
    public class SubscriptionToDto_Should
    {
        [TestMethod]
        public void ReturnsNulWhenSubscriptionIsNull()
        {
            //Arrange
            Subscriptions subscription = null;
            
            //Act
            var actual = subscription.ToDto();
            
            //Assert
            Assert.IsNull(actual);
        }
        [TestMethod]
        public void MapperMapsThePropertiesCorrect()
        {
            //Arrange
            Subscriptions subscription = new Subscriptions
            {
                UserId = 1,
                IndustryId = 1
            };
           
            //Act
            var actual = subscription.ToDto();
            var expectedUserId = 1;
            var expectedIndustryId = 1;
           
            //Assert
            Assert.AreEqual(expectedUserId, actual.UserId);
            Assert.AreEqual(expectedIndustryId, actual.IndustryId);
            
        }
        [TestMethod]
        public void MapperMapsCollectionPropertiesCorrect()
        {
            //Arrange
            Subscriptions subscription1 = new Subscriptions
            {
                UserId = 1,
                IndustryId = 1
            };
            
            Subscriptions subscription2 = new Subscriptions
            {
                UserId = 1,
                IndustryId = 2
            };
            List<Subscriptions> subscriptions = new List<Subscriptions> { subscription1, subscription2 };
            
            //Act
            var actual = subscriptions.ToDtos().ToArray();
            var expectedUserId1 = 1;
            var expectedUserId2 = 1;
            var expectedIndustryId1 = 1;
            var expectedIndustryId2 = 2;
            
            //Assert
            Assert.AreEqual(expectedUserId1, actual[0].UserId);
            Assert.AreEqual(expectedUserId2, actual[1].UserId);
            Assert.AreEqual(expectedIndustryId1, actual[0].IndustryId);
            Assert.AreEqual(expectedIndustryId2, actual[1].IndustryId);

        }
    }
}
