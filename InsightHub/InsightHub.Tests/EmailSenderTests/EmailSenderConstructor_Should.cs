﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.EmailSenderService;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.EmailSenderTests
{
    [TestClass]
    public class EmailSenderConstructor_Should
    {
        [TestMethod]
        public  void ThrowApiException_When_ContextIsNull()
        {
            //Arrange 
            InsightHubContext context = null;
            var SmtpCLient = new Mock<SmtpClient>();

            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => new EmailSender(context, SmtpCLient.Object));
            Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
        }


        [TestMethod]
        public void ThrowApiException_When_SmtpClientIsNull()
        {
            //Arrange 
            InsightHubContext context = new InsightHubContext(Options.GetOptions(nameof(ThrowApiException_When_SmtpClientIsNull)));
            

            //Act&Assert
            var ex = Assert.ThrowsException<ApiException>(() => new EmailSender(context, null));
            Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);

        }
    }
}
