﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.EmailSenderService;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.EmailSenderTests
{
    [TestClass]
    public class NotifyEmailProvider_Should
    {

        [TestMethod]
        public async Task ThrowApiException_When_ReportIdIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_ReportIdIsOutOfRange));
            var SmtpMoq = new Mock<SmtpClient>();
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new EmailSender(assertContext,SmtpMoq.Object);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.NotifyEmailProvider(new List<int> { 1 }, int.MaxValue));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }


        [TestMethod]
        public async Task ThrowApiException_When_OneOfTheUserIdsIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_OneOfTheUserIdsIsOutOfRange));
            var reports = ModelGenerator.ReturnReports();
            var SmtpMoq = new Mock<SmtpClient>();

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.AddRangeAsync(reports);
                await arrangeContext.SaveChangesAsync();
            }
            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {

                var sut = new EmailSender(assertContext,SmtpMoq.Object);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(
                    () => sut.NotifyEmailProvider(new List<int> { int.MaxValue }, 1));

                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
