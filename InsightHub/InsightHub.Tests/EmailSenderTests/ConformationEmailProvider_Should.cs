﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.EmailSenderService;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace InsightHub.Tests.EmailSenderTests
{
    [TestClass]
    public class ConformationEmailProvider_Should
    {
        [TestMethod]
        public async Task ThrowException_When_UserIdIsOutOfRange()
        {
            //Arrange
            var options = Options.GetOptions(nameof(ThrowException_When_UserIdIsOutOfRange));
            var SmtpMoq = new Mock<SmtpClient>();

            //Act&Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new EmailSender(assertContext,SmtpMoq.Object);
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.ConfirmationEmailProvider(int.MaxValue,"link"));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
