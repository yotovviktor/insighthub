﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;

namespace InsightHub.Tests.ReportMappersTests
{
    [TestClass]
    public class ToDto_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_ReportDto()
        {
            // Arrange
            var report = ModelGenerator.ReturnReports().First();
            var tag = ModelGenerator.ReturnTags().First();

            var reportTag = new ReportTags
            {
                ReportId = report.Id,
                Report = report,
                TagId = tag.Id,
                Tag = tag
            };

            report.Tags.Add(reportTag);
            report.Industry = ModelGenerator.ReturnIndustries().First(industry => industry.Id == report.IndustryId);

            // Act
            var reportDto = report.ToDto();

            // Assert
            Assert.IsInstanceOfType(reportDto, typeof(ReportDto));

            Assert.AreEqual(report.Id, reportDto.Id);
            Assert.AreEqual(report.Name, reportDto.Name);
            Assert.AreEqual(report.Summary, reportDto.Summary);
            Assert.AreEqual(report.Status, reportDto.Status);
            Assert.AreEqual(report.IsDeleted, reportDto.IsDeleted);
            Assert.AreEqual(report.IsFeatured, reportDto.IsFeatured);
            Assert.AreEqual(report.CreatedOn, reportDto.CreatedOn);
            Assert.AreEqual(report.AuthorId, reportDto.AuthorId);

            Assert.AreEqual(report.IndustryId, reportDto.IndustryId);
            Assert.AreEqual(report.Industry.Id, reportDto.Industry.Id);
            Assert.AreEqual(report.Industry.Name, reportDto.Industry.Name);

            Assert.AreEqual(report.Tags.Count(), reportDto.Tags.Count());
            Assert.AreEqual(report.Tags.First().TagId, reportDto.Tags.First().Id);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_ReportIsNull()
        {
            // Arrange
            Report report = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => report.ToDto());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
