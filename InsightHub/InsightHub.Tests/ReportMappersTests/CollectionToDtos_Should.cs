﻿using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InsightHub.Common.Extensions.MappersToDto;
using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using System.Net;

namespace InsightHub.Tests.ReportMappersTests
{
    [TestClass]
    public class CollectionToDtos_Should
    {
        [TestMethod]
        public void ReturnCorrectCollectionOf_ReportDtos()
        {
            // Arrange          
            var report1 = ModelGenerator.ReturnReports().First();
            var tag1 = ModelGenerator.ReturnTags().First();

            var reportTag1 = new ReportTags
            {
                ReportId = report1.Id,
                Report = report1,
                TagId = tag1.Id,
                Tag = tag1
            };
            report1.Tags.Add(reportTag1);

            report1.Industry = ModelGenerator.ReturnIndustries().First(industry => industry.Id == report1.IndustryId);

            var report2 = ModelGenerator.ReturnReports().Last();
            var tag2 = ModelGenerator.ReturnTags().Last();

            var reportTag2 = new ReportTags
            {
                ReportId = report2.Id,
                Report = report2,
                TagId = tag2.Id,
                Tag = tag2
            };
            report2.Tags.Add(reportTag2);

            report2.Industry = ModelGenerator.ReturnIndustries().First(industry => industry.Id == report2.IndustryId);

            List<Report> reports = new List<Report> { report1, report2 };

            // Act
            var reportsDtos = reports.ToDtos().ToList();

            // Assert
            Assert.AreEqual(reports.Count, reportsDtos.Count);

            for (int i = 0; i < reports.Count; i++)
            {
                Assert.IsInstanceOfType(reportsDtos[i], typeof(ReportDto));

                Assert.AreEqual(reports[i].Id, reportsDtos[i].Id);
                Assert.AreEqual(reports[i].Name, reportsDtos[i].Name);
                Assert.AreEqual(reports[i].Summary, reportsDtos[i].Summary);
                Assert.AreEqual(reports[i].AuthorId, reportsDtos[i].AuthorId);
                Assert.AreEqual(reports[i].IndustryId, reportsDtos[i].IndustryId);
                Assert.AreEqual(reports[i].Status, reportsDtos[i].Status);
                Assert.AreEqual(reports[i].IsDeleted, reportsDtos[i].IsDeleted);
                Assert.AreEqual(reports[i].IsFeatured, reportsDtos[i].IsFeatured);
                Assert.AreEqual(reports[i].CreatedOn, reportsDtos[i].CreatedOn);
                Assert.AreEqual(reports[i].Tags.First().TagId, reportsDtos[i].Tags.First().Id);
            }
        }

        [TestMethod]
        public void ThrowThrowApiException_When_AnyOftheReportsIsNull()
        {
            // Arrange          
            var report1 = ModelGenerator.ReturnReports().First();
            var tag1 = ModelGenerator.ReturnTags().First();

            var reportTag1 = new ReportTags
            {
                ReportId = report1.Id,
                Report = report1,
                TagId = tag1.Id,
                Tag = tag1
            };
            report1.Tags.Add(reportTag1);

            List<Report> reports = new List<Report> { report1, null };

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => reports.ToDtos());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
