﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Net;

namespace InsightHub.Tests.ReportMappersTests
{
    [TestClass]
    public class ToEntity_Should
    {
        [TestMethod]
        public void ReturnCorrectInstanceOf_Report()
        {
            // Arrange
            var industry = ModelGenerator.ReturnIndustries().First();

            var reportDto = new ReportDto
            {
                Id = 1,
                Name = "Report",
                Summary = "Summary",
                AuthorId = 1,
                IndustryId = industry.Id,
                Industry = industry.ToDto(),
                Status = Status.Approved,
                DownloadCount = 29,
                IsDeleted = false,
                IsFeatured = false,
                CreatedOn = DateTime.UtcNow,
            };

            // Act
            var report = reportDto.ToEntity();

            // Assert
            Assert.IsInstanceOfType(report, typeof(Report));

            Assert.AreEqual(reportDto.Id, report.Id);
            Assert.AreEqual(reportDto.Name, report.Name);
            Assert.AreEqual(reportDto.Summary, report.Summary);
            Assert.AreEqual(reportDto.AuthorId, report.AuthorId);
            Assert.AreEqual(reportDto.Status, report.Status);
            Assert.AreEqual(reportDto.IsDeleted, report.IsDeleted);
            Assert.AreEqual(reportDto.IsFeatured, report.IsFeatured);
            Assert.AreEqual(reportDto.CreatedOn, report.CreatedOn);

            Assert.AreEqual(reportDto.IndustryId, report.IndustryId);
            Assert.AreEqual(reportDto.Industry.Id, report.Industry.Id);
            Assert.AreEqual(reportDto.Industry.Name, report.Industry.Name);
        }

        [TestMethod]
        public void ThrowThrowApiException_When_ReportDtoIsNull()
        {
            // Arrange
            ReportDto reportDto = null;

            // Act & Assert
            var ex = Assert.ThrowsException<ApiException>(() => reportDto.ToEntity());
            Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
        }
    }
}
