﻿using InsightHub.Common.DtoModels;
using InsightHub.Data.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InsightHub.Common.Extensions.MappersToEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using InsightHub.Data.EntityModels;

namespace InsightHub.Tests.ReportMappersTests
{
    [TestClass]
    public class CollectionToEntities_Should
    {
        [TestMethod]
        public void ReturnCorrectCollectionOf_Reports()
        {
            // Arrange
            var reportDto1 = new ReportDto
            {
                Id = 1,
                Name = "Report1",
                Summary = "Summary1",
                AuthorId = 1,
                IndustryId = 1,
                Status = Status.Approved,
                DownloadCount = 29,
                IsDeleted = false,
                IsFeatured = false,
                CreatedOn = DateTime.UtcNow,
            };

            // Arrange
            var reportDto2 = new ReportDto
            {
                Id = 2,
                Name = "Report1",
                Summary = "Summary1",
                AuthorId = 2,
                IndustryId = 2,
                Status = Status.Pending,
                DownloadCount = 2,
                IsDeleted = false,
                IsFeatured = false,
                CreatedOn = DateTime.UtcNow,
            };

            List<ReportDto> reportDtos = new List<ReportDto> { reportDto1, reportDto2 };

            // Act
            var reports = reportDtos.ToEntities().ToList();

            // Assert
            Assert.AreEqual(reportDtos.Count, reports.Count);

            for (int i = 0; i < reports.Count; i++)
            {
                Assert.IsInstanceOfType(reports[i], typeof(Report));

                Assert.AreEqual(reportDtos[i].Id, reports[i].Id);
                Assert.AreEqual(reportDtos[i].Name, reports[i].Name);
                Assert.AreEqual(reportDtos[i].Summary, reports[i].Summary);
                Assert.AreEqual(reportDtos[i].AuthorId, reports[i].AuthorId);
                Assert.AreEqual(reportDtos[i].IndustryId, reports[i].IndustryId);
                Assert.AreEqual(reportDtos[i].Status, reports[i].Status);
                Assert.AreEqual(reportDtos[i].IsDeleted, reports[i].IsDeleted);
                Assert.AreEqual(reportDtos[i].IsFeatured, reports[i].IsFeatured);
                Assert.AreEqual(reportDtos[i].CreatedOn, reports[i].CreatedOn);
            }
        }
    }
}
