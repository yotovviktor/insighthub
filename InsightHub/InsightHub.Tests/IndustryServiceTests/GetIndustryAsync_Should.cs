﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.IndustryServices;
using InsightHub.Tests.IndustryServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests
{
    [TestClass]
    public class GetIndustryAsync_Should : IndustryBaseTest
    {     
        [TestMethod]
        [DynamicData(nameof(GetIndustry), DynamicDataSourceType.Method)]
        public async Task ReturnCorrectIndustry_When_ParamsAreValid(Industry industry)
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.GetIndustryAsync(industry.Id);

                Assert.AreEqual(industry.Id, result.Id);
                Assert.AreEqual(industry.Name, result.Name);
            }
        }

        public static IEnumerable<object[]> GetIndustry()
        {
            yield return new object[] { ModelGenerator.ReturnIndustries().First() };
            yield return new object[] { ModelGenerator.ReturnIndustries().Last() };
        }

        [TestMethod]
        public async Task ThrowApiExceptionWhen_IndustryNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                const int NOT_EXISTING_INDUSTRY_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.GetIndustryAsync(NOT_EXISTING_INDUSTRY_ID));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
