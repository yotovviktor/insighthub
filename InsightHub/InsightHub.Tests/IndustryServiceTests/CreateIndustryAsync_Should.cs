﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.IndustryServices;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests
{
    [TestClass]
    public class CreateIndustryAsync_Should
    {
        [TestMethod]
        public async Task CreateIndustry_When_ParamsAreValid()
        {
            // Arrange
            var options = Options.GetOptions(nameof(CreateIndustry_When_ParamsAreValid));
            var industryDto = new IndustryDto { Name = "High-Tech" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.CreateIndustryAsync(industryDto);
                
                Assert.AreEqual(industryDto.Name, result.Name);

                var createdIndustry = await assertContext.Industries.FindAsync(result.Id);
                Assert.AreEqual(createdIndustry.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_IndustryAlreadyExist()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiExeption_When_IndustryAlreadyExist));
            var industryDto = new IndustryDto { Name = "High-Tech" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(arrangeContext);
                var result = await sut.CreateIndustryAsync(industryDto);
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
    
                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateIndustryAsync(industryDto));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_PassedDtoIsNull()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiExeption_When_PassedDtoIsNull));          

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateIndustryAsync(null));
                Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
            }
        }
    }
}
