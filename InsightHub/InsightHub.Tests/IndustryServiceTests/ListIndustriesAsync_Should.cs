﻿using InsightHub.Common.DtoModels;
using InsightHub.Data.Context;
using InsightHub.Services.IndustryServices;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests
{
    [TestClass]
    public class ListIndustriesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCollectionOfIndustryDTOs()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ReturnCollectionOfIndustryDTOs));
            var industries = ModelGenerator.ReturnIndustries().OrderBy(industry => industry.Name).ToList();

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(industries);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.ListIndustriesAsync();

                Assert.IsInstanceOfType(result, typeof(ICollection<IndustryDto>));
                Assert.AreEqual(industries.Count, result.Count);

                for (int i = 0; i < industries.Count; i++)
                {
                    Assert.AreEqual(industries[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoIndustriesExist()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ReturnEmptyCollection_When_NoIndustriesExist));        

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.ListIndustriesAsync();

                Assert.IsFalse(result.Any());
            }
        }
    }
}
