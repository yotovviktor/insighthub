﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.IndustryServices;
using InsightHub.Tests.IndustryServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests
{
    [TestClass]
    public class DeleteIndustryAsync_Should : IndustryBaseTest
    {
        [TestMethod]
        public async Task DeleteIndustry_When_ParamsAreValid()
        {
            // Arrange           
            var industry = new Industry { Name = "Retail" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                await sut.DeleteIndustryAsync(industry.Id);

                var result = await assertContext.Industries.FindAsync(industry.Id);
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ThrowApiExceptionWhen_IndustryNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                const int NOT_EXISTING_INDUSTRY_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteIndustryAsync(NOT_EXISTING_INDUSTRY_ID));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_IndustryIsUsedByReport()
        {
            // Arrange           
            var industry = ModelGenerator.ReturnIndustries().First();

            var report = new Report { Name = "Report", Summary = "Summary", IndustryId = industry.Id };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);               

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteIndustryAsync(industry.Id));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
