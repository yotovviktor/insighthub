﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.IndustryServices;
using InsightHub.Tests.IndustryServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests
{
    [TestClass]
    public class UpdateIndustryAsync_Should : IndustryBaseTest
    {
        [TestMethod]
        [DynamicData(nameof(GetIndustry), DynamicDataSourceType.Method)]
        public async Task UpdateIndustry_When_ParamsAreValid(Industry industry)
        {           
            var industryDto = new IndustryDto { Id = industry.Id, Name = "High-Tech" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                await sut.UpdateIndustryAsync(industryDto);

                var updatedIndustry = await assertContext.Industries.FindAsync(industry.Id);

                Assert.AreEqual(industryDto.Name, updatedIndustry.Name);
            }
        }

        public static IEnumerable<object[]> GetIndustry()
        {
            yield return new object[] { ModelGenerator.ReturnIndustries().First() };
            yield return new object[] { ModelGenerator.ReturnIndustries().Last() };
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_IndustryNameExist()
        {          
            var industryDto = new IndustryDto { Id = industries.First().Id, Name = industries.Last().Name };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateIndustryAsync(industryDto));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_IndustryNotFound()
        { 
            // Arrange        
            const int NOT_EXISTING_INDUSTRY_ID = int.MaxValue;
            var industryDto = new IndustryDto { Id = NOT_EXISTING_INDUSTRY_ID, Name = "High-Tech" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);                

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateIndustryAsync(industryDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
