﻿using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.IndustryServiceTests.Abstract
{
    [TestClass]
    public abstract class IndustryBaseTest
    {
        protected DbContextOptions<InsightHubContext> options;
        protected List<Industry> industries = ModelGenerator.ReturnIndustries();

        [TestInitialize()]
        public async Task Initialize()
        {
            // Arrange
            options = Options.GetOptions(nameof(TestContext.TestName));            

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddRangeAsync(industries);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestCleanup()]
        public async Task Cleanup()
        {
            var context = new InsightHubContext(options);
            await context.Database.EnsureDeletedAsync();
        }
    }
}
