﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.TagServices;
using InsightHub.Tests.TagServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagServiceTests
{
    [TestClass]
    public class DeleteTagAsync_Should : TagBaseTest
    {
        [TestMethod]
        public async Task DeleteTag_When_ParamsAreValid()
        {
            // Arrange           
            var tag = new Tag { Name = "Audit" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                await sut.DeleteTagAsync(tag.Id);

                var result = await assertContext.Tags.FindAsync(tag.Id);
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ThrowApiExceptionWhen_TagNotFound()
        {
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                const int NOT_EXISTING_TAG_ID = int.MaxValue;

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteTagAsync(NOT_EXISTING_TAG_ID));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_TagIsUsedByReport()
        {
            // Arrange           
            var tag = ModelGenerator.ReturnTags().First();
            var report = ModelGenerator.ReturnReports().First();
            var reportTag = new ReportTags() { ReportId = report.Id, TagId = tag.Id };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.ReportTags.AddAsync(reportTag);
                await arrangeContext.SaveChangesAsync();
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.DeleteTagAsync(tag.Id));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }
    }
}
