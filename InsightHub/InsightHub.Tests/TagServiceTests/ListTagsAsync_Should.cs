﻿using InsightHub.Common.DtoModels;
using InsightHub.Data.Context;
using InsightHub.Services.TagServices;
using InsightHub.Tests.TagServiceTests.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagServiceTests
{
    [TestClass]
    public class ListTagsAsync_Should : TagBaseTest
    {
        [TestMethod]
        public async Task ReturnOrderedCollectionOfTagDTOs()
        {
            //Arrange
            tags = tags.OrderBy(tag => tag.Name).ToList();

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.ListTagsAsync();

                Assert.IsInstanceOfType(result, typeof(ICollection<TagDto>));
                Assert.AreEqual(tags.Count, result.Count);

                for (int i = 0; i < tags.Count; i++)
                {
                    Assert.AreEqual(tags[i].Name, result.ElementAt(i).Name);
                }
            }
        }

        [TestMethod]
        public async Task ReturnEmptyCollection_When_NoTagsExist()
        {           
            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                await assertContext.Database.EnsureDeletedAsync();

                var sut = new TagService(assertContext);
                var result = await sut.ListTagsAsync();

                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public async Task ReturnFilteredResultsCorrectly()
        {           
            var keyword = tags.First().Name.Substring(0,3);

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.ListTagsAsync(keyword);

                Assert.IsTrue(result.Any(tag => tag.Name.Equals(tags.First().Name)));
            }
        }       
    }
}
