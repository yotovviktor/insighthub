﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.TagServices;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagServiceTests
{
    [TestClass]
    public class CreateTagAsync_Should
    {
        [TestMethod]
        public async Task CreateTag_When_ParamsAreValid()
        {
            // Arrange
            var options = Options.GetOptions(nameof(CreateTag_When_ParamsAreValid));
            var tagDto = new TagDto { Name = "Marketing" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.CreateTagAsync(tagDto);

                Assert.AreEqual(tagDto.Name, result.Name);

                var createdTag = await assertContext.Tags.FindAsync(result.Id);
                Assert.AreEqual(createdTag.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_TagAlreadyExist()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiExeption_When_TagAlreadyExist));
            var tagDto = new TagDto { Name = "Marketing" };

            using (var arrangeContext = new InsightHubContext(options))
            {
                var sut = new TagService(arrangeContext);
                var result = await sut.CreateTagAsync(tagDto);
            }

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateTagAsync(tagDto));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_PassedDtoIsNull()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiExeption_When_PassedDtoIsNull));

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.CreateTagAsync(null));
                Assert.AreEqual(HttpStatusCode.BadRequest, ex.StatusCode);
            }
        }
    }
}
