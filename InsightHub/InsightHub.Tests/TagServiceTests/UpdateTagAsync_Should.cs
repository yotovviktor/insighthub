﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.TagServices;
using InsightHub.Tests.TagServiceTests.Abstract;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagServiceTests
{
    [TestClass]
    public class UpdateTagAsync_Should : TagBaseTest
    {
        [TestMethod]
        [DynamicData(nameof(GetTag), DynamicDataSourceType.Method)]
        public async Task UpdateTag_When_ParamsAreValid(Tag tag)
        {
            var tagDto = new TagDto { Id = tag.Id, Name = "Customer Service" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                await sut.UpdateTagAsync(tagDto);

                var updatedTag = await assertContext.Tags.FindAsync(tag.Id);

                Assert.AreEqual(tagDto.Name, updatedTag.Name);
            }
        }

        public static IEnumerable<object[]> GetTag()
        {
            yield return new object[] { ModelGenerator.ReturnTags().First() };
            yield return new object[] { ModelGenerator.ReturnTags().Last() };
        }

        [TestMethod]
        public async Task ThrowApiExeption_When_TagNameExist()
        {
            var tagDto = new TagDto { Id = tags.First().Id, Name = tags.Last().Name };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateTagAsync(tagDto));
                Assert.AreEqual(HttpStatusCode.Conflict, ex.StatusCode);
            }
        }

        [TestMethod]
        public async Task ThrowApiException_When_TagNotFound()
        {
            // Arrange        
            const int NOT_EXISTING_TAG_ID = int.MaxValue;
            var tagDto = new TagDto { Id = NOT_EXISTING_TAG_ID, Name = "Customer Service" };

            // Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                var ex = await Assert.ThrowsExceptionAsync<ApiException>(() => sut.UpdateTagAsync(tagDto));
                Assert.AreEqual(HttpStatusCode.NotFound, ex.StatusCode);
            }
        }
    }
}
