﻿using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Tests.TagServiceTests.Abstract
{
    [TestClass]
    public abstract class TagBaseTest
    {
        protected DbContextOptions<InsightHubContext> options;
        protected List<Tag> tags = ModelGenerator.ReturnTags();

        [TestInitialize()]
        public async Task Initialize()
        {
            // Arrange
            options = Options.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddRangeAsync(tags);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestCleanup()]
        public async Task Cleanup()
        {
            var context = new InsightHubContext(options);
            await context.Database.EnsureDeletedAsync();
        }
    }
}
