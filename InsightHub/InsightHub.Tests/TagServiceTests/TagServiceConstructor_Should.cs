﻿using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Services.TagServices;
using InsightHub.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InsightHub.Tests.TagServiceTests
{
    [TestClass]
    public class TagServiceConstructor_Should
    {
        [TestMethod]
        public void CreateInstance()
        {
            // Arrange
            var options = Options.GetOptions(nameof(CreateInstance));

            using (var arrangeContext = new InsightHubContext(options))
            {
                // Act 
                var sut = new TagService(arrangeContext);

                // Assert
                Assert.IsNotNull(sut);
            }
        }

        [TestMethod]
        public void ThrowApiException_When_ContextIsNull()
        {
            // Arrange
            var options = Options.GetOptions(nameof(ThrowApiException_When_ContextIsNull));

            using (var arrangeContext = new InsightHubContext(options))
            {
                // Act & Assert
                var ex = Assert.ThrowsException<ApiException>(() => new TagService(null));
                Assert.AreEqual(HttpStatusCode.InternalServerError, ex.StatusCode);
            }
        }
    }
}
