import { User } from '../models/user/user';
import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject } from 'rxjs';

import { UserService } from '../services/user.service';
import { UserFilter } from '../models/user/user-filter';
import { UserSortField } from '../models/user/user-sort-field';
import { Order } from '../models/order.type';

export class UsersDatasource implements DataSource<User> {
  private userSubject = new BehaviorSubject<User[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(protected userService: UserService) { }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.userSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.userSubject.complete();
    this.loadingSubject.complete();
  }

  async LoadUsers(filter?: UserFilter, sortField?: UserSortField, order?: Order) {
    this.loadingSubject.next(true);

    try {
      const users = await this.userService.getUsers(filter, sortField, order);
      this.userSubject.next(users);
    } finally {
      this.loadingSubject.next(false);
    }
  }
}
