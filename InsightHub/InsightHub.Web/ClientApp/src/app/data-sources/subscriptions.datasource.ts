import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { Subscription } from '../models/subscription';
import { SubscriptionService } from '../services/subscription.service';
import { CollectionViewer } from '@angular/cdk/collections';
import { Input } from '@angular/core';

export class SubscriptionsDatasource implements DataSource<Subscription> {

  private subscriptionsSubject = new BehaviorSubject<Subscription[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(protected subsService: SubscriptionService, private userId: number) { }

  connect(collectionViewer: CollectionViewer): Observable<Subscription[]> {
    return this.subscriptionsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subscriptionsSubject.complete();
    this.loadingSubject.complete();
  }

  async loadSubscriptions() {
    this.loadingSubject.next(true);

    try {
      const subscriptions = await this.subsService.listSubscriptions(this.userId);
      this.subscriptionsSubject.next(subscriptions);
    } finally {
      this.loadingSubject.next(false);
    }
  }

}
