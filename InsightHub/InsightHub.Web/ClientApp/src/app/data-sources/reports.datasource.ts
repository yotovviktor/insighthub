import { CollectionViewer, DataSource } from '@angular/cdk/collections';

import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { BehaviorSubject, Observable } from 'rxjs';

import { Report } from '../models/report/report';
import { ReportService } from '../services/report.service';
import { ReportsFilter } from '../models/report/report.filter';
import { ReportSortField } from '../models/report/sort-field.type';
import { Order } from '../models/order.type';
import { Paging } from '../models/paging';

export class ReportsDataSource implements DataSource<Report> {

  private reportsSubject = new BehaviorSubject<Report[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  // tslint:disable-next-line: variable-name
  private _paginator: MatPaginator;
  protected paging: Paging = {
    offset: 0,
    limit: 10
  };

  public loading$ = this.loadingSubject.asObservable();

  public filter?: ReportsFilter;
  public sortField?: ReportSortField;
  public order?: Order;

  constructor(protected reportService: ReportService) { }

  connect(collectionViewer: CollectionViewer): Observable<Report[]> {
    return this.reportsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.reportsSubject.complete();
    this.loadingSubject.complete();
  }

  set paginator(paginator: MatPaginator) {
    this._paginator = paginator;

    this._paginator.page.subscribe((event: PageEvent) => {
      this.paging = {
        offset: event.pageIndex * event.pageSize,
        limit: event.pageSize
      };

      this.loadReports();
    });
  }

  protected async getReports() {
    return await this.reportService.getAllReports(this.filter, this.sortField, this.order, this.paging);
  }

  async loadReports() {

    this.loadingSubject.next(true);

    try {
      const [reports, totalCount] = await this.getReports();

      this._paginator.length = totalCount;
      this.reportsSubject.next(reports);
    } finally {
      this.loadingSubject.next(false);
    }
  }
}
