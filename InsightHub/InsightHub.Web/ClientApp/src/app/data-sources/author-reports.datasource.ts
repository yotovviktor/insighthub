import { Router } from '@angular/router';

import { ReportsDataSource } from './reports.datasource';
import { ReportService } from '../services/report.service';
import { AuthenticationService } from '../services/authentication.service';

export class AuthorReportsDataSource extends ReportsDataSource {

  constructor(
    private authService: AuthenticationService,
    protected reportService: ReportService,
    protected router: Router) {

    super(reportService);
  }

  protected async getReports() {

    const claims = this.authService.getClaims();
    if (!claims.isAuthenticated) {
      this.router.navigate(['/login']);
    }

    const authorId = claims.userId;
    return await this.reportService.getAuthorReports(
      authorId, this.filter, this.sortField, this.order, this.paging
    );
  }

}
