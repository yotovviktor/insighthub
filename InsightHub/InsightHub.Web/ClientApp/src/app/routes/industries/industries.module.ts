import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

import { IndustryListComponent } from './industry-list/industry-list.component';
import { IndustrySelectorComponent } from './industry-selector/industry-selector.component';
import { AppRoutingModule } from '../../app-routing.module';
import { IndustryComponent } from './industry/industry.component';
import { SubscribtionsModule } from '../subscribtions/subscribtions.module';

@NgModule({
  declarations: [
    IndustryListComponent,
    IndustrySelectorComponent,
    IndustryComponent
    ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    SubscribtionsModule
  ],
  exports: [
    IndustrySelectorComponent,
    IndustryComponent
  ]
})
export class IndustriesModule { }
