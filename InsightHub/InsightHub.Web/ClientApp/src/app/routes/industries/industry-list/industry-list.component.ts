import { Component, OnInit } from '@angular/core';
import { Industry } from '../../../models/industry';
import { IndustryService } from '../../../services/industry.service';
import { chunk } from 'lodash';

@Component({
  selector: 'app-industry-list',
  templateUrl: './industry-list.component.html',
  styleUrls: ['./industry-list.component.scss']
})
export class IndustryListComponent implements OnInit {

  isLoading = true;
  industryPairs: Industry[][];
  private industries: Industry[];

  constructor(private industryService: IndustryService) { }

  ngOnInit() {
    this.getIndustries();
  }

  private async getIndustries() {
    this.industries = await this.industryService.getIndustries();
    this.industryPairs = chunk(this.industries, 2);

    this.isLoading = false;
  }
}
