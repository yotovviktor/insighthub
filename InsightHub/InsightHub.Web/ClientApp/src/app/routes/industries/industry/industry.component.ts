import { Component, Input } from '@angular/core';

import { Industry } from 'src/app/models/industry';
import { Router } from '@angular/router';

@Component({
  selector: 'app-industry',
  templateUrl: './industry.component.html',
  styleUrls: ['./industry.component.scss']
})
export class IndustryComponent {

  @Input() industry: Industry;

  constructor(private router: Router) { }

  onDetails(industryId: number) {
    this.router.navigate(['/reports'], { queryParams: { industry: industryId } });
  }
}
