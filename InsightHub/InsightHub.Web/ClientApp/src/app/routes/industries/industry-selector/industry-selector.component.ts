import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MatSelectChange } from '@angular/material/select';

import { Industry } from '../../../models/industry';
import { IndustryService } from '../../../services/industry.service';

@Component({
  selector: 'app-industry-selector',
  templateUrl: './industry-selector.component.html',
  styleUrls: ['./industry-selector.component.scss']
})
export class IndustrySelectorComponent implements OnInit {

  industries: Industry[];

  @Input() form: FormGroup;
  @Input() allowNoneSelection = true;
  @Output() selected = new EventEmitter<number>();

  constructor(private industryService: IndustryService) { }

  ngOnInit() {
    this.getIndustries();
  }

  private async getIndustries() {
    this.industries = await this.industryService.getIndustries();
  }

  onSelected(selection: MatSelectChange) {
    const industryId: number = selection.value;
    this.selected.emit(industryId);
  }
}
