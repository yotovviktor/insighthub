import { Component, OnInit } from '@angular/core';
import { AuthorReportsDataSource } from 'src/app/data-sources/author-reports.datasource';
import { ReportService } from 'src/app/services/report.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-author-reports',
  templateUrl: './author-reports.component.html',
  styleUrls: ['./author-reports.component.scss']
})
export class AuthorReportsComponent {

  dataSource: AuthorReportsDataSource;

  constructor(private authService: AuthenticationService, private reportService: ReportService, private router: Router) {

    this.dataSource = new AuthorReportsDataSource(authService, reportService, router);
  }

  onCreate(){
    this.router.navigate(['reports/create']);
  }
}
