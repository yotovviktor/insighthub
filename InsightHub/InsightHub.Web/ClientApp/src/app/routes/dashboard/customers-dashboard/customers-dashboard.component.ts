import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers-dashboard',
  templateUrl: './customers-dashboard.component.html',
  styleUrls: ['./customers-dashboard.component.scss']
})
export class CustomersDashboardComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    const claims = this.authService.getClaims();
    if (!claims || claims?.isAuthenticated === false) {
      this.router.navigate(['/login']);
    }
    else {
      if (claims?.userRole === 'author') {
        this.router.navigate(['/author']);
        return;
      }
      if (claims?.userRole === 'admin') {
        this.router.navigate(['/admin']);
        return;
      }
    }
  }

}
