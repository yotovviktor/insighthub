import { Component, OnInit } from '@angular/core';
import { CustomerDownloadsDataSource } from 'src/app/data-sources/customer-downloads.datasource';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ReportService } from 'src/app/services/report.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-reports',
  templateUrl: './customer-reports.component.html',
  styleUrls: ['./customer-reports.component.scss']
})
export class CustomerReportsComponent {

  dataSource: CustomerDownloadsDataSource;

  constructor(private authService: AuthenticationService, private reportService: ReportService, private router: Router) {

    this.dataSource = new CustomerDownloadsDataSource(authService, reportService, router);
  }
}

