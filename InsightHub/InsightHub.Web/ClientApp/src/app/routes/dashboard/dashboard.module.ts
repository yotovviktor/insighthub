import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersDashboardComponent } from './customers-dashboard/customers-dashboard.component';
import { AuthorDashboardComponent } from './author-dashboard/author-dashboard.component';
import { AuthorReportsComponent } from './author-reports/author-reports.component';
import { CustomerReportsComponent } from './customer-reports/customer-reports.component';
import { SubscribtionsModule } from '../subscribtions/subscribtions.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ReportsModule } from '../reports/reports.module';
import { UsersModule } from '../users/users.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    CustomersDashboardComponent,
    AuthorDashboardComponent,
    AuthorReportsComponent,
    CustomerReportsComponent,
    AdminDashboardComponent
  ],
  imports: [
    CommonModule,
    SubscribtionsModule,
    ReportsModule,
    UsersModule,
    MatButtonModule
  ]
})
export class DashboardModule { }
