import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-author-dashboard',
  templateUrl: './author-dashboard.component.html',
  styleUrls: ['./author-dashboard.component.scss']
})
export class AuthorDashboardComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    const claims = this.authService.getClaims();
    if (!claims || claims?.isAuthenticated === false) {
      this.router.navigate(['/login']);
    }
    else {
      if (claims?.userRole === 'customer') {
        this.router.navigate(['/customer']);
        return;
      }
      if (claims?.userRole === 'admin') {
        this.router.navigate(['/admin']);
        return;
      }
    }
  }

}
