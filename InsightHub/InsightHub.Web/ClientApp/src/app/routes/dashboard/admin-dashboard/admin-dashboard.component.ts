import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    const claims = this.authService.getClaims();
    if (!claims || claims?.isAuthenticated === false) {
      this.router.navigate(['/login']);
    }
    else {
      if (claims?.userRole === 'customer') {
        this.router.navigate(['/customer']);
        return;
      }
      if (claims?.userRole === 'author') {
        this.router.navigate(['/author']);
        return;
      }
    }
  }


}
