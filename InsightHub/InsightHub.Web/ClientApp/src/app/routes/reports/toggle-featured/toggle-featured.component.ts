import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { Report } from 'src/app/models/report/report';

@Component({
  selector: 'app-toggle-featured',
  templateUrl: './toggle-featured.component.html',
  styleUrls: ['./toggle-featured.component.scss']
})
export class ToggleFeaturedComponent {

  icon: string;
  color: string;
  isFeatured: boolean;
  submitted = false;

  @Output() toggled = new EventEmitter<boolean>();

  // tslint:disable-next-line: variable-name
  private _report: Report;

  constructor(private reportService: ReportService) { }

  @Input()
  set report(report: Report) {
    this._report = report;
    this.isFeatured = report.isFeatured;
    this.updateIcon();
  }

  async onClicked() {

    this.submitted = true;

    try {
      await this.reportService.setFeatured(this._report.id, !this.isFeatured);
      this.isFeatured = !this.isFeatured;
      this.toggled.emit(this.isFeatured);
      this.updateIcon();
    } finally {
      this.submitted = false;
    }
  }

  private updateIcon() {
    if (this.isFeatured) {
      this.icon = 'bookmark';
      this.color = 'accent';
    } else {
      this.icon = 'bookmark_border';
      this.color = 'disabled';
    }
  }

}
