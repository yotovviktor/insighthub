import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import { AuthenticationService } from '../../../services/authentication.service';
import { ReportService } from '../../../services/report.service';
import { ReportsDataSource } from '../../../data-sources/reports.datasource';
import { ReportsFilter } from '../../../models/report/report.filter';
import { ReportSortField } from '../../../models/report/sort-field.type';
import { Order } from '../../../models/order.type';

@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss'],
})
export class ReportListComponent implements AfterViewInit, OnInit {

  form: FormGroup;

  sortField: ReportSortField;
  order: Order;
  filter: ReportsFilter = {};

  @Input() dataSource: ReportsDataSource;
  @Input() showStatus = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  get columnsToDisplay(): string[] {
    const columns = ['name', 'industry', 'tags', 'downloads', 'date'];

    if (this.showStatus) {
      columns.push('status');
    }

    columns.push('operation');

    return columns;
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private authService: AuthenticationService,
    private titleService: Title
  ) {
    this.titleService.setTitle('InsightHub Reports');

    this.form = this.formBuilder.group({
      industry: [],
      tags: []
    });
  }

  private loadReports() {
    this.dataSource.filter = this.filter;
    this.dataSource.sortField = this.sortField;
    this.dataSource.order = this.order;

    this.dataSource.loadReports();
  }

  ngOnInit() {
    const role = this.authService.getClaims()?.userRole;
    this.showStatus = this.showStatus || (role === 'admin');

    this.filter.industryId = +this.route.snapshot.queryParamMap.get('industry');

    if (!this.dataSource) {
      this.dataSource = new ReportsDataSource(this.reportService);
    }

    this.loadReports();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  onIndustrySelected(industryId: number) {
    this.filter.industryId = industryId;
    this.loadReports();
  }

  onTagsSelected(tagIds: number[]) {
    this.filter.tagIds = tagIds;
    this.loadReports();
  }

  onNameSelected(event: Event) {
    this.filter.name = (event.target as HTMLInputElement).value;
    this.loadReports();
  }

  onStatusSelect(status: string) {
    if (status) {
      this.filter.status = status;
    } else {
      this.filter.status = null;
    }

    this.loadReports();
  }

  sortReports(sort: Sort) {
    this.order = sort.direction as Order;
    this.sortField = sort.active as ReportSortField;

    this.loadReports();
  }
}
