import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ReportService } from '../../../services/report.service';
import { Report } from '../../../models/report/report';
import { AuthenticationService } from '../../../services/authentication.service';
import { Claims } from '../../../models/claims';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss']
})
export class ReportDetailComponent implements OnInit {

  report: Report;
  isLoading = true;
  claims: Claims;
  result: boolean;
  isFeatured: boolean;
  status: string;

  get downloadDisabled() {
    const isNotOwner = this.claims?.userRole === 'author' && this.claims?.userId !== this.report.author.id;
    return isNotOwner;
  }

  get actionsVisible() {
    const isOwner = this.claims?.userRole === 'author' && this.claims?.userId === this.report.author.id;
    return (isOwner || this.claims?.userRole === 'admin');
  }

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private reportService: ReportService,
    private router: Router,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private titleService: Title
  ) {
    this.claims = this.authService.getClaims();
    this.titleService.setTitle('InsightHub Report');
  }

  ngOnInit(): void {
    this.getReport();
  }

  async onDownloadClicked() {

    if (!this.claims || !this.claims.isAuthenticated) {
      this.router.navigate(['/login']);
    } else {
      const downloadUrl = await this.reportService.getDownloadUrl(this.report.id);
      window.location.href = downloadUrl;
    }
  }

  private async getReport() {
    const id = +this.route.snapshot.paramMap.get('id');
    try {
      this.report = await this.reportService.getReport(id);
      this.isFeatured = this.report.isFeatured;
      this.status = this.report.status;
    } finally {
      this.isLoading = false;
    }
  }

  confirmDialog(): void {
    const dialogData =  `Are you sure you want to delete this report?`;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      this.deleteReport(dialogResult);
    });
  }

  private async deleteReport(result: boolean) {

    if (result) {
      try {
        await this.reportService.deleteReport(this.report.id);
        this.toastr.success('Report successfully deleted!', 'Success');
        this.router.navigate(['/my-reports']);
      } catch (error) {
        this.toastr.error('Something went wrong!', 'Error');
      }
    }
  }

}
