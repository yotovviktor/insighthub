import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent {

  @Input() enabled = false;
  @Input() form: FormGroup;
  @Output() submited = new EventEmitter();

  onSubmit() {
    this.submited.emit();
  }
}
