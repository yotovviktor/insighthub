import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule} from '@angular/material/expansion';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToggleFeaturedComponent } from './toggle-featured/toggle-featured.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';

import { MaterialFileInputModule } from 'ngx-material-file-input';

import { IndustriesModule } from '../industries/industries.module';
import { TagsModule } from '../tags/tags.module';
import { ReportListComponent } from './report-list/report-list.component';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { CreateReportComponent } from './create-report/create-report.component';
import { SharedModule } from '../shared/shared.module';
import { EditReportComponent } from './edit-report/edit-report.component';
import { ReportFormComponent } from './report-form/report-form.component';
import { ToastrModule } from 'ngx-toastr';
import { ReportPreviewComponent } from './report-preview/report-preview.component';

@NgModule({
  declarations: [
    ReportListComponent,
    ReportDetailComponent,
    CreateReportComponent,
    EditReportComponent,
    ReportFormComponent,
    ToggleFeaturedComponent,
    ReportPreviewComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    IndustriesModule,
    TagsModule,
    SharedModule,
    MatButtonModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSortModule,
    MatGridListModule,
    MatSelectModule,
    MatExpansionModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MaterialFileInputModule,
    MatTooltipModule,
    MatIconModule,
    MatPaginatorModule,
    RouterModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-center',
    }),
  ],
  exports: [
    ReportListComponent,
    ReportPreviewComponent
  ]

})
export class ReportsModule { }
