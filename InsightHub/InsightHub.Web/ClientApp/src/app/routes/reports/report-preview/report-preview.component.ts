import { Component, Input } from '@angular/core';
import { Report } from 'src/app/models/report/report';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-preview',
  templateUrl: './report-preview.component.html',
  styleUrls: ['./report-preview.component.scss']
})
export class ReportPreviewComponent {

  @Input() reports: Report[];

  constructor(private router: Router) { }

  onDetails(reportId: number) {
    this.router.navigate(['/reports/view', reportId]);
  }
}
