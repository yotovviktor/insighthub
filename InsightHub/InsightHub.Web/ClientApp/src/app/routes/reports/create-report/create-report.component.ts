import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

import { ReportService } from '../../../services/report.service';
import { Report } from '../../../models/report/report';

@Component({
  selector: 'app-create-report',
  templateUrl: './create-report.component.html',
  styleUrls: ['./create-report.component.scss']
})
export class CreateReportComponent implements OnInit {

  submitted = false;
  form: FormGroup;

  constructor(
    private reportService: ReportService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private titleService: Title
  ) {
    this.titleService.setTitle('Publish new report');
   }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      summary: ['', [Validators.required, Validators.minLength(50), Validators.maxLength(1000)]],
      industry: ['', Validators.required],
      file: ['', Validators.required],
      tags: [[]]
    });
  }

  async onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    const request: Report = {
      name: this.form.value.name,
      summary: this.form.value.summary,
      industry: { id: this.form.value.industry },
      tags: this.form.value.tags?.map(tagId => {
        const tag = { id: tagId };
        return tag;
       }),
      file: this.form.value.file.files[0]
    };

    try {
      await this.reportService.createReport(request);
      this.toastr.success('Report successfully published!', 'Success');
      this.router.navigate(['/my-reports']);
    } catch (error) {
      this.toastr.error('Something went wrong!', 'Error');
    } finally {
      this.submitted = false;
    }
  }

}
