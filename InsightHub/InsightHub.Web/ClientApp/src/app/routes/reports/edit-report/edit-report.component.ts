import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportService } from 'src/app/services/report.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Report } from 'src/app/models/report/report';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-report',
  templateUrl: './edit-report.component.html',
  styleUrls: ['./edit-report.component.scss']
})
export class EditReportComponent implements OnInit {

  submitted = false;
  form: FormGroup;
  isLoading = true;

  constructor(
    private reportService: ReportService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private titleService: Title
  ) {
    this.titleService.setTitle('Update report');
  }

  async ngOnInit() {

    const report = await this.getReport();

    this.form = this.formBuilder.group({
      name: [report.name, [Validators.minLength(3), Validators.maxLength(100)]],
      summary: [report.summary, [Validators.minLength(50), Validators.maxLength(1000)]],
      industry: [report.industry.id],
      file: [],
      tags: [report.tags?.map(tag => tag.id)]
    });
  }

  private async getReport(): Promise<Report> {

    const id = +this.route.snapshot.paramMap.get('id');

    try {
      return await this.reportService.getReport(id);
    } finally {
      this.isLoading = false;
    }
  }

  async onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    const request: Report = {
      id: +this.route.snapshot.paramMap.get('id'),
      name: this.form.value.name,
      summary: this.form.value.summary,
      industry: { id: this.form.value.industry },
      tags: this.form.value.tags?.map(tagId => {
        const tag = { id: tagId };
        return tag;
       }),
      file: this.form.value.file?.files[0]
    };

    try {
      await this.reportService.updateReport(request);
      this.toastr.success('Report successfully updated!', 'Success');
      this.router.navigate(['/reports']);
    } catch (error) {
      this.toastr.error('Something went wrong!', 'Error');
    } finally {
      this.submitted = false;
    }
  }

}
