import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/services/authentication.service';
import { Claims } from 'src/app/models/claims';
import { SubscriptionService } from 'src/app/services/subscription.service';
import { Subscription } from 'src/app/models/subscription';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  @Input() industryId: number;
  isSubscribedFor: boolean;
  userClaims?: Claims;
  isCustomer: boolean;
  isAuthenticated: boolean;

  constructor(
    private subsService: SubscriptionService,
    private authService: AuthenticationService,
    private router: Router
  ) { }
  async ngOnInit(): Promise<void> {

    try {
      this.userClaims = this.authService.getClaims();
      this.isAuthenticated = (this.userClaims.isAuthenticated);

      if (this.isAuthenticated) {
        this.isCustomer = (this.userClaims?.userRole === 'customer');
        this.isSubscribedFor = await this.subsService.isSubscribedFor(this.userClaims?.userId, this.industryId);
      }
      else {
        this.isSubscribedFor = false;
        this.isCustomer = false;
      }

    }
    catch (error) {
      this.isAuthenticated = false;
      this.isCustomer = false;
      this.isSubscribedFor = false;
    }
  }

  async subscribe(): Promise<void> {
    await this.subsService.subscribe(this.userClaims?.userId, this.industryId);
    this.isSubscribedFor = true;
  }

  async cancel(): Promise<void> {
    await this.subsService.cancelSubscription(this.userClaims.userId, this.industryId);
    this.isSubscribedFor = false;
  }

  goToLogIn() {
    this.router.navigate(['login']);
  }
}

