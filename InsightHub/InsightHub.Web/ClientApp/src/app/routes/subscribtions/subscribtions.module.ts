import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';

import { SubscriptionComponent } from './subscription/subscription.component';
import { SubscriptionsListComponent } from './subscriptions-list/subscriptions-list.component';
import { MatCardModule } from '@angular/material/card';




@NgModule({
  declarations: [SubscriptionComponent, SubscriptionsListComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSortModule,
    MatIconModule,
    MatTooltipModule,
    MatCardModule,
  ],
  exports: [
    SubscriptionComponent,
    SubscriptionsListComponent
  ]
})
export class SubscribtionsModule { }
