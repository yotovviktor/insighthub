import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { SubscriptionService } from 'src/app/services/subscription.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SubscriptionsDatasource } from 'src/app/data-sources/subscriptions.datasource';

@Component({
  selector: 'app-subscriptions-list',
  templateUrl: './subscriptions-list.component.html',
  styleUrls: ['./subscriptions-list.component.scss']
})
export class SubscriptionsListComponent implements OnInit {

  @Input() userId?: number;
  dataSource: SubscriptionsDatasource;
  columnsToDisplay = ['industry', 'actions'];

  constructor(
    private authService: AuthenticationService,
    private subsService: SubscriptionService,
    private router: Router
  ) { }

  private loadSubscriptions() {
    this.dataSource.loadSubscriptions();
  }


  ngOnInit() {
    if (!this.userId) {
      this.setUserId();
    }

    this.dataSource = new SubscriptionsDatasource(this.subsService, this.userId);
    this.loadSubscriptions();
  }

  setUserId() {
    const claims = this.authService.getClaims();

    if (claims) {

      this.userId = claims.userId;
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  async onCancel(industryId: number): Promise<void> {
    await this.subsService.cancelSubscription(this.userId, industryId);
    this.loadSubscriptions();
  }

  async onDetails(industryId: number): Promise<void> {
    this.router.navigate(['/reports'], { queryParams: { industry: industryId } });
  }
}
