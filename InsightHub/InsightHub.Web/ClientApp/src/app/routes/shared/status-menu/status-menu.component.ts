import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Report } from 'src/app/models/report/report';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-status-menu',
  templateUrl: './status-menu.component.html',
  styleUrls: ['./status-menu.component.scss']
})
export class StatusMenuComponent {

  icon: string;
  color: string;
  status: string;
  submitted = false;

  @Output() changed = new EventEmitter<string>();

  // tslint:disable-next-line: variable-name
  private _report: Report;

  constructor(private reportService: ReportService) { }

  @Input()
  set report(report: Report) {
    this._report = report;
    this.status = report.status;
    this.updateIcon();
  }

  async onSelected(status: string) {
    this.submitted = true;

    try {
      await this.reportService.setStatus(this._report.id, status);

      this.changed.emit(status);
      this.status = status;
      this.updateIcon();

    } finally {
      this.submitted = false;
    }

  }

  private updateIcon() {
    switch (this.status) {
      case 'Approved': {
        this.icon = 'check_circle';
        this.color = 'primary';
        break;
      }

      case 'Rejected': {
        this.icon = 'remove_circle';
        this.color = 'warn';
        break;
      }

      case 'Pending': {
        this.icon = 'help';
        this.color = 'accent';
        break;
      }
    }
  }
}
