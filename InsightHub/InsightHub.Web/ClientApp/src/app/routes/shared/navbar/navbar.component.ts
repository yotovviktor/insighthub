import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { Claims } from 'src/app/models/claims';
import { NavbarService } from 'src/app/services/navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  claims: Claims;
  constructor(private authService: AuthenticationService, private router: Router, public navbarService: NavbarService) { }

  ngOnInit() {
  }

  get role(): string {
    if (!this.isLogedIn) {
      return undefined;
    }
    else {
      if (this.claims.userRole === 'customer'
        || this.claims.userRole === 'author'
        || this.claims.userRole === 'admin') {
        return this.claims.userRole;
      }
      else {
        return undefined;
      }
    }
  }

  get isLogedIn(): boolean {
    this.claims = this.authService.getClaims();
    if (!this.claims || !this.claims.isAuthenticated) {
      return false;
    }
    else {
      return true;
    }
  }

}
