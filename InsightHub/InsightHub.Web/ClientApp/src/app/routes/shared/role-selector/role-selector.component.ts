import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-role-selector',
  templateUrl: './role-selector.component.html',
  styleUrls: ['./role-selector.component.scss']
})
export class RoleSelectorComponent {

  roles = ['customer', 'author'];

  @Output() selected = new EventEmitter<string>();

  onSelected(selection: MatSelectChange) {
    const role: string = selection.value;
    this.selected.emit(role);
  }

}
