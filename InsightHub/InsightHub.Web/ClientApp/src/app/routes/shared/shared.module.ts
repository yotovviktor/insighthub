import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';

import { StatusSelectorComponent } from './status-selector/status-selector.component';
import { RoleSelectorComponent } from './role-selector/role-selector.component';
import { StatusChipComponent } from './status-chip/status-chip.component';
import { MatChipsModule } from '@angular/material/chips';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { StatusMenuComponent } from './status-menu/status-menu.component';

@NgModule({
  declarations: [
    StatusSelectorComponent,
    RoleSelectorComponent,
    StatusChipComponent,
    NavbarComponent,
    ConfirmDialogComponent,
    StatusMenuComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatChipsModule,
    MatToolbarModule,
    MatDialogModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    NavbarComponent,
    RoleSelectorComponent,
    StatusChipComponent,
    StatusSelectorComponent,
    ConfirmDialogComponent,
    StatusMenuComponent
  ]
})
export class SharedModule { }
