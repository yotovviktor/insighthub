import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-status-selector',
  templateUrl: './status-selector.component.html',
  styleUrls: ['./status-selector.component.scss']
})
export class StatusSelectorComponent {


  statuses = ['approved', 'pending', 'rejected'];
  @Output() selected = new EventEmitter<string>();

  onSelected(selection: MatSelectChange) {
    const status: string = selection.value;
    this.selected.emit(status);
  }
}
