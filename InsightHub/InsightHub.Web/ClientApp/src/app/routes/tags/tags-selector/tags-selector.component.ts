import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { MatSelectChange } from '@angular/material/select';

import { TagService } from '../../../services/tag.service';
import { Tag } from '../../../models/tag';

@Component({
  selector: 'app-tags-selector',
  templateUrl: './tags-selector.component.html',
  styleUrls: ['./tags-selector.component.scss']
})
export class TagsSelectorComponent implements OnInit {

  tagList: Tag[];
  @Input() selectedTagIds: number[];
  @Output() selected = new EventEmitter<number[]>();

  @Input() form: FormGroup;

  constructor(private tagService: TagService) { }

  ngOnInit() {
    this.getTags();
  }

  private async getTags() {
    this.tagList = await this.tagService.getTags();
  }

  onSelected(selection: MatSelectChange) {
    const tagIds: number[] = selection.value;
    this.selected.emit(tagIds);
  }
}
