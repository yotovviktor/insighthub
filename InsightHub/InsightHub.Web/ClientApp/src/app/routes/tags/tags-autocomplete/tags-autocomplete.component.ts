import { ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit, Input } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { TagService } from 'src/app/services/tag.service';
import { Tag } from 'src/app/models/tag';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-tags-autocomplete',
  templateUrl: './tags-autocomplete.component.html',
  styleUrls: ['./tags-autocomplete.component.scss']
})
export class TagsAutocompleteComponent implements OnInit {

  separatorKeysCodes: number[] = [ENTER];

  filteredTags: Tag[] = [];
  selectedTags: Tag[] = [];
  allTags: Tag[];

  @Input() control: FormControl;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  constructor(private tagService: TagService) {}

  private async getTags() {
    this.allTags = await this.tagService.getTags();
    this.filteredTags = this.allTags.map(tag => tag);
    this.selectedTags = this.allTags.filter(tag => this.control.value.includes(tag.id));
  }

  ngOnInit() {
    this.getTags();
  }

  get availableTags() {
    return this.allTags.filter(tag => !this.selectedTags.includes(tag));
  }

  onTagSelected(event: MatAutocompleteSelectedEvent): void {
    const tag = event.option.value as Tag;
    this.addTag(tag);

    this.tagInput.nativeElement.value = '';
  }

  async onTagAdded(event: MatChipInputEvent): Promise<void> {
    const tagName = event.value.trim();

    const isAlreadySelected = this.selectedTags.filter(tag => tag.name === tagName).length > 0;
    if (isAlreadySelected) {
      return;
    }

    if (tagName) {
      let tag = this.availableTags.filter(t => t.name.toLowerCase() === tagName)?.[0];

      if (!tag) {
        tag = await this.createTag(tagName);
      }

      this.addTag(tag);
    }

    this.tagInput.nativeElement.value = '';
  }

  private async createTag(tagName: string): Promise<Tag> {
    tagName = tagName.charAt(0).toUpperCase() + tagName.toLowerCase().slice(1);

    const tag = await this.tagService.createTag({name: tagName});
    this.allTags.push(tag);
    this.allTags.sort();

    return tag;
  }

  private addTag(tag: Tag) {
    this.selectedTags.push(tag);
    this.control.value.push(tag.id);

    this.filteredTags = this.availableTags;
  }

  removeTag(tag: Tag): void {
    this.selectedTags = this.selectedTags.filter(t => t !== tag);

    const controlValue = this.control.value.filter(value => value !== tag.id);
    this.control.setValue(controlValue);
  }

  filterTags() {
    const keyword = this.tagInput.nativeElement.value;

    this.filteredTags = this.availableTags.filter(
      tag => tag.name.toLowerCase().startsWith(keyword.toLowerCase())
    );
  }
}

