import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MatSelectModule} from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { AppRoutingModule } from '../../app-routing.module';
import { TagsSelectorComponent } from './tags-selector/tags-selector.component';
import { TagsAutocompleteComponent } from './tags-autocomplete/tags-autocomplete.component';

@NgModule({
  declarations: [TagsSelectorComponent, TagsAutocompleteComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    ],
  exports: [
    TagsSelectorComponent,
    TagsAutocompleteComponent
  ]
})
export class TagsModule { }
