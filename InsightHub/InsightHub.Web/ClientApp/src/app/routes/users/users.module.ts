import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndustriesModule } from '../industries/industries.module';
import { TagsModule } from '../tags/tags.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { ReactiveFormsModule } from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';


import { UserListComponent } from './user-list/user-list.component';
import { SharedModule } from '../shared/shared.module';
import { TakeActionsOnUserComponent } from './take-actions-on-user/take-actions-on-user.component';
import { AccountComponent } from './account/account.component';
import { UserEditBaseComponent } from './account/user-edit-base/user-edit-base.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { UserEditPasswordComponent } from './account/user-edit-password/user-edit-password.component';


@NgModule({
  declarations: [UserListComponent, TakeActionsOnUserComponent, AccountComponent, UserEditBaseComponent, UserEditPasswordComponent],
  imports: [
    CommonModule,
    IndustriesModule,
    TagsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSortModule,
    SharedModule,
    MatSelectModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatGridListModule,
  ],
  exports: [
    UserListComponent
  ]
})
export class UsersModule { }
