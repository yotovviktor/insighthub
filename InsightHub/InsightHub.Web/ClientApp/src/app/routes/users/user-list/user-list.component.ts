import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Sort } from '@angular/material/sort';

import { UserSortField } from 'src/app/models/user/user-sort-field';
import { Order } from 'src/app/models/order.type';
import { UserFilter } from 'src/app/models/user/user-filter';
import { UsersDatasource } from 'src/app/data-sources/users.datasource';
import { UserService } from 'src/app/services/user.service';

export class ChipColor {
  name: string;
  color: string;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  sortField: UserSortField;
  order: Order;
  filter: UserFilter = {};

  dataSource: UsersDatasource;
  columnsToDisplay = ['firstName', 'lastName', 'email', 'phoneNumber', 'registeredOn', 'status', 'role', 'actions'];

  constructor(private userService: UserService) { }

  private loadUsers() {
    this.dataSource.LoadUsers(this.filter, this.sortField, this.order);
  }

  ngOnInit() {
    this.dataSource = new UsersDatasource(this.userService);
    this.loadUsers();
  }

  onStatusSelect(status: string) {
    if (status) {
      this.filter.status = status;
    } else {
      this.filter.status = null;
    }

    this.loadUsers();
  }

  onRoleSelect(role: string) {
    if (role) {
      this.filter.role = role;
    } else {
      this.filter.role = null;
    }

    this.loadUsers();
  }

  sortUsers(sort: Sort) {
    this.order = sort.direction as Order;
    this.sortField = sort.active as UserSortField;

    this.loadUsers();
  }

  onNameSelected(event: Event) {
    this.filter.keyword = (event.target as HTMLInputElement).value;
    this.loadUsers();
  }
}
