import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user/user';

@Component({
  selector: 'app-take-actions-on-user',
  templateUrl: './take-actions-on-user.component.html',
  styleUrls: ['./take-actions-on-user.component.scss']
})
export class TakeActionsOnUserComponent {

  @Input() user: User;
  actions = ['Ban', 'Approve', 'Reject', 'Enable'];

  constructor(private userService: UserService) { }

  onSelected(action: string) {
    this.takeAction(action);
  }

  private async takeAction(action: string) {

    switch (action) {
      case 'Ban':
        await this.userService.setUserLockOut(this.user.id, true);
        this.user.isDisabled = true;
        break;
      case 'Enable':
        await this.userService.setUserLockOut(this.user.id, false);
        this.user.isDisabled = false;
        break;
      case 'Approve':
        await this.userService.setUserStatus(this.user.id, 'Approved');
        this.user.status = 'Approved';
        break;
      case 'Reject':
        await this.userService.setUserStatus(this.user.id, 'Rejected');
        this.user.status = 'Rejected';
        break;
    }

  }
}
