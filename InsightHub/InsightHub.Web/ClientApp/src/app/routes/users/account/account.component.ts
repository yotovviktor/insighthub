import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'src/app/models/user/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  properties = ['Email Address', 'First Name', 'Last Name', 'Phone Number', 'Password'];
  userId: number;
  user: User;

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.user = await this.getUser();
  }

  async getUser(): Promise<User> {

    const claims = this.authService.getClaims();

    if (claims) {

      this.userId = claims.userId;
      return await this.userService.getUserById(this.userId);
    }
    else {
      this.router.navigate(['/login']);
    }
  }

}


