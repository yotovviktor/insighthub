import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-edit-base',
  templateUrl: './user-edit-base.component.html',
  styleUrls: ['./user-edit-base.component.scss']
})
export class UserEditBaseComponent implements OnInit {

  hide = true;
  submitted = false;
  form: FormGroup;
  @Input() propertyName: string;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      param: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  get f() { return this.form.controls; }

  async onSubmit(): Promise<void> {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    await this.userService.editUserBase(this.form, this.propertyName);
  }

}
