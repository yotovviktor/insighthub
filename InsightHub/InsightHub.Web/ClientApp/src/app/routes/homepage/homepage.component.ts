import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { Report } from 'src/app/models/report/report';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  isLoading = true;

  featuredReports: Report[];
  latestReports: Report[];
  popularReports: Report[];

  constructor(private reportService: ReportService) { }

  async ngOnInit(): Promise<void> {
    this.featuredReports = await this.reportService.getFeaturedReports();
    this.latestReports = await this.reportService.getLatestReports();
    this.popularReports = await this.reportService.getPopularReports();

    this.isLoading = false;
  }

}
