import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavbarService } from 'src/app/services/navbar.service';

@Component({
  selector: 'app-intro-homepage',
  templateUrl: './intro-homepage.component.html',
  styleUrls: ['./intro-homepage.component.scss']
})
export class IntroHomepageComponent implements OnDestroy {

  constructor(public navbarService: NavbarService) {
    this.navbarService.showNavbar = false;
  }

  ngOnDestroy(): void {
    this.navbarService.showNavbar = true;
  }


}
