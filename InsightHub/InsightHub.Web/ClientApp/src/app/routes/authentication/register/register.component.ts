import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  submitted = false;
  form: FormGroup;
  passwordConfirmed = false;
  passwordChecked = false;
  isEmailClicked = false;
  isPasswordClicked = false;

  constructor(
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirm: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      role: ['', Validators.required],
      cookiePolicy: [, Validators.required]
    });
  }

  get f() { return this.form.controls; }

  comparePasswords() {
    if (this.f.password.value === this.f.passwordConfirm.value) {
      this.passwordConfirmed = true;
    }
  }

  isPasswordValid() {
    const password = this.f.password.value as string;
    const decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (password.match(decimal)) {
      this.passwordChecked = true;
    }
  }

  async onSubmit(): Promise<void> {

    this.submitted = true;

    this.comparePasswords();

    if (this.form.invalid || !this.passwordConfirmed) {
      return;
    }

    this.isPasswordValid();

    if (!this.passwordChecked) {
      return;
    }
    try {
      await this.authService.register(this.form.value);
      this.toastr.info('Please confirm your email adress and wait until you are approved');
      this.router.navigate(['login']);
    }
    catch (error) {
      this.toastr.error('Please provide valid data', 'Error');
      return;
    }
  }

  emailClicked() {
    this.isEmailClicked = true;
  }

  passwordClicked() {
    this.isPasswordClicked = true;
  }
}
