import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  submitted = false;
  form: FormGroup;

  constructor(
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      remmemberLogin: [true],
    });
  }

  get f() { return this.form.controls; }

  async onSubmit(): Promise<void> {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
    try {
      await this.authService.login(this.form.value);
      this.redirectToDashboard();
    }
    catch (error) {
      this.toastr.error('Incorrect email and/or password');
    }
  }

  redirectToDashboard() {
    const claims = this.authService.getClaims();
    if (!claims || claims.isAuthenticated !== true) {
      this.router.navigate(['/login']);
    }
    else {
      switch (claims.userRole) {
        case 'customer': this.router.navigate(['/customer']); break;
        case 'author': this.router.navigate(['/author']); break;
        case 'admin': this.router.navigate(['/admin']); break;
      }
    }
  }

}
