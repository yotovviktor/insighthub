import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './routes/authentication/authentication.module';
import { IndustriesModule } from './routes/industries/industries.module';
import { ReportsModule } from './routes/reports/reports.module';
import { TagsModule } from './routes/tags/tags.module';
import { UsersModule } from './routes/users/users.module';
import { SharedModule } from './routes/shared/shared.module';
import { SubscribtionsModule } from './routes/subscribtions/subscribtions.module';
import { DashboardModule } from './routes/dashboard/dashboard.module';
import { HomepageComponent } from './routes/homepage/homepage.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { IntrohomepageModule } from './routes/introhomepage/introhomepage.module';
import { NavbarComponent } from './routes/shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
  ],
  imports: [
    AppRoutingModule,
    MatButtonModule,
    MatTabsModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AuthenticationModule,
    IndustriesModule,
    ReportsModule,
    TagsModule,
    UsersModule,
    SharedModule,
    SubscribtionsModule,
    DashboardModule,
    IntrohomepageModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
