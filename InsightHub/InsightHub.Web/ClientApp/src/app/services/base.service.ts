import { HttpClient, HttpParams } from '@angular/common/http';

export class BaseService<Model> {

  protected baseUrl = 'api';

  constructor(protected http: HttpClient) { }

  protected async list(path: string, queryParams: HttpParams = new HttpParams()): Promise<Model[]> {
    const url = `${this.baseUrl}/${path}`;
    const options = { params: queryParams };

    return this.http.get<Model[]>(url, options).toPromise();
  }

  protected async listPaginated(path: string, queryParams: HttpParams = new HttpParams()): Promise<[Model[], number]> {
    const url = `${this.baseUrl}/${path}`;

    return new Promise((resolve, reject) => {
      this.http.get(url, { observe: 'response', params: queryParams })
        .subscribe(
          response => {
            const reports = response.body as Model[];
            const totalCount = +response.headers.get('X-TOTAL-COUNT');

            resolve([reports, totalCount]);
          },
          error => reject(error)
        );
      });
  }

  protected async update<BodyType>(path: string, body: BodyType): Promise<void> {
    const url = `${this.baseUrl}/${path}`;

    await this.http.put<BodyType>(url, body).toPromise();
  }

  protected async get(path: string, id: number): Promise<Model> {
    const url = `${this.baseUrl}/${path}/${id}`;

    return this.http.get<Model>(url).toPromise();
  }

  protected async create(path: string, request: Model): Promise<Model> {
    const url = `${this.baseUrl}/${path}`;

    return this.http.post<Model>(url, request).toPromise();
  }

  protected async delete(path: string, id: number): Promise<Model> {
    const url = `${this.baseUrl}/${path}/${id}`;

    return this.http.delete<Model>(url).toPromise();
  }
}
