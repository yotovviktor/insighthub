import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

import { BaseService } from './base.service';
import { User } from '../models/user/user';
import { UserFilter } from '../models/user/user-filter';
import { UserSortField } from '../models/user/user-sort-field';
import { Order } from '../models/order.type';
import { StatusModel } from '../models/user/status-model';
import { UserLockout } from '../models/user/user-lockout';
import { AuthenticationService } from './authentication.service';
import { UserEditModel } from '../models/user/user-edit-model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class UserService extends BaseService<User> {

  private userUrl = 'users';

  constructor(
    protected http: HttpClient,
    private authService: AuthenticationService,
    private router: Router) {
    super(http);
  }

  async getUsers(filter?: UserFilter, sortField?: UserSortField, order: Order = 'desc'): Promise<User[]> {
    let queryParams = new HttpParams();

    if (sortField) {
      queryParams = queryParams.set('sortby', sortField.toString());
    }

    if (order) {
      queryParams = queryParams.set('orderby', order.toString());
    }

    if (filter) {
      if (filter.keyword) {
        queryParams = queryParams.set('keyWord', filter.keyword.toString());
      }

      if (filter.status) {
        queryParams = queryParams.set('status', filter.status.toString());
      }

      if (filter.role) {
        queryParams = queryParams.set('role', filter.role.toString());
      }
    }

    return this.list(this.userUrl + '/filter', queryParams);
  }

  async setUserStatus(id: number, status: string): Promise<void> {
    const statusModel = new StatusModel();
    statusModel.status = status;
    return this.update<StatusModel>(this.userUrl + '/' + id.toString() + '/status', statusModel);
  }

  async setUserLockOut(id: number, lockOutEnable: boolean): Promise<void> {
    const lockOutModel = new UserLockout();
    lockOutModel.lockOutEnable = lockOutEnable;

    if (lockOutEnable) {
      lockOutModel.lockOutPeriod = 14;
    }

    return this.update<UserLockout>(this.userUrl + '/' + id.toString() + '/lockout', lockOutModel);
  }

  async getUserById(id: number): Promise<User> {
    return this.get(this.userUrl, id);
  }

  async editUserBase(form: FormGroup, propetryName: string): Promise<void> {
    const claims = this.authService.getClaims();
    let id: number;

    if (claims && claims.isAuthenticated) {
      id = claims.userId;
    }
    else{
      this.router.navigate(['/login']);
    }

    switch (propetryName) {
      case 'Email Address':
        await this.editEmail(form, id);
        break;
      case 'First Name':
        await this.editFirstName(form, id);
        break;
      case 'Last Name':
        await this.editLastName(form, id);
        break;
      case 'Phone Name':
        await this.editPhoneNumber(form, id);
        break;
      case 'Password':
        await this.editPassword(form, id);
        break;
    }
  }
  private async editEmail(form: FormGroup, id: number) {
    const userEditModel = new UserEditModel();
    userEditModel.email = form.controls.param.value;
    userEditModel.password = form.controls.password.value;

    return this.update<UserEditModel>(`${this.userUrl}/${id.toString()}/email`, userEditModel);
  }

  private async editPhoneNumber(form: FormGroup, id: number) {
    const userEditModel = new UserEditModel();
    userEditModel.phoneNumber = form.controls.param.value;
    userEditModel.password = form.controls.password.value;

    return  this.update<UserEditModel>(`${this.userUrl}/${id.toString()}/phonenumber`, userEditModel);
  }

  private async editFirstName(form: FormGroup, id: number) {
    const userEditModel = new UserEditModel();
    userEditModel.name = form.controls.param.value;
    userEditModel.password = form.controls.password.value;

    return this.update<UserEditModel>(`${this.userUrl}/${id.toString()}/firstname`, userEditModel);
  }

  private async editLastName(form: FormGroup, id: number) {
    const userEditModel = new UserEditModel();
    userEditModel.name = form.controls.param.value;
    userEditModel.password = form.controls.password.value;

    return this.update<UserEditModel>(`${this.userUrl}/${id.toString()}/lastname`, userEditModel);
  }

  private async editPassword(form: FormGroup, id: number) {
    const userEditModel = new UserEditModel();
    userEditModel.password = form.controls.password.value;
    userEditModel.newPassword = form.controls.newPassword.value;
    userEditModel.confirmNewPassword = form.controls.confirmNewPassword.value;

    return this.update<UserEditModel>(`${this.userUrl}/${id.toString()}/password`, userEditModel);
  }
}

