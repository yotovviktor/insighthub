import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Industry } from '../models/industry';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class IndustryService extends BaseService<Industry> {

  private industriesPath = 'industries';

  constructor(protected http: HttpClient) {
    super(http);
  }

  async getIndustry(id: number): Promise<Industry> {
    return this.get(this.industriesPath, id);
  }

  async getIndustries(): Promise<Industry[]> {
    return this.list(this.industriesPath);
  }
}
