import { Injectable, Input } from '@angular/core';
import { BaseService } from './base.service';
import { Subscription } from '../models/subscription';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService extends BaseService<Subscription> {

  url = 'users';

  constructor(http: HttpClient) {
    super(http);
  }

  async subscribe(userId: number, industryId: number): Promise<void> {
    const subscribeUrl = `${this.url}/${userId}/subscriptions/${industryId}`;
    await this.update<void>(subscribeUrl, null);
  }

  async cancelSubscription(userId: number, industryId: number): Promise<void> {
    const subscribeUrl = `api/${this.url}/${userId}/subscriptions/${industryId}`;
    await this.http.delete(subscribeUrl).toPromise();
  }

  async isSubscribedFor(userId: number, industryId: number): Promise<boolean> {
    return await this.http.get<boolean>(`api/users/${userId}/subscription/${industryId}`).toPromise();
  }

  async listSubscriptions(userId: number): Promise<Subscription[]> {
    const listUrl = `${this.url}/${userId}/subscriptions`;
    return await this.list(listUrl);
  }

}
