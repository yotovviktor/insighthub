import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService extends BaseService<Tag> {

  private tagsUrl = 'tags';

  constructor(protected http: HttpClient) {
    super(http);
  }

  async getTags(): Promise<Tag[]> {
    return this.list(this.tagsUrl);
  }

  async createTag(tag: Tag) {
    return this.create(this.tagsUrl, tag);
  }
}
