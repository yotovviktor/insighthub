import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { blobToBase64 } from 'base64-blob';

import { BaseService } from './base.service';
import { Report } from '../models/report/report';
import { ReportsFilter } from '../models/report/report.filter';
import { ReportSortField } from '../models/report/sort-field.type';
import { Order } from '../models/order.type';
import { Paging } from '../models/paging';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends BaseService<Report>{

  private reportsPath = 'reports';

  constructor(protected http: HttpClient) {
    super(http);
  }

  async getReport(id: number): Promise<Report> {
    return this.get(this.reportsPath, id);
  }

  async getDownloadUrl(id: number) {
    return `${this.baseUrl}/${this.reportsPath}/${id}/content`;
  }

  async createReport(report: Report) {
    const data: any = await this.toRequest(report);
    return this.create(this.reportsPath, data);
  }

  async updateReport(report: Report) {
    const data: any = await this.toRequest(report);
    const path = `${this.reportsPath}/${report.id}`;
    return this.update(path, data);
  }

  async deleteReport(id: number): Promise<Report> {
    return this.delete(this.reportsPath, id);
  }

  async setFeatured(id: number, featured: boolean) {
    const path = `${this.reportsPath}/${id}/featured`;
    return this.update(path, { isFeatured: featured });
  }

  async setStatus(id: number, status: string) {
    const path = `${this.reportsPath}/${id}/status`;
    return this.update(path, { status });
  }

  private async toRequest(report: Report) {
    const fileContent = report.file ? await blobToBase64(report.file) : null;

    return {
      name: report.name,
      summary: report.summary,
      industryId: report.industry.id,
      tagIds: report.tags?.map(tag => tag.id),
      content: fileContent,
      contentType: report.file?.type
    };
  }

  async getAuthorReports(authorId: number, filter?: ReportsFilter,
                         sortField?: ReportSortField, order: Order = 'desc', paging?: Paging):
    Promise<[Report[], number]> {

    const path = `users/${authorId}/${this.reportsPath}`;
    const queryParams = this.getQueryParams(filter, sortField, order, paging);
    return this.listPaginated(path, queryParams);
  }

  async getCustomerDownloads(customerId: number, filter?: ReportsFilter,
                             sortField?: ReportSortField, order: Order = 'desc', paging?: Paging):
    Promise<[Report[], number]> {

    const path = `users/${customerId}/downloads`;
    const queryParams = this.getQueryParams(filter, sortField, order, paging);
    return this.listPaginated(path, queryParams);
  }

  async getAllReports(filter?: ReportsFilter, sortField?: ReportSortField,
                      order: Order = 'desc', paging?: Paging ): Promise<[Report[], number]> {

    const queryParams = this.getQueryParams(filter, sortField, order, paging);
    return this.listPaginated(this.reportsPath, queryParams);
  }

  async getFeaturedReports(): Promise<Report[]> {

    const filter = { featured: true, status: 'approved' };
    const sortField = 'date';
    const order = 'desc';
    const paging = { limit: 3 };

    return this.getReports(this.reportsPath, filter, sortField, order, paging);
  }

  async getLatestReports(): Promise<Report[]> {

    const filter = { status: 'approved' };
    const sortField = 'date';
    const order = 'desc';
    const paging = { limit: 3 };

    return this.getReports(this.reportsPath, filter, sortField, order, paging);
  }

  async getPopularReports(): Promise<Report[]> {

    const filter = { status: 'approved' };
    const sortField = 'downloads';
    const order = 'desc';
    const paging = { limit: 3 };

    return this.getReports(this.reportsPath, filter, sortField, order, paging);
  }

  private async getReports(path: string, filter?: ReportsFilter, sortField?: ReportSortField,
                           order: Order = 'desc', paging?: Paging): Promise<Report[]> {

    const queryParams = this.getQueryParams(filter, sortField, order, paging);
    return this.list(path, queryParams);
  }

  private getQueryParams(
      filter?: ReportsFilter, sortField?: ReportSortField,
      order: Order = 'desc', paging?: Paging
  ): HttpParams {
    let queryParams = new HttpParams();

    if (sortField) {
      queryParams = queryParams.set('sortby', sortField.toString());
    }

    if (order) {
      queryParams = queryParams.set('order', order.toString());
    }

    if (paging) {
      if (paging.limit) {
        queryParams = queryParams.set('limit', paging.limit.toString());
      }

      if (paging.offset) {
        queryParams = queryParams.set('offset', paging.offset.toString());
      }
    }

    if (filter) {
      if (filter.name) {
        queryParams = queryParams.set('name', filter.name);
      }

      if (filter.featured) {
        queryParams = queryParams.set('featured', filter.featured.toString());
      }

      if (filter.industryId) {
        queryParams = queryParams.set('industry', filter.industryId.toString());
      }

      if (filter.status) {
        queryParams = queryParams.set('status', filter.status);
      }

      if (filter.tagIds) {
        filter.tagIds.forEach(id => {
          queryParams = queryParams.append('tags', id.toString());
        });
      }
    }

    return queryParams;
  }
}
