import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoginModel } from '../models/authentication-folder/login';
import { RegisterModel } from '../models/authentication-folder/register';
import { Cookie } from '../models/authentication-folder/cookie';
import { Claims } from '../models/claims';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(protected http: HttpClient, private toastr: ToastrService) { }

  private errorHandler(errorResponse: HttpErrorResponse): string {

    if (errorResponse.error.status === 400 || errorResponse.error.status === 404) {
      return 'Incorect email and/or password';
    }
    return 'There is a problem and we are working on it. Try again latter';
  }


  public async login(model: LoginModel): Promise<void> {
    try {
      await this.http.post<LoginModel>('api/auth/login', model).toPromise();
      const cookie = await this.getCookie();
      localStorage.setItem('claims', JSON.stringify(cookie));
    }
    catch (error) {
      this.toastr.error('Incorrect email and/or password', 'Error');
    }
  }

  public getClaims(): Claims {
    const jsonClaims = localStorage.getItem('claims');

    if (jsonClaims) {
      const claims = JSON.parse(jsonClaims);
      return {
        isAuthenticated: claims.isAuthenticated,
        userId: claims.id,
        userRole: claims.role
      };
    }
  }

  public async register(model: RegisterModel): Promise<RegisterModel> {
    return this.http.post<RegisterModel>('api/auth/register', model).toPromise();
  }

  public async logout(): Promise<RegisterModel> {
    localStorage.clear();

    return this.http.post<RegisterModel>('api/auth/logout', null).toPromise();
  }

  public async getCookie(): Promise<Cookie> {
    let cookie = new Cookie();
    try {
      cookie = await this.http.get<Cookie>('api/auth/cookie').toPromise();

      if (cookie.id && cookie.role) {
        cookie.isAuthenticated = true;
      }
      else {
        cookie.isAuthenticated = false;
      }
    }
    catch (error) {
      cookie.isAuthenticated = false;
    }

    return cookie;
  }
}

