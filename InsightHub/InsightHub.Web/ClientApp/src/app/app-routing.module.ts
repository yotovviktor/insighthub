import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndustryListComponent } from './routes/industries/industry-list/industry-list.component';
import { ReportListComponent } from './routes/reports/report-list/report-list.component';
import { ReportDetailComponent } from './routes/reports/report-detail/report-detail.component';
import { LoginComponent } from './routes/authentication/login/login.component';
import { RegisterComponent } from './routes/authentication/register/register.component';
import { LogoutComponent } from './routes/authentication/logout/logout.component';
import { UserListComponent } from './routes/users/user-list/user-list.component';
import { CreateReportComponent } from './routes/reports/create-report/create-report.component';
import { AuthorReportsComponent } from './routes/dashboard/author-reports/author-reports.component';
import { AccountComponent } from './routes/users/account/account.component';
import { UserEditBaseComponent } from './routes/users/account/user-edit-base/user-edit-base.component';
import { EditReportComponent } from './routes/reports/edit-report/edit-report.component';
import { SubscriptionsListComponent } from './routes/subscribtions/subscriptions-list/subscriptions-list.component';
import { CustomersDashboardComponent } from './routes/dashboard/customers-dashboard/customers-dashboard.component';
import { AuthorDashboardComponent } from './routes/dashboard/author-dashboard/author-dashboard.component';
import { AdminDashboardComponent } from './routes/dashboard/admin-dashboard/admin-dashboard.component';
import { HomepageComponent } from './routes/homepage/homepage.component';
import { IntroHomepageComponent } from './routes/introhomepage/intro-homepage/intro-homepage.component';

const routes: Routes = [
  { path: 'home', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'users', component: UserListComponent },
  { path: 'industries', component: IndustryListComponent },
  { path: 'reports', component: ReportListComponent },
  { path: 'reports/create', component: CreateReportComponent },
  { path: 'reports/edit/:id', component: EditReportComponent },
  { path: 'reports/view/:id', component: ReportDetailComponent },
  { path: 'my-reports', component: AuthorReportsComponent },
  { path: 'account', component: AccountComponent },
  { path: 'edit', component: UserEditBaseComponent },
  { path: 'mysubs', component: SubscriptionsListComponent },
  { path: 'customer', component: CustomersDashboardComponent },
  { path: 'author', component: AuthorDashboardComponent },
  { path: 'admin', component: AdminDashboardComponent },
  { path: '**', component: IntroHomepageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
