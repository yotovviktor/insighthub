export interface Industry {
  id: number;
  name?: string;
  imageUrl?: string;
  description?: string;
}
