export class Cookie {
  id: number;
  role: string;
  expirationDate: Date;
  isAuthenticated: boolean;
}
