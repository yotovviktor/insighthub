export class Claims {
  userId: number;
  userRole: string;
  isAuthenticated: boolean;
}
