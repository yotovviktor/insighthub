import { Industry } from '../industry';
import { Tag } from '../tag';
import { User } from '../user/user';

export interface Report {
  id?: number;
  name?: string;
  summary?: string;
  author?: User;
  industry?: Industry;
  status?: string;
  isFeatured?: boolean;
  downloadCount?: number;
  createdOn?: Date;
  tags?: Tag[];

  file?: File;
}
