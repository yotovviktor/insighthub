export interface ReportsFilter {
  name?: string;
  industryId?: number;
  status?: string;
  featured?: boolean;
  tagIds?: number[];
}
