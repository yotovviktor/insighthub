export class UserEditModel {
  email?: string;
  name?: string;
  phoneNumber?: string;
  password: string;
  newPassword: string;
  confirmNewPassword: string;
}
