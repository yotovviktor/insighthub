export class User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  status: string;
  role: string;
  registeredOn: Date;
  phoneNumber: string;
  isDisabled: boolean;
  lockOutEnd: Date;
}
