﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models
{
    public class TagViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; }
    }
}