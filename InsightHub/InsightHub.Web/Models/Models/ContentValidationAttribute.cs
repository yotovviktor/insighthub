﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace InsightHub.Web.Models.Models
{
    public class ContentTypeValidation : ValidationAttribute
    {
        public string ContentType { get; }
        public string GetErrorMessage() => $"The file type: {ContentType} is not allowed.";

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string[] allowedFiles = new string[]
            {
                "aplication/pdf", //.pdf
                "application/msword", // .doc
                "aplication/application/vnd.openxmlformats-officedocument.wordprocessingml.document", //.docx
                "text/plain", //.txt
                "application/rtf" // .rtf
            };

            if (ContentType != null && !allowedFiles.Contains(ContentType))
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
