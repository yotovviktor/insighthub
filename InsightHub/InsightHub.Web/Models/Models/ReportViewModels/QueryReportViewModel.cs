﻿using InsightHub.Common.Enums;
using InsightHub.Data.Enums;

namespace InsightHub.Web.Models.Models.ReportViewModels
{
    public class QueryReportViewModel
    {
        public string Name { get; set; }

        public int? Industry { get; set; }

        public bool? Featured { get; set; }

        public Status? Status { get; set; }

        public int[] Tags { get; set; }

        public ReportSortBy? SortBy { get; set; }

        public Order? Order { get; set; }

        public int? Limit { get; set; }

        public int? Offset { get; set; }
    }
}
