﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.Models.ReportViewModels
{
    public class ReportFeaturedViewModel
    {
        [Required]
        public bool IsFeatured { get; set; }     
    }
}
