﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace InsightHub.Web.Models.Models.ReportViewModels
{
    public class ReportCreateViewModel
    {
        [Required]
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(50), MaxLength(1000)]
        public string Summary { get; set; }

        [Required]
        public int IndustryId { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [ContentTypeValidation]
        public string ContentType { get; set; }

        public ICollection<int> TagIds { get; set; } = new List<int>();      
    }
}
