﻿using InsightHub.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models.Models.ReportViewModels
{
    public class ReportStatusViewModel
    {
        [Required]
        public Status Status { get; set; }
    }
}
