﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace InsightHub.Web.Models.Models.ReportViewModels
{
    public class ReportUpdateViewModel : IValidatableObject
    {
        [MinLength(3), MaxLength(100)]
        public string Name { get; set; }

        [MinLength(50), MaxLength(1000)]
        public string Summary { get; set; }

        public int? IndustryId { get; set; }

        public string Content { get; set; }

        [ContentTypeValidation]
        public string ContentType { get; set; }

        public ICollection<int> TagIds { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(Summary) && IndustryId == null &&
                Content == null && ContentType == null && TagIds == null)
            {
                yield return new ValidationResult("At least one of the properties should be other than null");
            }
        }
    }
}
