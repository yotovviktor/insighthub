﻿using InsightHub.Data.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace InsightHub.Web.Models.ReportViewModels
{
    public class ReportViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Summary { get; set; }  

        public IndustryViewModel Industry { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

        public bool IsFeatured { get; set; }

        public int DownloadCount { get; set; }

        public DateTime CreatedOn { get; set; }

        public ICollection<TagViewModel> Tags { get; set; } = new List<TagViewModel>();
    }
}
