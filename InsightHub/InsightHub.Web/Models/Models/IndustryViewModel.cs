﻿using System.ComponentModel.DataAnnotations;

namespace InsightHub.Web.Models
{
    public class IndustryViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string Name { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        public string Description { get; set; }
    }
}