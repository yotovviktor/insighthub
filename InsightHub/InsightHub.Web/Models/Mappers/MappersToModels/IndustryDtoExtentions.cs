﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace InsightHub.Web.Models.Mappers
{
    public static class IndustryDtoExtentions
    {
        public static IndustryViewModel ToModel(this IndustryDto industryDto)
        {
            return new IndustryViewModel
            {
                Id = industryDto.Id,
                Name = industryDto.Name,
                ImageUrl = industryDto.ImageUrl,
                Description = industryDto.Description
            };
        }

        public static ICollection<IndustryViewModel> ToModels(this IEnumerable<IndustryDto> industryDtos)
        {
            return industryDtos.Select(industry => industry.ToModel()).ToList();
        }
    }
}
