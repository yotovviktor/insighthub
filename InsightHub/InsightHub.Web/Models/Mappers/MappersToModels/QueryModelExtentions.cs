﻿using InsightHub.Common.Models;
using InsightHub.Web.Models.Models.ReportViewModels;

namespace InsightHub.Web.Models.Mappers.MappersToModels
{
    public static class QueryModelExtentions
    {
        public static FilterCriteria ToFilterModel(this QueryReportViewModel model)
        {
            return new FilterCriteria
            {
                Name = model.Name,
                IndustryId = model.Industry,
                IsFeatured = model.Featured,
                Status = model.Status,
                TagIds = model.Tags
            };
        }

        public static Paging ToPagingModel(this QueryReportViewModel model)
        {
            return new Paging
            {
                Limit = model.Limit,
                Offset= model.Offset,
            };
        }
    }
}
