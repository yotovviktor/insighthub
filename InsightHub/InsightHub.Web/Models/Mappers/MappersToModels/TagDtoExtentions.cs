﻿using InsightHub.Common.DtoModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Models.Mappers.MappersToModels
{
    public static class TagDtoExtentions
    {
        public static TagViewModel ToModel(this TagDto tagDto)
        {
            return new TagViewModel
            {
                Id = tagDto.Id,
                Name = tagDto.Name,
            };
        }

        public static ICollection<TagViewModel> ToModels(this IEnumerable<TagDto> tagDtos)
        {
            return tagDtos.Select(tag => tag.ToModel()).ToList();
        }
    }
}
