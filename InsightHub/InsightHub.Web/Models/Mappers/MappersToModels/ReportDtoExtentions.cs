﻿using InsightHub.Common.DtoModels;
using InsightHub.Web.Models.Mappers.MappersToModels;
using InsightHub.Web.Models.Models.ReportViewModels;
using InsightHub.Web.Models.ReportViewModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Models.Mappers
{
    public static class ReportDtoExtentions
    {
        public static ReportViewModel ToModel(this ReportDto reportDto)
        {
            return new ReportViewModel
            {
                Id = reportDto.Id,
                Name = reportDto.Name,
                Summary = reportDto.Summary,
                Industry = reportDto.Industry?.ToModel(),
                Status = reportDto.Status,
                IsFeatured = reportDto.IsFeatured,
                DownloadCount = reportDto.DownloadCount,
                CreatedOn = reportDto.CreatedOn,
                Tags = reportDto.Tags.Select(tag => tag.ToModel()).ToList()
            };
        }

        public static ICollection<ReportViewModel> ToModels(this IEnumerable<ReportDto> reportsDtos)
        {
            return reportsDtos.Select(reportDto => reportDto.ToModel()).ToList();
        }

        public static ReportDetailsViewModel ToDetailedModel(this ReportDto reportDto)
        {
            var author = new AuthorViewModel
            {
                Id = reportDto.AuthorId,
                FirstName = reportDto.Author.FirstName,
                LastName = reportDto.Author.LastName,
                RegisteredOn = reportDto.Author.CreatedOn
            };

            return new ReportDetailsViewModel
            {
                Id = reportDto.Id,
                Name = reportDto.Name,
                Summary = reportDto.Summary,
                Author = author,
                Industry = reportDto.Industry.ToModel(),
                Status = reportDto.Status,
                IsFeatured = reportDto.IsFeatured,
                DownloadCount = reportDto.DownloadCount,
                CreatedOn = reportDto.CreatedOn,
                Tags = reportDto.Tags.Select(tag => tag.ToModel()).ToList()
            };
        }
    }
}
