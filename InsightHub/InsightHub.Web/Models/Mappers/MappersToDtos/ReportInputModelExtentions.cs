﻿using InsightHub.Common.DtoModels;
using InsightHub.Web.Models.Models.ReportViewModels;
using System;
using System.IO;

namespace InsightHub.Web.Models.Mappers.MappersToDtos
{

    public static class ReportInputModelExtentions
    {

        public static ReportDto ToDto(this ReportCreateViewModel report)
        {
            var prefix = "data:application";
            var base64 = report.Content;
            if (report.Content.StartsWith(prefix))
            {
                base64 = base64.Substring(report.Content.IndexOf(',') + 1);
            }

            byte[] data = Convert.FromBase64String(base64);
            MemoryStream content = new MemoryStream(data);

            return new ReportDto
            {
                Name = report.Name,
                Summary = report.Summary,
                IndustryId = report.IndustryId,
                TagIds = report.TagIds,
                Content = content,
                ContentType = report.ContentType
             };
        }

        public static ReportDto ToDto(this ReportUpdateViewModel report)
        {
            var reportDto = new ReportDto
            {
                Name = report.Name,
                Summary = report.Summary,                
                TagIds = report.TagIds
            };

            if (report.IndustryId != null)
            {
                reportDto.IndustryId = (int)report.IndustryId;
            }

            if (report.Content != null && report.Content != null)
            {
                byte[] data = Convert.FromBase64String(report.Content);
                MemoryStream content = new MemoryStream(data);

                reportDto.Content = content;
                reportDto.ContentType = report.ContentType;
            }

            return reportDto;
        }
    }
}
