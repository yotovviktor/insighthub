﻿using InsightHub.Common.DtoModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Models.Mappers.MappersToDtos
{
    public static class TagModelExtentions
    {
        public static TagDto ToDto(this TagViewModel tag)
        {
            return new TagDto
            {
                Id = tag.Id,
                Name = tag.Name,
            };
        }

        public static ICollection<TagDto> ToDtos(this IEnumerable<TagViewModel> tags)
        {
            return tags.Select(tag => tag.ToDto()).ToList();
        }
    }
}
