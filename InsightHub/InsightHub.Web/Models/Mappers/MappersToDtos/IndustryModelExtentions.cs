﻿using InsightHub.Common.DtoModels;
using System.Collections.Generic;
using System.Linq;

namespace InsightHub.Web.Models.Mappers.MappersToDtos
{
    public static class IndustryModelExtentions
    {
        public static IndustryDto ToDto(this IndustryViewModel industry)
        {
            return new IndustryDto
            {
                Id = industry.Id,
                Name = industry.Name,
                ImageUrl = industry.ImageUrl,
                Description = industry.Description
            };
        }

        public static ICollection<IndustryDto> ToDtos(this IEnumerable<IndustryViewModel> industries)
        {
            return industries.Select(industry => industry.ToDto()).ToList();
        }
    }
}
