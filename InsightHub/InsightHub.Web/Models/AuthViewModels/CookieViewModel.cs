﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.AuthViewModels
{
    public class CookieViewModel
    {
        public int Id { get; set; }

        public string Role { get; set; }

        public DateTimeOffset ExpirationDate{ get; set; }
    }
}
