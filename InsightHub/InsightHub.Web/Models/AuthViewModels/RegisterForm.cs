﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;

namespace InsightHub.Web.Models
{
    public class RegisterForm
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public Data.Enums.Role Role { get; set; }
    }
}
