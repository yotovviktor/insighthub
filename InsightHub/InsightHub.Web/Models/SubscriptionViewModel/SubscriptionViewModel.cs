﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.SubscriptionViewModel
{
    public class SubscriptionViewModel
    {
        public int UserId { get; set; }
        public int IndustryId { get; set; }
        public string IndustryName { get; set; }
    }
}
