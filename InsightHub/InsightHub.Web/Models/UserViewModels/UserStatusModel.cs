﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Data.Enums;

namespace InsightHub.Web.Models.UserViewModels
{
    public class UserStatusModel
    {
        [Required]
        public Status Status { get; set; }
    }
}
