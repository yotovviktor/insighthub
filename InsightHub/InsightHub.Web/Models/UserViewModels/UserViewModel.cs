﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using Newtonsoft.Json.Converters;

namespace InsightHub.Web.Models.UserViewModels
{
    /// <summary>
    ///  Model that transfers brief information for a user from the Controller to the presentation layer.
    ///  Properties: Id, FirstName, LastName, Email, Status
    /// </summary>
    public class UserViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }


        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Data.Enums.Role Role { get; set; }

        public DateTimeOffset RegisteredOn { get; set; }

    }
}
