﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using InsightHub.Data.Enums;

namespace InsightHub.Web.Models.UserViewModels
{
    /// <summary>
    /// Detailed view model of the user
    /// Properties: Role, Id, IsDisabled, LockoutEnd, FIrstName, LastName, RegisteredOn, Email, PhoneNumber, Status
    /// </summary>
    public class UserDetailsViewModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Data.Enums.Role Role { get; set; }

        public int Id { get; set; }

        public bool IsDisabled { get; set; }

        public DateTimeOffset? LockoutEnd { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime RegisteredOn { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

    }
}
