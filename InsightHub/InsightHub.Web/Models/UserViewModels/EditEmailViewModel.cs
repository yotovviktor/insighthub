﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.UserViewModels
{
    public class EditEmailViewModel
    {
        [EmailAddress(ErrorMessage ="This is not a valid email type")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
