﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.UserViewModels
{
    public class LockOutViewModel
    {
        [Required]
        public bool LockOutEnable { get; set; }

        public int? LockOutPeriod { get; set; }
    }
}
