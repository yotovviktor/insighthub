﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;

namespace InsightHub.Web.Models.UserViewModels
{
    public static class MapperToViewModel
    {
        public static UserViewModel ToViewModel(this UserDto model)
        {
            if (model == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.NotFound, "The user is not found");
            }

            return new UserViewModel
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                RegisteredOn = model.RegisteredOn
            };
        }

        public static IEnumerable<UserViewModel> ToUserViewModels(this IEnumerable<UserDto> collection)
        {
            if (collection == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.NotFound, "The users are not found");
            }

            return collection.Select(x => x.ToViewModel());
        }
    }
}
