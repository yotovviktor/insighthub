﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;

namespace InsightHub.Web.Models.UserViewModels
{
    public static class MapperToUserDetailsViewModel
    {
        public static UserDetailsViewModel ToDetailsViewModel(this UserDto model)
        {
            if (model == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "This user is not found");
            }
            return new UserDetailsViewModel
            {
                Id = model.Id,
                Email = model.Email,
                Status = model.Status,
                Role = model.Role,
                PhoneNumber = model.PhoneNumber,
                FirstName = model.FirstName,
                LastName = model.LastName,
                IsDisabled = model.IsDisabled,
                LockoutEnd = model.LockoutEnd,
                RegisteredOn = model.RegisteredOn,
            };
        }
        public static IEnumerable<UserDetailsViewModel> ToUserDetailsViewModels(this IEnumerable<UserDto> collection)
        {
            if (collection == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.NotFound, "The users are not found");
            }

            return collection.Select(x => x.ToDetailsViewModel());
        }
    }
}
