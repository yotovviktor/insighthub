﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models.UserViewModels
{
    public class EditPasswordViewModel
    {
        [Required]
        public string Password { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Compare("NewPassword",ErrorMessage ="The Passwords do not match")]
        public string ConfirmNewPassword { get; set; }

    }
}
