﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Common.Enums;
using InsightHub.Data.Enums;

namespace InsightHub.Web.Models.UserViewModels
{
    /// <summary>
    /// A class designed to transfer the the filter params from the presentation layer to the Api
    /// Properties Role, Property, SortBy, Keyword
    /// </summary>
    public class FIlterModel
    {
        public Role? Role { get; set; } 

        public UserSortBy? SortBy { get; set; } 

        public Order? OrderBy { get; set; }

        public string Keyword { get; set; }

        public Status? Status { get; set; }

    }
}
