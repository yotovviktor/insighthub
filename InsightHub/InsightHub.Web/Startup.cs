using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Azure.Storage.Blobs;
using InsightHub.Common.Middlewares;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.Contracts;
using InsightHub.Services.ContentServices;
using InsightHub.Services.EmailSenderService;
using InsightHub.Services.ReportServices;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Services.UserServices;
using InsightHub.Services.IndustryServices;
using InsightHub.Services.TagServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.SpaServices.AngularCli;

namespace InsightHub.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers().AddNewtonsoftJson(); 

            services.AddDbContext<InsightHubContext>(options =>
            options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<ISubscriptionService, SubscriptionService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IIndustryService, IndustryService>();
            services.AddScoped<ITagService, TagService>();

            services.AddIdentity<User, Role>(options => {
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = true;
            })
                .AddEntityFrameworkStores<InsightHubContext>()
                .AddDefaultTokenProviders();
            

            services.AddSingleton(x => new BlobServiceClient(Configuration.GetConnectionString("AzureBlobStorageConnection")));
            services.AddScoped<IContentService, AzureContentService>();

            services.AddControllersWithViews();

            services.AddScoped<SmtpClient>((serviceProvider) =>
            {
                var config = serviceProvider.GetRequiredService<IConfiguration>();
                return new SmtpClient()
                {
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Host = config.GetValue<String>("Smtp:Host"),
                    Port = config.GetValue<int>("Smtp:Port"),
                    Credentials = new NetworkCredential(
                            config.GetValue<String>("Smtp:Username"),
                            config.GetValue<String>("Smtp:Password"))
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireLogIn", policy =>
                       policy.RequireAuthenticatedUser());
                options.AddPolicy("AdminOnly", policy =>
                       policy.RequireRole("admin"));
                options.AddPolicy("AuthorOrAdmin", policy =>
                      policy.RequireRole("admin","author"));
                options.AddPolicy("CustomerOrAdmin", policy =>
                      policy.RequireRole("admin", "customer"));
                options.AddPolicy("CustomerOnly", policy =>
                    policy.RequireRole("customer"));
                options.AddPolicy("AuthorOnly", policy =>
                   policy.RequireRole("author"));
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "InsightHub API",

                    Contact = new OpenApiContact
                    {
                        Name = "Insight Hub Team",
                        Email = "info.insighthub@gmail.com",
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseCookiePolicy();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InsightHub API V1");
                //c.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }
    }
}
