﻿using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Services.TagServices;
using InsightHub.Web.Models;
using InsightHub.Web.Models.Mappers.MappersToDtos;
using InsightHub.Web.Models.Mappers.MappersToModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// The controller perfoms CRUD operations on tags
    /// </summary>
    [Route("api/tags")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ITagService _tagService;

        public TagController(ITagService tagService)
        {
            this._tagService = tagService ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(tagService));
        }

        /// <summary>
        /// Retrieve all tags
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="500">Oops! Can't list the tags right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet(Name = "ListTags")]
        public async Task<IActionResult> ListTagsAsync()
        {
            var tagsDtos = await _tagService.ListTagsAsync();
            var tagsModels = tagsDtos.ToModels();

            return Ok(tagsModels);
        }

        /// <summary>
        /// Create new tag
        /// </summary>
        /// Sample request:
        ///
        ///     POST /tags
        ///     {
        ///        "name": "Marketing"
        ///     }
        ///
        /// <response code="201">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to create is not with role admin or author</response>
        /// <response code="409">Tag with the same name already exist</response>
        /// <response code="500">Oops! Can't create tag right now</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AuthorOrAdmin")]
        [HttpPost(Name = "CreateTag")]
        public async Task<IActionResult> CreateTagAsync([FromBody] TagViewModel tagModel)
        {
            var tagDto = tagModel.ToDto();

            var tag = await _tagService.CreateTagAsync(tagDto);
            return Created("created tag", tag.ToModel());
        }

        /// <summary>
        /// Update a existing tag
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /tags/1
        ///     {
        ///        "name": "Sales"
        ///     }
        ///
        /// </remarks>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to update is not with role admin</response>
        /// <response code="404">Tag not found</response>
        /// <response code="409">Tag with the same name already exist</response>
        /// <response code="500">Oops! Can't update the tag right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}", Name = "UpdateTag")]
        public async Task<IActionResult> UpdateTagAsync(int id, [FromBody]TagViewModel tagModel)
        {
            var tagDto = tagModel.ToDto();
            tagDto.Id = id;

            await _tagService.UpdateTagAsync(tagDto);
            return NoContent();
        }

        /// <summary>
        /// Delete a specific tag
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to delete is not with role admin</response>
        /// <response code="404">Tag not found</response>
        /// <response code="409">There are reports using this tag</response>
        /// <response code="500">Oops! Can't delete the tag right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpDelete("{id}", Name = "DeleteTag")]
        public async Task<IActionResult> DeleteTagAsync(int id)
        {
            await this._tagService.DeleteTagAsync(id);
            return NoContent();
        }
    }
}
