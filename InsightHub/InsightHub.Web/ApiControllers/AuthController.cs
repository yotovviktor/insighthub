﻿using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using InsightHub.Services.EmailSenderService;
using InsightHub.Services.UserServices;
using InsightHub.Web.Models;
using InsightHub.Web.Models.AuthViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{

    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {

        public AuthController(UserManager<User> userManager, IEmailSender emailSender, SignInManager<User> signInManager, IUserService userService)
        {
            UserManager = userManager ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) UserManager");
            EmailSender = emailSender ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) EmailSender");
            SignInManager = signInManager ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) SignInManager");
            UserService = userService ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) UserService");
        }


        private IEmailSender EmailSender { get; set; }

        private UserManager<User> UserManager { get; set; }

        private SignInManager<User> SignInManager { get; set; }

        private IUserService UserService { get; set; }


        /// <summary>
        /// Logs in the user, creates and returns a cookie
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Incorrect email or password</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Oops! Can't logins rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> AuthenticateAsync([FromBody] LogInModel model)
        {

            if (!await UserService.IsUserApprovedAsync(model.Email))
            {
                return Unauthorized();
            }

            var result = await this.SignInManager
                .PasswordSignInAsync(model.Email, model.Password, model.RemmemberLogin, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                var newlyLogedUser = (await this.UserService.FilterAsync(null, null, model.Email, null, null)).FirstOrDefault();
                await this.UserService.EditTheLastLogInDateAsync(newlyLogedUser.Id);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Registers a user and saves it in the database
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="500">Oops! Can't register rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterForm form)
        {
            //TODO: Status= rejected and email confirmation = false 

            var newlyCreatedUser = new User
            {
                FirstName = form.FirstName,
                LastName = form.LastName,
                Email = form.Email,
                NormalizedEmail = form.Email.ToUpperInvariant(),
                UserName = form.Email,
                NormalizedUserName = form.Email.ToUpperInvariant(),
                PhoneNumber = form.PhoneNumber,
                LockoutEnabled = false,
                RegisteredOn = DateTime.UtcNow,
                Role = form.Role,
                Status = Data.Enums.Status.Pending,
            };

            var registerResult = await UserManager.CreateAsync(newlyCreatedUser, form.Password);

            if (registerResult.Succeeded)
            {
                await UserManager.AddToRoleAsync(newlyCreatedUser, form.Role.ToString());
                var newUserId = await UserManager.GetUserIdAsync(newlyCreatedUser);
                var confirmationLink = await GenerateEmailConfirmationLinkAsync(newlyCreatedUser);

                await EmailSender.ConfirmationEmailProvider(int.Parse(newUserId), confirmationLink);
                return Ok();
            }

            return BadRequest();

        }


        /// <summary>
        /// Signs out the user and sets the cookie as expired
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPost("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            await SignInManager.SignOutAsync();
            return Ok();
        }


        private async Task<string> GenerateEmailConfirmationLinkAsync(User user)
        {
            string confirmationToken = await UserManager.
                 GenerateEmailConfirmationTokenAsync(user);

            string confirmationLink = Url.Action("ConfirmEmail",
              "Auth", new
              {
                  userid = user.Id,
                  token = confirmationToken
              },
               protocol: HttpContext.Request.Scheme);

            return confirmationLink;
        }


        /// <summary>
        /// Confirms the email 
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Incorrect email or password</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> ConfirmEmailAsync(int userId, string token)
        {
            var user = await UserManager.FindByIdAsync(userId.ToString());

            var result = await UserManager.ConfirmEmailAsync(user, token);

            if (result.Succeeded)
            {
                return NoContent();
            }

            return BadRequest();
        }

        /// <summary>
        /// Gets the current user information
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpGet("cookie")]
        public async Task<ActionResult<CookieViewModel>> GetCookieValueAsync()
        {
            var user = await this.UserManager.GetUserAsync(this.User);
            var idClaim = int.Parse(this.UserManager.GetUserId(this.User));
            var roleClaim = (await this.UserManager.GetRolesAsync(user)).FirstOrDefault();
            var expirationDate = user.LastLogedIn;
            expirationDate = expirationDate.Value.AddDays(14);

            var cookieModel = new CookieViewModel
            {
                Id = idClaim,
                Role = roleClaim,
                ExpirationDate = expirationDate.Value
            };

            return Ok(cookieModel);
        }
    }
}