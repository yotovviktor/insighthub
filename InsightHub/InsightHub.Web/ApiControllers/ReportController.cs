using System.Net;
using System.Threading.Tasks;
using HeyRed.Mime;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using InsightHub.Services.ReportServices;
using InsightHub.Web.Models.Mappers;
using InsightHub.Web.Models.Mappers.MappersToDtos;
using InsightHub.Web.Models.Mappers.MappersToModels;
using InsightHub.Web.Models.Models.ReportViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// The controller perfoms CRUD operations on reports
    /// </summary>
    [Route("api/reports")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportService _reportService;
        private readonly UserManager<User> _userManager;

        public ReportController(IReportService reportService, UserManager<User> userManager)
        {
            this._reportService = reportService ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(reportService));
            this._userManager = userManager ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(userManager));
        }

        /// <summary>
        /// Retrieve all reports filtered and sorted by params
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /reports?industry=1&amp;sortby=downloads&amp;order=desc
        ///
        /// </remarks>
        /// <response code="200">Succesfull operation</response>
        /// <response code="500">Oops! Can't list the reports right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet(Name = "ListReports")]
        public async Task<IActionResult> ListReportsAsync([FromQuery]QueryReportViewModel model)
        {
            var filter = model.ToFilterModel();
            var paging = model.ToPagingModel();

            if (!User.IsInRole("admin"))
            {
                filter.Status = Status.Approved;
            }

            var reportDtos = await _reportService.ListReportsAsync(filter, model.SortBy, model.Order, paging);
            var reportModels = reportDtos.ToModels();
           
            Response.Headers.Add("X-Total-Count", paging.TotalCount.ToString());

            return Ok(reportModels);
        }

        /// <summary>
        /// Retrieve a specific report
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="404">Report not found</response>
        /// <response code="500">Oops! Can't get the report right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetReport")]
        public async Task<IActionResult> GetReportAsync(int id)
        {            
            var reportDTO = await this._reportService.GetReportAsync(id);

            if (reportDTO.Status != Status.Approved)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var userId = int.Parse(_userManager.GetUserId(User));

                    if (!(User.IsInRole("admin") || reportDTO.AuthorId == userId))
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }          

            var reportModel = reportDTO.ToDetailedModel();
            return Ok(reportModel);
        }

        /// <summary>
        /// Download a specific report binary content
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">The user is not approved customer</response>
        /// <response code="404">Report not found</response>
        /// <response code="500">Oops! Can't download the report right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpGet("{id}/content", Name = "DownloadReport")]
        public async Task<IActionResult> DownloadReportAsync(int id)
        {
            var userId = int.Parse(_userManager.GetUserId(User));

            if (User.IsInRole("author") && !await _reportService.IsValidOwnershipAsync(userId, id))
            {
                return Unauthorized();
            }

            if (User.IsInRole("customer"))
            {
                await this._reportService.MarkReportAsDownloadedAsync(id, userId);
            }

            var reportDTO = await this._reportService.DownloadReportAsync(id, userId);

            var extention = MimeTypesMap.GetExtension(reportDTO.ContentType);
            var fileName = $"{reportDTO.Name}.{extention}";

            return File(reportDTO.Content, Application.Octet, fileName);
        }

        /// <summary>
        /// Create new report
        /// </summary>
        /// <response code="201">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">The user is not the approved author</response>
        /// <response code="500">Oops! Can't create report right now</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AuthorOnly")]
        [HttpPost(Name = "CreateReport")]
        public async Task<IActionResult> CreateReportAsync([FromBody] ReportCreateViewModel reportModel)
        {
            var userId = int.Parse(_userManager.GetUserId(User));

            var reportDto = reportModel.ToDto();
            reportDto.AuthorId = userId;

            var report = await _reportService.CreateReportAsync(reportDto);
            return Created("created report", report.ToModel());
        }

        /// <summary>
        /// Update a existing report
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">The user is not the author of the report</response>
        /// <response code="404">Report not found</response>
        /// <response code="500">Oops! Can't update the report right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AuthorOrAdmin")]
        [HttpPut("{id}", Name = "UpdateReport")]
        public async Task<IActionResult> UpdateReportAsync(int id, [FromBody]ReportUpdateViewModel reportModel)
        {
            await ValidateOwnerOrAdminAsync(id);

            var reportDto = reportModel.ToDto();
            reportDto.Id = id;

            await _reportService.UpdateReportAsync(reportDto);
            return NoContent();
        }

        /// <summary>
        /// Change reports status
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="404">Report not found</response>
        /// <response code="500">Oops! Can't change the status of the report right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}/status", Name = "ChangeReportStatus")]
        public async Task<IActionResult> ChangeReportStatusAsync(int id, [FromBody]ReportStatusViewModel report)
        {
            await this._reportService.ChangeReportStatusAsync(id, report.Status);
            return NoContent();
        }

        /// <summary>
        /// Change featured report state 
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="404">Report not found</response>
        /// <response code="500">Oops! Can't change the state of the report right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}/featured", Name = "ChangeReportFeaturedState")]
        public async Task<IActionResult> ChangeReportFeaturedStateAsync(int id, [FromBody]ReportFeaturedViewModel report)
        {
            await this._reportService.ChangeIsFeaturedAsync(id, report.IsFeatured);
            return NoContent();
        }

        /// <summary>
        /// Delete a specific report
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="401">The user is not the author of the report</response>
        /// <response code="404">Report not found or already deleted</response>
        /// <response code="500">Oops! Can't delete the report right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AuthorOrAdmin")]
        [HttpDelete("{id}", Name = "DeleteReport")]
        public async Task<IActionResult> DeleteReportAsync(int id)
        {
            await ValidateOwnerOrAdminAsync(id);

            await this._reportService.DeleteReportAsync(id);
            return NoContent();
        }

        private async Task ValidateOwnerOrAdminAsync(int reportId)
        {
            var userId = int.Parse(_userManager.GetUserId(User));

            if (!User.IsInRole("admin") && !await _reportService.IsValidOwnershipAsync(userId, reportId))
            {
                throw new ApiException(HttpStatusCode.Unauthorized, string.Empty);
            }
        }
    }
}
