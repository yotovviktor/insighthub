﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using InsightHub.Services.ReportServices;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Services.UserServices;
using InsightHub.Web.Models.Mappers;
using InsightHub.Web.Models.Mappers.MappersToModels;
using InsightHub.Web.Models.Models.ReportViewModels;
using InsightHub.Web.Models.ReportViewModels;
using InsightHub.Web.Models.SubscriptionViewModel;
using InsightHub.Web.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(IUserService userService, UserManager<User> userManager, SignInManager<User> signInManager, ISubscriptionService subscriptionService, IReportService reportService)
        {
            UserService = userService ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) UserService");
            UserManager = userManager ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) UserManager");
            SignInManager = signInManager ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) SignInManager");
            SubscriptionService = subscriptionService ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) SubscriptionService");
            ReportService = reportService ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) ReportService");
        }

        private IUserService UserService { get; set; }

        private UserManager<User> UserManager { get; set; }

        private SignInManager<User> SignInManager { get; set; }

        private ISubscriptionService SubscriptionService { get; set; }

        private IReportService ReportService { get; set; }

        /// <summary>
        ///  Gets all users in the application
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't list users rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //[Authorize(Policy = "RequireLogIn")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> GetAllUsersAsync()
        {
            var result = await UserService.GetAllUsersAsync();
            return Ok(result.ToUserViewModels());
        }


        /// <summary>
        ///  Gets a specific user in the application
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="404">The user with that id does not exist</response>
        /// <response code="500">Oops! Can't get the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDetailsViewModel>> GetDetailsAsync(int id)
        {
            var userId = int.Parse(UserManager.GetUserId(User));

            if (this.User.IsInRole("admin") || userId == id)
            {
                var result = await UserService.GetUserByIdAsync(id);
                return Ok(result.ToDetailsViewModel());
            }

            return Unauthorized();
        }


        /// <summary>
        ///  Filters users by a specific criteria
        /// </summary>
        /// <response code="200">Succesfull operation</response> 
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't get the users rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpGet("filter")]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> FilterAsync([FromQuery]FIlterModel model)
        {

            var result = await this.UserService.FilterAsync(model.Role, model.SortBy, model.Keyword, model.Status, model.OrderBy);

            return Ok(result.ToUserDetailsViewModels());
        }

        /// <summary>
        ///  This action asynchronously edits the user's first name.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't edit the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPut("{id}/firstname")]
        public async Task<IActionResult> EditUserFirstNameAsync(int id, [FromBody] EditNameViewModel nameModel)
        {
            if (!await IsAuthorizedAsync(id, nameModel.Password))
            {
                return Unauthorized();
            }

            await this.UserService.EditFirstNameAsync(id, nameModel.Name);
            return NoContent();
        }


        /// <summary>
        ///  This action asynchronously edits the user's last name.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't edit the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPut("{id}/lastname")]
        public async Task<IActionResult> EditUserLastNameAsync(int id, [FromBody] EditNameViewModel nameModel)
        {
            if (!await IsAuthorizedAsync(id, nameModel.Password))
            {
                return Unauthorized();
            }

            await this.UserService.EditLastNameAsync(id, nameModel.Name);
            return NoContent();
        }


        /// <summary>
        /// This action asynchronously edits the user's Email.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Parameters are not correct</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't edit the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPut("{id}/email")]
        public async Task<IActionResult> EditUserEmailAsync(int id, [FromBody] EditEmailViewModel emailModel)
        {
            if (!await IsAuthorizedAsync(id, emailModel.Password))
            {
                return Unauthorized();
            }

            var user = await UserManager.GetUserAsync(this.User);
            var token = await UserManager.GenerateChangeEmailTokenAsync(user, emailModel.Email);

            if ((await UserManager.ChangeEmailAsync(user, emailModel.Email, token)).Succeeded)
            {
                await UserManager.UpdateNormalizedEmailAsync(user);
                await UserManager.SetUserNameAsync(user, emailModel.Email);
                await UserManager.UpdateNormalizedUserNameAsync(user);

                return NoContent();
            }

            return BadRequest();
        }


        /// <summary>
        /// This action asynchronously edits the user's Phone Number.
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Parameters are not correct</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't edit the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPut("{id}/phonenumber")]
        public async Task<IActionResult> EditUsersPhoneAsync(int id, [FromBody] EditPhoneNumberViewModel phoneModel)
        {
            if (!await IsAuthorizedAsync(id, phoneModel.Password))
            {
                return Unauthorized();
            }

            var user = await UserManager.GetUserAsync(this.User);
            var token = await UserManager.GenerateChangePhoneNumberTokenAsync(user, phoneModel.PhoneNumber);

            if ((await UserManager.ChangePhoneNumberAsync(user, phoneModel.PhoneNumber, token)).Succeeded)
            {
                return NoContent();
            }

            return BadRequest();
        }


        /// <summary>
        /// This action asynchronously edits the user's Password.
        /// </summary>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Parameters are not correct</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="500">Oops! Can't edit the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "RequireLogIn")]
        [HttpPut("{id}/password")]
        public async Task<IActionResult> EditUserPasswordAsync(int id, [FromBody] EditPasswordViewModel passwordModel)
        {
            if (!await IsAuthorizedAsync(id, passwordModel.Password))
            {
                return Unauthorized();
            }

            var user = await UserManager.GetUserAsync(this.User);
            var token = await UserManager.GeneratePasswordResetTokenAsync(user);

            if ((await UserManager.ResetPasswordAsync(user, token, passwordModel.NewPassword)).Succeeded)
            {
                return NoContent();
            }

            return BadRequest();
        }


        /// <summary>
        ///  Changes the status of a user, only for admins
        /// </summary>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="401">The user is not authenticated</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="500">Oops! Can't change the status rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //[Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}/status")]
        public async Task<IActionResult> ApproveUserAsync(int id, UserStatusModel model)
        {
            await UserService.ChangeUserStatusAsync(id, model.Status);

            return NoContent();
        }


        /// <summary>
        ///  Manages User Lockouts, only for admins
        /// </summary>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authorized or not authenticated</response>
        /// <response code="500">Oops! Can't disable or enable the user rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //[Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}/lockout")]
        public async Task<IActionResult> LockOutEnableAsync(int id, [FromBody] LockOutViewModel model)
        {
            var targetedUser = await this.UserManager.FindByIdAsync(id.ToString());

            if (model.LockOutEnable)
            {
                if (model.LockOutPeriod == null)
                {
                    model.LockOutPeriod = 14;
                }

                var lockoutEnd = DateTimeOffset.UtcNow
                    .AddDays((double)model.LockOutPeriod);

                await this.UserManager.SetLockoutEnabledAsync(targetedUser, true);
                await this.UserManager.SetLockoutEndDateAsync(targetedUser, lockoutEnd);

                return NoContent();
            }
            else
            {
                await this.UserManager.SetLockoutEnabledAsync(targetedUser, false);

                return NoContent();
            }
        }

        /// <summary>
        ///  Subscribes for a certain industry to recieve email notifications
        /// </summary>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "CustomerOnly")]
        [HttpPut("{userId}/subscriptions/{industryId}")]
        public async Task<IActionResult> SubscribeForIndustryAsync(int userId, int industryId)
        {
            if (userId != int.Parse(UserManager.GetUserId(User)))
            {
                return Unauthorized();
            }

            await SubscriptionService.SubscribeForIndustryAsync(userId, industryId);

            return NoContent();
        }


        /// <summary>
        ///  Cancel a subscribtion for a certain industry
        /// </summary>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "CustomerOnly")]
        [HttpDelete("{userId}/subscriptions/{industryId}")]
        public async Task<IActionResult> CancelSubscriptionAsync(int userId, int industryId)
        {
            if (userId != int.Parse(UserManager.GetUserId(User)))
            {
                return Unauthorized();
            }

            await SubscriptionService.DeleteSubscriptionAsync(userId, industryId);

            return NoContent();
        }


        /// <summary>
        /// Gets the downloaded reports of a certain user, filtered and sorted by params
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="403">Can't get other users information</response>
        /// <response code="500">Oops! Can't list downloads rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "CustomerOrAdmin")]
        [HttpGet("{id}/downloads")]
        public async Task<ActionResult<IEnumerable<ReportViewModel>>> GetDownloadedReportsAsync(int id, [FromQuery] QueryReportViewModel model)
        {
            if (int.Parse(UserManager.GetUserId(User)) != id && !this.User.IsInRole("admin"))
            {
                return Unauthorized();
            }

            //Checks if the userId belongs to a customer.
            var isTheTargetedUserACustomer = await UserManager
                .IsInRoleAsync(await UserManager
                                .FindByIdAsync(id.ToString()), "customer");

            if (isTheTargetedUserACustomer)
            {
                var filter = model.ToFilterModel();
                var paging = model.ToPagingModel();

                var result = await ReportService.ListUserDownloadsAsync(id, filter, model.SortBy, model.Order, paging);

                Response.Headers.Add("X-Total-Count", paging.TotalCount.ToString());
                return Ok(result.ToModels());
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieve author reports filtered and sorted by params
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="204">Succesfull operation, but no content</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="403">Can't get other users information</response>
        /// <response code="500">Oops! Can't list reports rigth now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AuthorOrAdmin")]
        [HttpGet("{id}/reports")]
        public async Task<ActionResult<IEnumerable<ReportViewModel>>> ListAuthorReportsAsync(int id, [FromQuery] QueryReportViewModel model)
        {
            if (int.Parse(UserManager.GetUserId(User)) != id && !this.User.IsInRole("admin"))
            {
                return Unauthorized();
            }

            //Checks if the userId belongs to a author.
            var isTheTargetedUserAuthor = await UserManager
                .IsInRoleAsync(await UserManager
                                .FindByIdAsync(id.ToString()), "author");

            if (!isTheTargetedUserAuthor)
            {
                return Unauthorized();
            }

            var filter = model.ToFilterModel();
            var paging = model.ToPagingModel();
            filter.AuthorId = id;

            var reportDtos = await ReportService.ListReportsAsync(filter, model.SortBy, model.Order, paging);
            var reportModels = reportDtos.ToModels();

            Response.Headers.Add("X-Total-Count", paging.TotalCount.ToString());
            return Ok(reportModels);
        }

        /// <summary>
        /// Gets the subscriptions of a certain user
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="500">Oops! Can't subscribe rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "CustomerOrAdmin")]
        [HttpGet("{id}/subscriptions")]
        public async Task<ActionResult<IEnumerable<SubscriptionViewModel>>> ListSubscribtionsForUserAsync(int id)
        {
            if (id != int.Parse(UserManager.GetUserId(User)))
            {
                return Unauthorized();
            }

            var industryResult = await SubscriptionService.ListSubsciptionsOfUserAsync(id);

            var result = industryResult.Select(x => new SubscriptionViewModel
            {
                UserId = id,
                IndustryId = x.Id,
                IndustryName = x.Name
            });

            return Ok(result);
        }

        /// <summary>
        /// Checks if a user is subscribed for a specific industry
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Bad parameters</response>
        /// <response code="401">The user is not authenticated or not authorized</response>
        /// <response code="500">Oops! Can't look up rigth now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "CustomerOnly")]
        [HttpGet("{userId}/subscription/{industryId}")]
        public async Task<ActionResult<bool>> IsTheUserSubscribed(int userId, int industryId)
        {
            var result = await this.SubscriptionService.IsTheUserSubscribedForIndustryAsync(userId, industryId);

            return Ok(result);
        }

        private async Task<bool> EvaluatePasswordAsync(string password)
        {
            var user = await UserManager.GetUserAsync(this.User);

            if ((await SignInManager.CheckPasswordSignInAsync(user, password, false)).Succeeded)
            {
                return true;
            }

            return false;
        }

        private async Task<bool> IsAuthorizedAsync(int userId, string password)
        {
            if (userId != int.Parse(UserManager.GetUserId(User)))
            {
                return false;
            }

            if (!await EvaluatePasswordAsync(password))
            {
                return false;
            }

            return true;
        }
    }
}