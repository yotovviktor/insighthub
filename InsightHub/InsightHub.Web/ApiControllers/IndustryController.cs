﻿using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.Exceptions;
using InsightHub.Services.IndustryServices;
using InsightHub.Web.Models;
using InsightHub.Web.Models.Mappers;
using InsightHub.Web.Models.Mappers.MappersToDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// The controller perfoms CRUD operations on industries
    /// </summary>
    [Route("api/industries")]
    [ApiController]
    public class IndustryController : ControllerBase
    {
        private readonly IIndustryService _industryService;

        public IndustryController(IIndustryService industryService)
        {
            this._industryService = industryService ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(industryService));
        }

        /// <summary>
        /// Retrieve all industries
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="500">Oops! Can't list the industries right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet(Name = "ListIndustries")]
        public async Task<IActionResult> ListIndustriesAsync()
        {           
            var industryDtos = await _industryService.ListIndustriesAsync();
            var industryModels = industryDtos.ToModels();

            return Ok(industryModels);
        }

        /// <summary>
        /// Retrieve a specific industry
        /// </summary>
        /// <response code="200">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="404">Industry not found</response>
        /// <response code="500">Oops! Can't get the industry right now</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetIndustry")]
        public async Task<IActionResult> GetIndustryAsync(int id)
        {
            var industryDTO = await this._industryService.GetIndustryAsync(id);          

            var industryModel = industryDTO.ToModel();
            return Ok(industryModel);
        }

        /// <summary>
        /// Create new industry
        /// </summary>
        /// Sample request:
        ///
        ///     POST /industries
        ///     {
        ///        "name": "Healthcare"
        ///     }
        ///
        /// <response code="201">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to create is not with role admin</response>
        /// <response code="500">Oops! Can't create industry right now</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpPost(Name = "CreateIndustry")]
        public async Task<IActionResult> CreateIndustryAsync([FromBody] IndustryViewModel industryModel)
        {
            var industryDto = industryModel.ToDto();

            var industry = await _industryService.CreateIndustryAsync(industryDto);
            return Created("created industry", industry.ToModel());
        }

        /// <summary>
        /// Update a existing industry
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /industries/1
        ///     {
        ///        "name": "Banking"
        ///     }
        ///
        /// </remarks>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to update is not with role admin</response>
        /// <response code="404">Industry not found</response>
        /// <response code="409">Industry with the same name already exist</response>
        /// <response code="500">Oops! Can't update the industry right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpPut("{id}", Name = "UpdateIndustry")]
        public async Task<IActionResult> UpdateIndustryAsync(int id, [FromBody]IndustryViewModel industryModel)
        {         
            var industryDto = industryModel.ToDto();
            industryDto.Id = id;

            await _industryService.UpdateIndustryAsync(industryDto);
            return NoContent();
        }

        /// <summary>
        /// Delete a specific industry
        /// </summary>
        /// <response code="204">Succesfull operation</response>
        /// <response code="400">Invalid parameters</response>
        /// <response code="403">The user trying to delete is not with role admin</response>
        /// <response code="404">Industry not found</response>
        /// <response code="409">There are reports using this industry</response>
        /// <response code="500">Oops! Can't delete the industry right now</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Authorize(Policy = "AdminOnly")]
        [HttpDelete("{id}", Name = "DeleteIndustry")]
        public async Task<IActionResult> DeleteIndustryAsync(int id)
        {
            await this._industryService.DeleteIndustryAsync(id);
            return NoContent();
        }
    }
}
