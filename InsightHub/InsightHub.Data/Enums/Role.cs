﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Enums
{
    public enum Role
    {
        customer,
        author,
        admin
    }
}
