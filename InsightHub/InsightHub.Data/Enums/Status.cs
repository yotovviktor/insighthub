﻿namespace InsightHub.Data.Enums
{
    public enum Status
    {
        Pending,
        Approved,
        Rejected
    }
}
