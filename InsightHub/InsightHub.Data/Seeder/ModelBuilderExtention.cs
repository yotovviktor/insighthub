﻿using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using Role = InsightHub.Data.Enums.Role;

namespace InsightHub.Data.Seeder
{
    public static class ModelBuilderExtention
    {
        public static void Seeder(this ModelBuilder builder)
        {
            //seeding industries
            builder.Entity<Industry>().HasData(
               new Industry
               {
                   Id = 1,
                   Name = "Government & Public sector",
                   Description = "Most government agencies are challenged to do more with less — to maximize budget allocated to the mission by making technology spend as efficient and effective as possible. This means attacking technology debt and, in some cases, organizational inertia that has limited the full impact of technology. All of this plays out in a hostile cyber-environment where leaders must change the mindset to Zero Trust.",
                   ImageUrl = "https://i.ibb.co/jyf2GWx/goverment-0.jpg"
               },
               new Industry
               {
                   Id = 2,
                   Name = "High Tech & Telecom",
                   Description = "We have shifted to an outcomes-based market. Becoming digital-first means competing on the strength of your business technology and realizing the agility and efficiency of digital-native firms. Today more than ever, technology investments are inseparable from business strategy; technology buying decisions are made to deliver business outcomes. And so sits the challenge of high-tech firms: To move past buzzwords and products and better understand the issues and challenges of the industries and buyers you are trying to serve.",
                   ImageUrl = "https://i.ibb.co/YpxDxLg/high-tech.png"
               },
               new Industry
               {
                   Id = 3,
                   Name = "Retail",
                   Description = "The retail industry is doing fine — consumer spending is healthy -  the jobs market is strong. Fears of a downturn loom, but not right now. So, what ails retail? The root of the pain is a simple problem. Many retailers struggle to re-orient operations to meet customer expectations. Those that do this well take share and are restating minimum expectations for experience and value. Hyperpersonalization, instant gratification, and perpetual accessibility are becoming the benchmark.",
                   ImageUrl = "https://i.ibb.co/TvHrGxb/Retail.jpg"
               },
                new Industry
                {
                    Id = 4,
                    Name = "Health Insurance",
                    Description = "Healthcare reform. Vertical consolidation. Disruptive entrants. There is no shortage of forces creating uncertainty and risk for health insurers. However, the immediate risk comes from within: experiences that confuse, frustrate, and disenfranchise members. Health insurers have lagged other industries in experience — that gap now has consequence. Demand for alternative care and digital service models is intensifying, all while the baseline expectations of members are increasing. Doing nothing is not an option. Insighthub can help.",
                    ImageUrl = "https://i.ibb.co/Jcstj4n/heath.jpg"
                },
                new Industry
                {
                    Id = 5,
                    Name = "Wealth Management",
                    Description = "The wealth management advisor model is powerful and has withstood the test of time. That test is getting harder. A new generation of self-directed investors with a willingness to experiment on new platforms is forcing you to consider direct digital engagement. You need to tap into that future investor on their terms and through their preferred engagement path. How do you do that and maintain or even enhance the advisor channel, managing the channel conflict that can put a real dent in the business?",
                    ImageUrl = "https://i.ibb.co/Dfscs29/wealth-management.jpg"
                },
                new Industry
                {
                    Id = 6,
                    Name = "Insurance",
                    Description = "Insurance was the stable, steady industry. Not anymore. Whether growing claims cost due to global warming weather events, the early stages of ride sharing shifting to a bolder mobility market, or the first impact of insurtech, the insurance industry will change. Risk models and competitive assumptions are aging out. What will the insurance industry look like and act like, and what choices made in the near term decide insurers’ fate in the long term?",
                    ImageUrl = "https://i.ibb.co/Pggpzx6/Insurance.jpg"
                },
                new Industry
                {
                    Id = 7,
                    Name = "Banking",
                    Description = "The banking industry is opening up — literally. Whether through regulation or market forces, open banking is now a reality. Banks face many challenges: declining customer loyalty, an emergent fintech class, and commerce platforms that continue to inch toward providing financial services. The tried-and-true banking business model is fracturing, and strategic decisions loom.",
                    ImageUrl = "https://i.ibb.co/s9zJ7w9/banking.jpg"
                },
                new Industry
                {
                    Id = 8,
                    Name = "Manufactoring",
                    Description = "Various forces of digitalization impact how manufacturing enterprises plan, make and distribute goods. As CIO, you must understand how to enable digital business and leverage the disruptive technologies that can improve competitiveness in a rapidly evolving ecosystem. With the accelerating pace of digital change, manufacturing CIOs need help seeing around corners, avoiding pitfalls and learning from what others have done. Build transformative strategies for your organization.",
                    ImageUrl = "https://i.ibb.co/89GK92D/manufac.jpg"
                });

            //seeding tags
            builder.Entity<Tag>().HasData(
              new Tag { Id = 1, Name = "Banking" },
              new Tag { Id = 2, Name = "Digital Tranformation" },
              new Tag { Id = 3, Name = "Business & IT Alignment" },
              new Tag { Id = 4, Name = "Financial Services" },
              new Tag { Id = 5, Name = "User Experience" },
              new Tag { Id = 6, Name = "eCommerce" },
              new Tag { Id = 7, Name = "B2C" },
              new Tag { Id = 8, Name = "Branding" },
              new Tag { Id = 9, Name = "Pandemic" },
              new Tag { Id = 10, Name = "Online Retail" },
              new Tag { Id = 11, Name = "Technology" },
              new Tag { Id = 12, Name = "Manufactoring" },
              new Tag { Id = 13, Name = "Innovation" },
              new Tag { Id = 14, Name = "Computer Vision" },
              new Tag { Id = 15, Name = "AI Insights" },
              new Tag { Id = 16, Name = "Digital Insurance" },
              new Tag { Id = 17, Name = "Insurance" },
              new Tag { Id = 18, Name = "Strategy" },
              new Tag { Id = 19, Name = "Return of Investments (ROI)" },
              new Tag { Id = 20, Name = "Customer Centricity" },
              new Tag { Id = 21, Name = "Government" },
              new Tag { Id = 22, Name = "Public Sector" },
              new Tag { Id = 23, Name = "Robotic Process Automation (RPA" },
              new Tag { Id = 24, Name = "Information Security" },
              new Tag { Id = 25, Name = "Data & Information Protection" },
              new Tag { Id = 26, Name = "Energy & Utilities" },
              new Tag { Id = 27, Name = "Healthcare" });

            //seeding authors
            builder.Entity<User>().HasData(
             new User
             {
                 Id = 1,
                 FirstName = "Monique",
                 LastName = "Mckee",
                 Email = "monique.mckee@hotmail.com",
                 PhoneNumber = "0897616174",
                 RegisteredOn = new DateTime(2020, 02, 23),
                 Status = Status.Approved,
                 Role = Role.author
             },
             new User
             {
                 Id = 2,
                 FirstName = "Huma",
                 LastName = "Neale",
                 Email = "huma.neale@gmail.com",
                 PhoneNumber = "0897226134",
                 RegisteredOn = new DateTime(2020, 01, 23),
                 Status = Status.Approved,
                 Role = Role.author
             },
             new User
             {
                 Id = 3,
                 FirstName = "Ehsan",
                 LastName = "Hendricks",
                 Email = "ehsan_he@outlook.com",
                 PhoneNumber = "0899999174",
                 RegisteredOn = new DateTime(2020, 02, 11),
                 Status = Status.Approved,
                 Role = Role.author
             },
             new User
             {
                 Id = 4,
                 FirstName = "Miriam",
                 LastName = "Mcguire",
                 Email = "miriam_web@hotmail.com",
                 PhoneNumber = "0877938474",
                 RegisteredOn = new DateTime(2020, 01, 06),
                 Status = Status.Approved,
                 Role = Role.author
             },
             new User
             {
                 Id = 5,
                 FirstName = "Shelley",
                 LastName = "Parks",
                 Email = "shelly_parks_77@gmail.com",
                 PhoneNumber = "0899099176",
                 RegisteredOn = new DateTime(2020, 01, 14),
                 Status = Status.Approved,
                 Role = Role.author
             },
             new User
             {
                 Id = 6,
                 FirstName = "Courteney",
                 LastName = "Newman",
                 Email = "newman_ltd@outlook.com",
                 PhoneNumber = "0899162738",
                 RegisteredOn = new DateTime(2020, 04, 23),
                 Status = Status.Approved,
                 Role = Role.author
             });

            var hasher = new PasswordHasher<User>();
            //SEEDING ADMIN 
            var userAdmin = new User
            {
                Id = 7,
                FirstName = "Greta",
                LastName = "Pavlova",
                Email = "admin@gmail.com",
                NormalizedEmail = "admin@gmail.com".ToUpperInvariant(),
                UserName = "admin@gmail.com",
                NormalizedUserName = "admin@gmail.com".ToUpperInvariant(),
                PhoneNumber = "0899162738",
                RegisteredOn = new DateTime(2020, 04, 23),
                Status = Status.Approved,
                Role = Role.admin,
                EmailConfirmed = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN"
            };

            var passwordHashAdmin = hasher.HashPassword(userAdmin, "InsightHub@123");
            userAdmin.PasswordHash = passwordHashAdmin;

            //SEEDING AUTHOR 
            var userAuthor = new User
            {
                Id = 8,
                FirstName = "Viktor",
                LastName = "Yotov",
                Email = "author@gmail.com",
                NormalizedEmail = "author@gmail.com".ToUpperInvariant(),
                UserName = "author@gmail.com",
                NormalizedUserName = "author@gmail.com".ToUpperInvariant(),
                PhoneNumber = "0899162738",
                RegisteredOn = new DateTime(2020, 04, 23),
                Status = Status.Approved,
                Role = Role.author,
                EmailConfirmed = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN"
            };

            var passwordHashAuthor = hasher.HashPassword(userAuthor, "InsightHub@123");
            userAuthor.PasswordHash = passwordHashAuthor;

            //SEEDING CUSTOMER 
            var userCustomer = new User
            {
                Id = 9,
                FirstName = "Pasha",
                LastName = "Karaivanova",
                Email = "customer@gmail.com",
                NormalizedEmail = "customer@gmail.com".ToUpperInvariant(),
                UserName = "customer@gmail.com",
                NormalizedUserName = "customer@gmail.com".ToUpperInvariant(),
                PhoneNumber = "0899162738",
                RegisteredOn = new DateTime(2020, 04, 23),
                Status = Status.Approved,
                Role = Role.customer,
                EmailConfirmed = true,
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN"

            };

            var passwordHashCustomer = hasher.HashPassword(userCustomer, "InsightHub@123");
            userCustomer.PasswordHash = passwordHashCustomer;

            builder.Entity<User>().HasData(userAdmin, userAuthor, userCustomer);
            builder.Entity<IdentityUserRole<int>>().HasData(
               new IdentityUserRole<int>
               {
                   RoleId = 2,
                   UserId = userAdmin.Id
               },
               new IdentityUserRole<int>
               {
                   RoleId = 3,
                   UserId = userAuthor.Id
               },
               new IdentityUserRole<int>
               {
                   RoleId = 1,
                   UserId = userCustomer.Id
               });

            //seeding reports
            builder.Entity<Report>().HasData(
            new Report
            {
                Id = 1,
                Name = "The Future Of Digital Banking",
                Summary = "Traditional competitive and industry lines are fading into the background as fintech providers and commerce platforms offer a greater array of banking services. Fintech represents both risk and opportunity: the risk that fintech providers will poach margin-rich or relationship-oriented business and the opportunity to partner with or buy those companies that have a freer hand and more natural mindset to innovate.Like any startup community, the fintech market can be volatile. Some will evolve gracefully, some will crash, some will soar, and some will be acquired and be a new strategic threat to you. On the other hand, commerce platforms can scale — rapidly. You will need to be digitally astute and digitally nimble to win in a market that is often digital-first.",
                AuthorId = 2,
                IndustryId = 7,
                Status = Status.Approved,
                BlobPath = "dd181f5b-3a4a-416a-a24c-b9b862088dc0.pdf",
                DownloadCount = 12,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 03, 01)
            },
            new Report
            {
                Id = 2,
                Name = "Growth In A Customer-Led Market",
                Summary = "Insighthub Customer Experience Index (CX Index) results show that banks are struggling to create and maintain a human connection with customers. If customer loyalty is an economic engine, that engine is sputtering. This erosion of loyalty is happening while fintech providers specializing in specific high-value experiences and commerce platforms optimized for customer engagement move more aggressively into banking services.Banks need to embrace open banking, unleashing the power of data and scale. But job No. 1 is to rebuild the relationship with customers. Remake human connections through their life stages as a growth strategy.",
                AuthorId = 6,
                IndustryId = 7,
                Status = Status.Approved,
                BlobPath = "bc84c9af-a9fd-439c-a73c-57caa9af9cd5.pdf",
                DownloadCount = 56,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 02)
            },
            new Report
            {
                Id = 3,
                Name = "We Evaluated Banks’ In-App Messaging On COVID-19: Five Brands Stood Out",
                Summary = "What do consumers expect from their banks in response to the COVID-19 pandemic? We interviewed 40 customers of 14 banks to find out. We also asked them what they think of how their banks are handling the crisis so far. Their responses show that many banks are missing the mark by making COVID-19 content hard to find or focusing primarily on service availability and not on the more serious financial concerns customers have, such as how to make their next loan payment.",
                AuthorId = 5,
                IndustryId = 7,
                Status = Status.Approved,
                BlobPath = "fc043d7c-9fe0-4d76-a506-f404546d0c33.pdf",
                DownloadCount = 66,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 03)
            },
            new Report
            {
                Id = 4,
                Name = "Modernize IT With CX As The Core Design Principle",
                Summary = "Agencies need to improve the performance of technology to comply with FITARA and the MGT Act and to fuel mission success. To do this, you need to address corrosive technical debt — spending precious money on systems that add marginal value at outsized costs — and likely organizational inertia.The goal is to place the user — whether a citizen or an employee — at the center of design and modernize to shift debt to leverage. Reverse technical debt to empower the mission.",
                AuthorId = 1,
                IndustryId = 1,
                Status = Status.Approved,
                BlobPath = "a246522f-965b-4e8b-8612-dd8857c7036d.pdf",
                DownloadCount = 24,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 03, 05)
            },
            new Report
            {
                Id = 5,
                Name = "Zero Trust",
                Summary = "Cyberthreats are prolific and continuously adapting — we are in a cyber arms race where combatants have a broad threat surface to play with and no shortage of tactics to do damage. “Trust but verify” is no longer a valid approach. Moat and castle strategies ignore threats and compromise assets inside the castle. The Zero Trust framework provides CIOs and security leaders a rigorous approach to defend and counter today’s escalating risk. Embrace the mindset and reality of Zero Trust as the best path to protecting your firm and brand.",
                AuthorId = 4,
                IndustryId = 1,
                Status = Status.Approved,
                BlobPath = "e304c4e5-6c07-4ab0-aaec-73cb03362d43.pdf",
                DownloadCount = 84,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 11)
            },
            new Report
            {
                Id = 6,
                Name = "The Good And The Bad Of US Federal Website UX",
                Summary = "Content on federal websites is generally written in words customers understand. That’s not surprising, given standards in place thanks to resources like Digital.gov and the Plain Language Action and Information Network (PLAIN). But despite efforts to be clear, users reported feeling overwhelmed by the amount of information. Even the sites that fared best in our review stumbled when it comes to writing concisely and establishing a clear content hierarchy. ",
                AuthorId = 4,
                IndustryId = 1,
                Status = Status.Approved,
                BlobPath = "47fc0309-6d62-4e91-aecc-e1cfdef309fc.pdf",
                DownloadCount = 19,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 16)
            },
            new Report
            {
                Id = 7,
                Name = "How Technology Can Curb the Spread of COVID-19",
                Summary = "As governments explore different options for reducing the spread of COVID-19 after lifting some shelter-in-place and essential-only orders, gamification may be an answer. People are familiar with gamification principles used to inspire behaviors like weight loss or increased movement, but they also can be used to encourage many other behaviors by focusing on helping people achieve their individual goals. For example, China’s Health Code app displays a colored “badge” to represent the health status of an individual: Green for the ability to freely travel and yellow or red to indicate the person should alert authorities. While there are concerns about the app’s transparency and data collection, similar options with transparent criteria for badge colors might work in other countries. ",
                AuthorId = 3,
                IndustryId = 4,
                Status = Status.Approved,
                BlobPath = "36d728f9-089c-4677-89f1-daaab767ac47.pdf",
                DownloadCount = 74,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 03, 19)
            },
            new Report
            {
                Id = 8,
                Name = "How The COVID-19 Crisis Will Impact Marketing Spend Between Now And 2022",
                Summary = "The current COVID-19 crisis has upended CMO priorities and budgets, leaving many scrambling for alternate revenue sources and all asking, “What in the world do I plan for now?”To develop guidance in response to this question, Forrester forecasted how CMOs would spend over the next two years on media and advertising, marketing technology, marketing services, and internal marketing headcount. See our full report, “The 2020 COVID-19 Crisis Will Stun US Marketing,” for the detailed outlook. Here are the headlines: We used two scenarios to guide our forecasts. In all scenarios, marketing spend declines. But a return to marketing budget growth is much faster if the pandemic stalls by the end of 2020. Our second scenario — which we see as the more probable one — anticipates a mid-2021 recovery and will cause deeper budget cuts with broader effects across the marketing ecosystem and broader economy.",
                AuthorId = 2,
                IndustryId = 4,
                Status = Status.Approved,
                BlobPath = "b1e9426b-ab67-41a7-a4f1-b44b8c0c2393.pdf",
                DownloadCount = 49,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 20)
            },
            new Report
            {
                Id = 9,
                Name = "IoT Makes More Sense When You Break It Down Into Three Business Scenarios",
                Summary = "Business leaders always want more information about business operations and customer interactions than they have. The technologies of the internet of things (IoT) promise to provide sensing and remote control of physical things, like products or equipment. But the technologies, solutions, and use cases are also so diverse and complex that tech and business leaders get tangled up. We’ve just overhauled our framework for IoT to show how business and tech leaders can break down IoT into three fundamental scenarios, driven by three classes of stakeholders. All these scenarios revolve around physical things. However, the stakeholders and scenarios are different, based on whether the stakeholder is creating, using, or observing the internet-connected thing. And just to note, the internet-connected things are regular, everyday products and assets — not the personal smartphones, tablets, or PCs that we all use to connect to apps and services.",
                AuthorId = 1,
                IndustryId = 2,
                Status = Status.Approved,
                BlobPath = "dd133444-f1c2-4854-a175-a3f5d770e176.pdf",
                DownloadCount = 58,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 21)
            },
            new Report
            {
                Id = 10,
                Name = "Why Now Is The Perfect Time To Negotiate Cost Reductions With Software Vendors",
                Summary = "For almost all companies and industries impacted by the coronavirus pandemic, cost reduction is a top priority. For some, the cost trimming is a way to get through until the “new normal.” But for others, it’s an urgent survival tactic. Worker layoffs and furloughs aren’t the only way to quickly reduce costs — renegotiating contracts with vendors can provide almost immediate cost relief. In this upcoming webinar, we’ll provide some timely ideas (and solicit more from attendees) on how to work with software companies to renegotiate contracts to reduce costs while still maintaining healthy relationships for the long term. ",
                AuthorId = 5,
                IndustryId = 2,
                Status = Status.Approved,
                BlobPath = "354bfc55-a3b4-4322-b8f5-752428f6d625.pdf",
                DownloadCount = 90,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 03, 21)
            },
            new Report
            {
                Id = 11,
                Name = "Forecasting In Uncertainty: What Q1 2020 Vendor Earnings Tell Us About The Tech Market Outlook",
                Summary = "Tech vendors have begun reporting Q1 2020 earnings. IBM, Manhattan Associates, and SAP reported last week, as did Infosys, Tata Consultancy Services (TCS), and Wipro. This week, we have earnings reports from Alphabet (Google Cloud), Amazon Web Services, Cerner, Citrix, Microsoft, PTC, Pegasystems, ServiceNow, and Unisys, among others. In most cases, nine or 10 of the 13 weeks in the quarter predate the onset of the COVID-19 pandemic and the containment measures, so the reported revenue growth rates still look positive, if not strong. But in those earnings releases are clues to the future.",
                AuthorId = 6,
                IndustryId = 2,
                Status = Status.Approved,
                BlobPath = "36e1a1ef-453b-4a2c-a16f-794ff6fd7944.pdf",
                DownloadCount = 23,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 28),
            },
            new Report
            {
                Id = 12,
                Name = "Staying In Front Of Mobility",
                Summary = "The Sharing Economy is upon us. Moment-in-time travel is slowly but surely replacing the ownership culture. Ridesharing has moved from a novel experience to a base expectation in short order. The disruption was mostly contained to taxi services; the next wave will have a more far-reaching impact. Insurance as we know it is in full-board change mode: customer demand policies that better fit use; new technologies like AI that open new opportunities; and an emerging insurtech market and looming commerce platform players are set to change the rules and expectations. And we have yet to see the implications of self-driving cars and how to think about, assign and manage risk in that environment.",
                AuthorId = 5,
                IndustryId = 6,
                Status = Status.Approved,
                BlobPath = "50829ab4-fb01-4abd-9263-57c7dbac6ec4.pdf",
                DownloadCount = 56,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 04, 01)
            },
            new Report
            {
                Id = 13,
                Name = "Transforming The Claims Journey",
                Summary = "Claims is a profound proof point — will you really protect the customer financially in times of duress? It is the basic purpose of insurers, but the factors affecting the financials of risk and protection are changing and getting harder. Mobility complicates what was a rather simple model; global warming is already attacking bottom-line performance and turning old risk models into antiques.Something has to give — and it can’t be turning back on the purpose of protecting customers. There needs to be a rethink of property and casualty claims so that protecting the customer and delivering bottom-line performance is not an unsolvable equation. Transform claims to protect both the customer and your financials.",
                AuthorId = 4,
                IndustryId = 6,
                Status = Status.Approved,
                BlobPath = "5e5c9453-623f-4311-a056-645b5fe991a2.pdf",
                DownloadCount = 78,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 04, 11)
            },
            new Report
            {
                Id = 14,
                Name = "What Is The ROI Of Digital Transformation In Insurance?",
                Summary = "Working with digital execs at global insurance firms that are in the throes of digital transformation. One of the top inquiries that I’ve received from our insurance clients is: What is the ROI of digital transformation? The time has come for insurers to take a hard look at their digital investments to date. Globally, insurers have invested billions of dollars in digital transformation. Greater economic and regulatory uncertainty in 2020 will push digital execs to not only demonstrate the results from the projects that they have already implemented but also to explain how future initiatives will drive customer and business outcomes. But digital execs continue to struggle to demonstrate the value of digital. Many depend too much on easy-to-track but inadequate metrics, such as app downloads or login frequency, to measure, validate, and justify their digital initiatives.",
                AuthorId = 3,
                IndustryId = 6,
                Status = Status.Approved,
                BlobPath = "a9191014-5af4-4f4c-87f3-4c178c1064f1.pdf",
                DownloadCount = 15,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 04, 22)
            },
            new Report
            {
                Id = 15,
                Name = "Remaking Retail Loyalty",
                Summary = "Too many retail loyalty programs are expressions of merchant relationships and priorities. They are about the next transaction — often delivered by sterile emails. They have little to do with creating differentiated experiences on the customer’s terms. And the results show. Retail loyalty programs are failing to capture customers’ imaginations and, ultimately, loyalty — eroding the economic power of loyalty. Remake loyalty to be in service of the customer versus the next transaction.",
                AuthorId = 2,
                IndustryId = 3,
                Status = Status.Pending,
                BlobPath = "0bb16f59-04f6-4d99-8317-1e18a76aed9b.pdf",
                DownloadCount = 0,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 04, 23)
            },
            new Report
            {
                Id = 16,
                Name = "Brand Experience",
                Summary = "Consumer behavior and disruptors — (very) large and small — are remaking retail. The challenge is to catch up or fit in a way that affirms a distinctive brand — to not lose “you” in the pursuit of strategy. With fewer opportunities for product differentiation and the limited effectiveness of loyalty, delivering a brand experience that wins hearts, minds, and spend looms as the central challenge. Make customer experience the best expression of your brand promise.",
                AuthorId = 1,
                IndustryId = 3,
                Status = Status.Approved,
                BlobPath = "10d6c57f-3003-41a6-97df-f4d100c9eec7.pdf",
                DownloadCount = 13,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 05, 11)
            },
            new Report
            {
                Id = 17,
                Name = "Redesign Your Physical Location To Successfully Welcome Customers Back",
                Summary = "Consumer-facing services and retail businesses in parts of the US are readying themselves to reopen. But judging from current guidelines, grocery store experiences, and global examples like Shanghai Disneyland’s reopening, one thing is crystal clear: Don’t expect a “return to normal” anytime soon. Your customer is a key reason why. COVID-19 will cast a long shadow on customer attitudes, behavior, and trust and create a pervasive impact on how people want to interact with physical locations and the employees, services, and products they offer. To encourage customers to come back and usher in economic recovery, companies must plan to redesign their physical-location experiences to address customer fears of COVID-19 and their desire to maintain social distancing",
                AuthorId = 6,
                IndustryId = 3,
                Status = Status.Approved,
                BlobPath = "8a018ed8-6ee6-4967-b966-e32676009f71.pdf",
                DownloadCount = 212,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 22)
            },
            new Report
            {
                Id = 18,
                Name = "The Impact Of 5G In Financial Services",
                Summary = "5G will become a general-purpose technology for financial services firms as it provides super-fast mobile broadband, massive machine-type communications, and ultra-reliable and low-latency communications. 5G will provide new possibilities to create, store, and protect value, to move money, and to access credit.",
                AuthorId = 5,
                IndustryId = 5,
                Status = Status.Pending,
                BlobPath = "81837204-ecc0-4c09-ac15-2b93212838e0.pdf",
                DownloadCount = 0,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 03, 24)
            },
            new Report
            {
                Id = 19,
                Name = "Seize Payment Innovation Opportunities By Delivering Value To All",
                Summary = "Different customer segments have different payment needs and concerns. To develop a digital payment offering that brings value to consumers, closely research the needs of your target customers. Most players in this space tend to focus on younger consumers — the main users of digital payments. But our research also shows that more consumers aged 35–44 are adopting them — a key segment that typically represent a significant portion of a bank’s revenues and margin. Also, un/underbanked customers hold great potential for those institutions that know how to design and run the right operating model to serve them.",
                AuthorId = 4,
                IndustryId = 5,
                Status = Status.Approved,
                BlobPath = "438cd08f-8217-4b33-bd50-a130ade6aae6.pdf",
                DownloadCount = 18,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 04, 09)
            },
            new Report
            {
                Id = 20,
                Name = "The Future Is Unwritten: Writing The Future Of Financial Advice",
                Summary = "As digital and mobile technologies continue to dominate the everyday life of consumers, this trend is also having an outsize impact on all aspects of the retail investment value chain. How investors interact with investment management firms, research investments, make trades, and receive financial advice is fundamentally changing. Similarly, for financial advisors, the changing expectations of investors are influencing how advisors interact with clients, create financial plans, and interact with their wealth platforms to provide advice, manage their daily activities, and support clients.",
                AuthorId = 2,
                IndustryId = 5,
                Status = Status.Approved,
                BlobPath = "5c8aadef-87b8-485b-b1f9-46f83965153d.pdf",
                DownloadCount = 24,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 04, 18)
            },
            new Report
            {
                Id = 21,
                Name = "The InsightHub Tech Tide: Smart Manufacturing, Q2 2020",
                Summary = "Smart manufacturing is increasingly critical to manufacturers' ability to win, serve, and retain their customers. To accelerate their performance in smart manufacturing, companies are evaluating and adopting a range of contributing technologies. This Forrester Tech Tide™ report presents an analysis of the maturity and business value of 20 technology categories that support smart manufacturing. CIOs and manufacturing business leaders should read this report to shape their firm's investment approach to these technologies.",
                AuthorId = 3,
                IndustryId = 8,
                Status = Status.Approved,
                BlobPath = "afb5bc75-13e6-46b2-839e-b4608504cc9e.pdf",
                DownloadCount = 64,
                IsFeatured = false,
                CreatedOn = new DateTime(2020, 05, 02)
            },
            new Report
            {
                Id = 22,
                Name = "InsighHub Infographic: Manufacturing Firms Look Beyond Core IT For Digital Capabilities",
                Summary = "In April and May of 2020, InsightHub surveyed 450 leaders in various manufacturing disciplines across five countries in Asia Pacific to understand their priorities, barriers, and investments. We found that most manufacturers are increasing their IT spending and appear to be doing so to improve efficiency. However, rising customer expectations are forcing manufacturers to also become more customer-obsessed — an attitude that is starting to become evident in where these firms are focusing their tech investments.",
                AuthorId = 3,
                IndustryId = 8,
                Status = Status.Approved,
                BlobPath = "55e46c5e-87a1-467e-8150-771f4c02ac48.pdf",
                DownloadCount = 54,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 05, 15)
            },
            new Report
            {
                Id = 23,
                Name = "Emerging Technology Spotlight: Additive Manufacturing",
                Summary = "Advancements in 3D-printing technology stand poised to revolutionize the way mass-produced products are made. With additive manufacturing, a technology-enabled process, manufacturers can accelerate time-to-market, reduce supply chain costs, increase returns on parts, and enable mass customization. Meanwhile, barriers to entry in manufacturing will decrease, allowing new competitors to enter and disrupt markets. CIOs and business technology leaders should read this brief to understand how to gain competitive advantage through additive methods.",
                AuthorId = 1,
                IndustryId = 8,
                Status = Status.Approved,
                BlobPath = "a5cc80b8-be49-4bde-9123-0b56a4c4f16f.pdf",
                DownloadCount = 13,
                IsFeatured = true,
                CreatedOn = new DateTime(2020, 05, 01)
            });

            //seeding report-tags
            builder.Entity<ReportTags>().HasData(
              new ReportTags { ReportId = 1, TagId = 2 },
              new ReportTags { ReportId = 1, TagId = 11 },
              new ReportTags { ReportId = 1, TagId = 1 },
              new ReportTags { ReportId = 1, TagId = 13 },
              new ReportTags { ReportId = 2, TagId = 8 },
              new ReportTags { ReportId = 2, TagId = 18 },
              new ReportTags { ReportId = 2, TagId = 10 },
              new ReportTags { ReportId = 3, TagId = 1 },
              new ReportTags { ReportId = 3, TagId = 2 },
              new ReportTags { ReportId = 3, TagId = 7 },
              new ReportTags { ReportId = 4, TagId = 16 },
              new ReportTags { ReportId = 4, TagId = 19 },
              new ReportTags { ReportId = 5, TagId = 6 },
              new ReportTags { ReportId = 5, TagId = 5 },
              new ReportTags { ReportId = 5, TagId = 4 },
              new ReportTags { ReportId = 6, TagId = 21 },
              new ReportTags { ReportId = 6, TagId = 22 },
              new ReportTags { ReportId = 6, TagId = 18 },
              new ReportTags { ReportId = 7, TagId = 2 },
              new ReportTags { ReportId = 7, TagId = 11 },
              new ReportTags { ReportId = 8, TagId = 9 },
              new ReportTags { ReportId = 8, TagId = 26 },
              new ReportTags { ReportId = 9, TagId = 19 },
              new ReportTags { ReportId = 9, TagId = 24 },
              new ReportTags { ReportId = 9, TagId = 2 },
              new ReportTags { ReportId = 10, TagId = 2 },
              new ReportTags { ReportId = 10, TagId = 3 },
              new ReportTags { ReportId = 10, TagId = 14 },
              new ReportTags { ReportId = 11, TagId = 17 },
              new ReportTags { ReportId = 11, TagId = 7 },
              new ReportTags { ReportId = 11, TagId = 3 },
              new ReportTags { ReportId = 12, TagId = 12 },
              new ReportTags { ReportId = 13, TagId = 2 },
              new ReportTags { ReportId = 14, TagId = 19 },
              new ReportTags { ReportId = 15, TagId = 25 },
              new ReportTags { ReportId = 15, TagId = 11 },
              new ReportTags { ReportId = 16, TagId = 10 },
              new ReportTags { ReportId = 16, TagId = 8 },
              new ReportTags { ReportId = 16, TagId = 6 },
              new ReportTags { ReportId = 17, TagId = 6 },
              new ReportTags { ReportId = 18, TagId = 16 },
              new ReportTags { ReportId = 18, TagId = 3 },
              new ReportTags { ReportId = 19, TagId = 4 },
              new ReportTags { ReportId = 20, TagId = 4 },
              new ReportTags { ReportId = 20, TagId = 1 },
              new ReportTags { ReportId = 21, TagId = 12 },
              new ReportTags { ReportId = 21, TagId = 23 },
              new ReportTags { ReportId = 22, TagId = 12 },
              new ReportTags { ReportId = 22, TagId = 4 },
              new ReportTags { ReportId = 23, TagId = 12 },
              new ReportTags { ReportId = 23, TagId = 13 });
        }
    }
}
