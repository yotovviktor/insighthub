﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    RegisteredOn = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Role = table.Column<int>(nullable: false),
                    LastLogedIn = table.Column<DateTimeOffset>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Summary = table.Column<string>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    BlobPath = table.Column<string>(nullable: true),
                    DownloadCount = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsFeatured = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reports_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reports_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => new { x.UserId, x.IndustryId });
                    table.ForeignKey(
                        name: "FK_Subscriptions_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscriptions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DownloadedReports",
                columns: table => new
                {
                    ReportId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DownloadedReports", x => new { x.UserId, x.ReportId });
                    table.ForeignKey(
                        name: "FK_DownloadedReports_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DownloadedReports_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportTags",
                columns: table => new
                {
                    ReportId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTags", x => new { x.TagId, x.ReportId });
                    table.ForeignKey(
                        name: "FK_ReportTags_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "8e92e608-b805-45a3-b381-bf1f0c669491", "customer", "CUSTOMER" },
                    { 2, "317a31ce-a792-4326-896b-82ad1eaa02d9", "admin", "ADMIN" },
                    { 3, "f2837f7a-51bb-4180-8012-eacef4d1a4b8", "author", "AUTHOR" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastLogedIn", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "RegisteredOn", "Role", "SecurityStamp", "Status", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 9, 0, "da71e2ed-072b-419c-b7b6-a88a87565b29", "customer@gmail.com", true, "Pasha", null, "Karaivanova", false, null, "CUSTOMER@GMAIL.COM", "CUSTOMER@GMAIL.COM", "AQAAAAEAACcQAAAAEPfwANPahYzkN6CryPIr7AyWWOFLXHeKj9R5TUiUj3lC3GS7GlfOx/NwxOOut7iTvA==", "0899162738", false, new DateTime(2020, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", 1, false, "customer@gmail.com" },
                    { 7, 0, "a14b18a7-bd09-4bec-b1e9-b86969433213", "admin@gmail.com", true, "Greta", null, "Pavlova", false, null, "ADMIN@GMAIL.COM", "ADMIN@GMAIL.COM", "AQAAAAEAACcQAAAAECO0i2nZ9Pe9A5P5CFzVwbmAIlcuj9EvcGPiVcWrx4hoB+R9gZi/o/SphLSPjSN+QA==", "0899162738", false, new DateTime(2020, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", 1, false, "admin@gmail.com" },
                    { 6, 0, "2316f9b9-ca9d-4893-afe6-1a9629e4c69c", "newman_ltd@outlook.com", false, "Courteney", null, "Newman", false, null, null, null, null, "0899162738", false, new DateTime(2020, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 5, 0, "0693de90-2a17-4f8b-8125-df229d01ab9b", "shelly_parks_77@gmail.com", false, "Shelley", null, "Parks", false, null, null, null, null, "0899099176", false, new DateTime(2020, 1, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 4, 0, "c4cff178-9fcb-4c2c-b559-0400b5d88fa7", "miriam_web@hotmail.com", false, "Miriam", null, "Mcguire", false, null, null, null, null, "0877938474", false, new DateTime(2020, 1, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 3, 0, "c9304fab-ec8f-4e11-8b7a-100ff1a17a42", "ehsan_he@outlook.com", false, "Ehsan", null, "Hendricks", false, null, null, null, null, "0899999174", false, new DateTime(2020, 2, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 2, 0, "d1ee4484-dc05-4b04-8421-7d2100641d96", "huma.neale@gmail.com", false, "Huma", null, "Neale", false, null, null, null, null, "0897226134", false, new DateTime(2020, 1, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 1, 0, "1c237f95-0559-4a03-bedf-f86aad9e00aa", "monique.mckee@hotmail.com", false, "Monique", null, "Mckee", false, null, null, null, null, "0897616174", false, new DateTime(2020, 2, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, 1, false, null },
                    { 8, 0, "9e176a67-f2b2-4531-b5b2-737d90f1b265", "author@gmail.com", true, "Viktor", null, "Yotov", false, null, "AUTHOR@GMAIL.COM", "AUTHOR@GMAIL.COM", "AQAAAAEAACcQAAAAEF6bcQcgz563mAjs5ort3wMG2eAPSTLUVXy85rvhn4+OY7qFdLzIhGORa3tQxx/dTQ==", "0899162738", false, new DateTime(2020, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", 1, false, "author@gmail.com" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "Description", "ImageUrl", "Name" },
                values: new object[,]
                {
                    { 1, "Most government agencies are challenged to do more with less — to maximize budget allocated to the mission by making technology spend as efficient and effective as possible. This means attacking technology debt and, in some cases, organizational inertia that has limited the full impact of technology. All of this plays out in a hostile cyber-environment where leaders must change the mindset to Zero Trust.", "https://i.ibb.co/jyf2GWx/goverment-0.jpg", "Government & Public sector" },
                    { 2, "We have shifted to an outcomes-based market. Becoming digital-first means competing on the strength of your business technology and realizing the agility and efficiency of digital-native firms. Today more than ever, technology investments are inseparable from business strategy; technology buying decisions are made to deliver business outcomes. And so sits the challenge of high-tech firms: To move past buzzwords and products and better understand the issues and challenges of the industries and buyers you are trying to serve.", "https://i.ibb.co/YpxDxLg/high-tech.png", "High Tech & Telecom" },
                    { 3, "The retail industry is doing fine — consumer spending is healthy -  the jobs market is strong. Fears of a downturn loom, but not right now. So, what ails retail? The root of the pain is a simple problem. Many retailers struggle to re-orient operations to meet customer expectations. Those that do this well take share and are restating minimum expectations for experience and value. Hyperpersonalization, instant gratification, and perpetual accessibility are becoming the benchmark.", "https://i.ibb.co/TvHrGxb/Retail.jpg", "Retail" },
                    { 4, "Healthcare reform. Vertical consolidation. Disruptive entrants. There is no shortage of forces creating uncertainty and risk for health insurers. However, the immediate risk comes from within: experiences that confuse, frustrate, and disenfranchise members. Health insurers have lagged other industries in experience — that gap now has consequence. Demand for alternative care and digital service models is intensifying, all while the baseline expectations of members are increasing. Doing nothing is not an option. Insighthub can help.", "https://i.ibb.co/Jcstj4n/heath.jpg", "Health Insurance" },
                    { 5, "The wealth management advisor model is powerful and has withstood the test of time. That test is getting harder. A new generation of self-directed investors with a willingness to experiment on new platforms is forcing you to consider direct digital engagement. You need to tap into that future investor on their terms and through their preferred engagement path. How do you do that and maintain or even enhance the advisor channel, managing the channel conflict that can put a real dent in the business?", "https://i.ibb.co/Dfscs29/wealth-management.jpg", "Wealth Management" },
                    { 6, "Insurance was the stable, steady industry. Not anymore. Whether growing claims cost due to global warming weather events, the early stages of ride sharing shifting to a bolder mobility market, or the first impact of insurtech, the insurance industry will change. Risk models and competitive assumptions are aging out. What will the insurance industry look like and act like, and what choices made in the near term decide insurers’ fate in the long term?", "https://i.ibb.co/Pggpzx6/Insurance.jpg", "Insurance" },
                    { 7, "The banking industry is opening up — literally. Whether through regulation or market forces, open banking is now a reality. Banks face many challenges: declining customer loyalty, an emergent fintech class, and commerce platforms that continue to inch toward providing financial services. The tried-and-true banking business model is fracturing, and strategic decisions loom.", "https://i.ibb.co/s9zJ7w9/banking.jpg", "Banking" },
                    { 8, "Various forces of digitalization impact how manufacturing enterprises plan, make and distribute goods. As CIO, you must understand how to enable digital business and leverage the disruptive technologies that can improve competitiveness in a rapidly evolving ecosystem. With the accelerating pace of digital change, manufacturing CIOs need help seeing around corners, avoiding pitfalls and learning from what others have done. Build transformative strategies for your organization.", "https://i.ibb.co/89GK92D/manufac.jpg", "Manufactoring" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 3, "Business & IT Alignment" },
                    { 27, "Healthcare" },
                    { 26, "Energy & Utilities" },
                    { 25, "Data & Information Protection" },
                    { 24, "Information Security" },
                    { 23, "Robotic Process Automation (RPA" },
                    { 22, "Public Sector" },
                    { 21, "Government" },
                    { 20, "Customer Centricity" },
                    { 19, "Return of Investments (ROI)" },
                    { 18, "Strategy" },
                    { 4, "Financial Services" },
                    { 5, "User Experience" },
                    { 6, "eCommerce" },
                    { 7, "B2C" },
                    { 8, "Branding" },
                    { 9, "Pandemic" },
                    { 2, "Digital Tranformation" },
                    { 10, "Online Retail" },
                    { 12, "Manufactoring" },
                    { 1, "Banking" },
                    { 14, "Computer Vision" },
                    { 15, "AI Insights" },
                    { 16, "Digital Insurance" },
                    { 17, "Insurance" },
                    { 11, "Technology" },
                    { 13, "Innovation" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 9, 1 },
                    { 7, 2 },
                    { 8, 3 }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "AuthorId", "BlobPath", "CreatedOn", "DeletedOn", "DownloadCount", "IndustryId", "IsDeleted", "IsFeatured", "ModifiedOn", "Name", "Status", "Summary" },
                values: new object[,]
                {
                    { 17, 6, "8a018ed8-6ee6-4967-b966-e32676009f71.pdf", new DateTime(2020, 3, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 212, 3, false, false, null, "Redesign Your Physical Location To Successfully Welcome Customers Back", 1, "Consumer-facing services and retail businesses in parts of the US are readying themselves to reopen. But judging from current guidelines, grocery store experiences, and global examples like Shanghai Disneyland’s reopening, one thing is crystal clear: Don’t expect a “return to normal” anytime soon. Your customer is a key reason why. COVID-19 will cast a long shadow on customer attitudes, behavior, and trust and create a pervasive impact on how people want to interact with physical locations and the employees, services, and products they offer. To encourage customers to come back and usher in economic recovery, companies must plan to redesign their physical-location experiences to address customer fears of COVID-19 and their desire to maintain social distancing" },
                    { 11, 6, "36e1a1ef-453b-4a2c-a16f-794ff6fd7944.pdf", new DateTime(2020, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 23, 2, false, false, null, "Forecasting In Uncertainty: What Q1 2020 Vendor Earnings Tell Us About The Tech Market Outlook", 1, "Tech vendors have begun reporting Q1 2020 earnings. IBM, Manhattan Associates, and SAP reported last week, as did Infosys, Tata Consultancy Services (TCS), and Wipro. This week, we have earnings reports from Alphabet (Google Cloud), Amazon Web Services, Cerner, Citrix, Microsoft, PTC, Pegasystems, ServiceNow, and Unisys, among others. In most cases, nine or 10 of the 13 weeks in the quarter predate the onset of the COVID-19 pandemic and the containment measures, so the reported revenue growth rates still look positive, if not strong. But in those earnings releases are clues to the future." },
                    { 2, 6, "bc84c9af-a9fd-439c-a73c-57caa9af9cd5.pdf", new DateTime(2020, 3, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 56, 7, false, false, null, "Growth In A Customer-Led Market", 1, "Insighthub Customer Experience Index (CX Index) results show that banks are struggling to create and maintain a human connection with customers. If customer loyalty is an economic engine, that engine is sputtering. This erosion of loyalty is happening while fintech providers specializing in specific high-value experiences and commerce platforms optimized for customer engagement move more aggressively into banking services.Banks need to embrace open banking, unleashing the power of data and scale. But job No. 1 is to rebuild the relationship with customers. Remake human connections through their life stages as a growth strategy." },
                    { 18, 5, "81837204-ecc0-4c09-ac15-2b93212838e0.pdf", new DateTime(2020, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, 5, false, false, null, "The Impact Of 5G In Financial Services", 0, "5G will become a general-purpose technology for financial services firms as it provides super-fast mobile broadband, massive machine-type communications, and ultra-reliable and low-latency communications. 5G will provide new possibilities to create, store, and protect value, to move money, and to access credit." },
                    { 12, 5, "50829ab4-fb01-4abd-9263-57c7dbac6ec4.pdf", new DateTime(2020, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 56, 6, false, false, null, "Staying In Front Of Mobility", 1, "The Sharing Economy is upon us. Moment-in-time travel is slowly but surely replacing the ownership culture. Ridesharing has moved from a novel experience to a base expectation in short order. The disruption was mostly contained to taxi services; the next wave will have a more far-reaching impact. Insurance as we know it is in full-board change mode: customer demand policies that better fit use; new technologies like AI that open new opportunities; and an emerging insurtech market and looming commerce platform players are set to change the rules and expectations. And we have yet to see the implications of self-driving cars and how to think about, assign and manage risk in that environment." },
                    { 10, 5, "354bfc55-a3b4-4322-b8f5-752428f6d625.pdf", new DateTime(2020, 3, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 90, 2, false, true, null, "Why Now Is The Perfect Time To Negotiate Cost Reductions With Software Vendors", 1, "For almost all companies and industries impacted by the coronavirus pandemic, cost reduction is a top priority. For some, the cost trimming is a way to get through until the “new normal.” But for others, it’s an urgent survival tactic. Worker layoffs and furloughs aren’t the only way to quickly reduce costs — renegotiating contracts with vendors can provide almost immediate cost relief. In this upcoming webinar, we’ll provide some timely ideas (and solicit more from attendees) on how to work with software companies to renegotiate contracts to reduce costs while still maintaining healthy relationships for the long term. " },
                    { 3, 5, "fc043d7c-9fe0-4d76-a506-f404546d0c33.pdf", new DateTime(2020, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 66, 7, false, false, null, "We Evaluated Banks’ In-App Messaging On COVID-19: Five Brands Stood Out", 1, "What do consumers expect from their banks in response to the COVID-19 pandemic? We interviewed 40 customers of 14 banks to find out. We also asked them what they think of how their banks are handling the crisis so far. Their responses show that many banks are missing the mark by making COVID-19 content hard to find or focusing primarily on service availability and not on the more serious financial concerns customers have, such as how to make their next loan payment." },
                    { 19, 4, "438cd08f-8217-4b33-bd50-a130ade6aae6.pdf", new DateTime(2020, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 18, 5, false, true, null, "Seize Payment Innovation Opportunities By Delivering Value To All", 1, "Different customer segments have different payment needs and concerns. To develop a digital payment offering that brings value to consumers, closely research the needs of your target customers. Most players in this space tend to focus on younger consumers — the main users of digital payments. But our research also shows that more consumers aged 35–44 are adopting them — a key segment that typically represent a significant portion of a bank’s revenues and margin. Also, un/underbanked customers hold great potential for those institutions that know how to design and run the right operating model to serve them." },
                    { 13, 4, "5e5c9453-623f-4311-a056-645b5fe991a2.pdf", new DateTime(2020, 4, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 78, 6, false, true, null, "Transforming The Claims Journey", 1, "Claims is a profound proof point — will you really protect the customer financially in times of duress? It is the basic purpose of insurers, but the factors affecting the financials of risk and protection are changing and getting harder. Mobility complicates what was a rather simple model; global warming is already attacking bottom-line performance and turning old risk models into antiques.Something has to give — and it can’t be turning back on the purpose of protecting customers. There needs to be a rethink of property and casualty claims so that protecting the customer and delivering bottom-line performance is not an unsolvable equation. Transform claims to protect both the customer and your financials." },
                    { 6, 4, "47fc0309-6d62-4e91-aecc-e1cfdef309fc.pdf", new DateTime(2020, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 19, 1, false, false, null, "The Good And The Bad Of US Federal Website UX", 1, "Content on federal websites is generally written in words customers understand. That’s not surprising, given standards in place thanks to resources like Digital.gov and the Plain Language Action and Information Network (PLAIN). But despite efforts to be clear, users reported feeling overwhelmed by the amount of information. Even the sites that fared best in our review stumbled when it comes to writing concisely and establishing a clear content hierarchy. " },
                    { 5, 4, "e304c4e5-6c07-4ab0-aaec-73cb03362d43.pdf", new DateTime(2020, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 84, 1, false, false, null, "Zero Trust", 1, "Cyberthreats are prolific and continuously adapting — we are in a cyber arms race where combatants have a broad threat surface to play with and no shortage of tactics to do damage. “Trust but verify” is no longer a valid approach. Moat and castle strategies ignore threats and compromise assets inside the castle. The Zero Trust framework provides CIOs and security leaders a rigorous approach to defend and counter today’s escalating risk. Embrace the mindset and reality of Zero Trust as the best path to protecting your firm and brand." },
                    { 21, 3, "afb5bc75-13e6-46b2-839e-b4608504cc9e.pdf", new DateTime(2020, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 64, 8, false, false, null, "The InsightHub Tech Tide: Smart Manufacturing, Q2 2020", 1, "Smart manufacturing is increasingly critical to manufacturers' ability to win, serve, and retain their customers. To accelerate their performance in smart manufacturing, companies are evaluating and adopting a range of contributing technologies. This Forrester Tech Tide™ report presents an analysis of the maturity and business value of 20 technology categories that support smart manufacturing. CIOs and manufacturing business leaders should read this report to shape their firm's investment approach to these technologies." },
                    { 14, 3, "a9191014-5af4-4f4c-87f3-4c178c1064f1.pdf", new DateTime(2020, 4, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 15, 6, false, true, null, "What Is The ROI Of Digital Transformation In Insurance?", 1, "Working with digital execs at global insurance firms that are in the throes of digital transformation. One of the top inquiries that I’ve received from our insurance clients is: What is the ROI of digital transformation? The time has come for insurers to take a hard look at their digital investments to date. Globally, insurers have invested billions of dollars in digital transformation. Greater economic and regulatory uncertainty in 2020 will push digital execs to not only demonstrate the results from the projects that they have already implemented but also to explain how future initiatives will drive customer and business outcomes. But digital execs continue to struggle to demonstrate the value of digital. Many depend too much on easy-to-track but inadequate metrics, such as app downloads or login frequency, to measure, validate, and justify their digital initiatives." },
                    { 7, 3, "36d728f9-089c-4677-89f1-daaab767ac47.pdf", new DateTime(2020, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 74, 4, false, true, null, "How Technology Can Curb the Spread of COVID-19", 1, "As governments explore different options for reducing the spread of COVID-19 after lifting some shelter-in-place and essential-only orders, gamification may be an answer. People are familiar with gamification principles used to inspire behaviors like weight loss or increased movement, but they also can be used to encourage many other behaviors by focusing on helping people achieve their individual goals. For example, China’s Health Code app displays a colored “badge” to represent the health status of an individual: Green for the ability to freely travel and yellow or red to indicate the person should alert authorities. While there are concerns about the app’s transparency and data collection, similar options with transparent criteria for badge colors might work in other countries. " },
                    { 20, 2, "5c8aadef-87b8-485b-b1f9-46f83965153d.pdf", new DateTime(2020, 4, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 24, 5, false, false, null, "The Future Is Unwritten: Writing The Future Of Financial Advice", 1, "As digital and mobile technologies continue to dominate the everyday life of consumers, this trend is also having an outsize impact on all aspects of the retail investment value chain. How investors interact with investment management firms, research investments, make trades, and receive financial advice is fundamentally changing. Similarly, for financial advisors, the changing expectations of investors are influencing how advisors interact with clients, create financial plans, and interact with their wealth platforms to provide advice, manage their daily activities, and support clients." },
                    { 15, 2, "0bb16f59-04f6-4d99-8317-1e18a76aed9b.pdf", new DateTime(2020, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, 3, false, false, null, "Remaking Retail Loyalty", 0, "Too many retail loyalty programs are expressions of merchant relationships and priorities. They are about the next transaction — often delivered by sterile emails. They have little to do with creating differentiated experiences on the customer’s terms. And the results show. Retail loyalty programs are failing to capture customers’ imaginations and, ultimately, loyalty — eroding the economic power of loyalty. Remake loyalty to be in service of the customer versus the next transaction." },
                    { 8, 2, "b1e9426b-ab67-41a7-a4f1-b44b8c0c2393.pdf", new DateTime(2020, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 49, 4, false, false, null, "How The COVID-19 Crisis Will Impact Marketing Spend Between Now And 2022", 1, "The current COVID-19 crisis has upended CMO priorities and budgets, leaving many scrambling for alternate revenue sources and all asking, “What in the world do I plan for now?”To develop guidance in response to this question, Forrester forecasted how CMOs would spend over the next two years on media and advertising, marketing technology, marketing services, and internal marketing headcount. See our full report, “The 2020 COVID-19 Crisis Will Stun US Marketing,” for the detailed outlook. Here are the headlines: We used two scenarios to guide our forecasts. In all scenarios, marketing spend declines. But a return to marketing budget growth is much faster if the pandemic stalls by the end of 2020. Our second scenario — which we see as the more probable one — anticipates a mid-2021 recovery and will cause deeper budget cuts with broader effects across the marketing ecosystem and broader economy." },
                    { 1, 2, "dd181f5b-3a4a-416a-a24c-b9b862088dc0.pdf", new DateTime(2020, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 12, 7, false, true, null, "The Future Of Digital Banking", 1, "Traditional competitive and industry lines are fading into the background as fintech providers and commerce platforms offer a greater array of banking services. Fintech represents both risk and opportunity: the risk that fintech providers will poach margin-rich or relationship-oriented business and the opportunity to partner with or buy those companies that have a freer hand and more natural mindset to innovate.Like any startup community, the fintech market can be volatile. Some will evolve gracefully, some will crash, some will soar, and some will be acquired and be a new strategic threat to you. On the other hand, commerce platforms can scale — rapidly. You will need to be digitally astute and digitally nimble to win in a market that is often digital-first." },
                    { 23, 1, "a5cc80b8-be49-4bde-9123-0b56a4c4f16f.pdf", new DateTime(2020, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 13, 8, false, true, null, "Emerging Technology Spotlight: Additive Manufacturing", 1, "Advancements in 3D-printing technology stand poised to revolutionize the way mass-produced products are made. With additive manufacturing, a technology-enabled process, manufacturers can accelerate time-to-market, reduce supply chain costs, increase returns on parts, and enable mass customization. Meanwhile, barriers to entry in manufacturing will decrease, allowing new competitors to enter and disrupt markets. CIOs and business technology leaders should read this brief to understand how to gain competitive advantage through additive methods." },
                    { 16, 1, "10d6c57f-3003-41a6-97df-f4d100c9eec7.pdf", new DateTime(2020, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 13, 3, false, true, null, "Brand Experience", 1, "Consumer behavior and disruptors — (very) large and small — are remaking retail. The challenge is to catch up or fit in a way that affirms a distinctive brand — to not lose “you” in the pursuit of strategy. With fewer opportunities for product differentiation and the limited effectiveness of loyalty, delivering a brand experience that wins hearts, minds, and spend looms as the central challenge. Make customer experience the best expression of your brand promise." },
                    { 9, 1, "dd133444-f1c2-4854-a175-a3f5d770e176.pdf", new DateTime(2020, 3, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 58, 2, false, false, null, "IoT Makes More Sense When You Break It Down Into Three Business Scenarios", 1, "Business leaders always want more information about business operations and customer interactions than they have. The technologies of the internet of things (IoT) promise to provide sensing and remote control of physical things, like products or equipment. But the technologies, solutions, and use cases are also so diverse and complex that tech and business leaders get tangled up. We’ve just overhauled our framework for IoT to show how business and tech leaders can break down IoT into three fundamental scenarios, driven by three classes of stakeholders. All these scenarios revolve around physical things. However, the stakeholders and scenarios are different, based on whether the stakeholder is creating, using, or observing the internet-connected thing. And just to note, the internet-connected things are regular, everyday products and assets — not the personal smartphones, tablets, or PCs that we all use to connect to apps and services." },
                    { 22, 3, "55e46c5e-87a1-467e-8150-771f4c02ac48.pdf", new DateTime(2020, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 54, 8, false, true, null, "InsighHub Infographic: Manufacturing Firms Look Beyond Core IT For Digital Capabilities", 1, "In April and May of 2020, InsightHub surveyed 450 leaders in various manufacturing disciplines across five countries in Asia Pacific to understand their priorities, barriers, and investments. We found that most manufacturers are increasing their IT spending and appear to be doing so to improve efficiency. However, rising customer expectations are forcing manufacturers to also become more customer-obsessed — an attitude that is starting to become evident in where these firms are focusing their tech investments." },
                    { 4, 1, "a246522f-965b-4e8b-8612-dd8857c7036d.pdf", new DateTime(2020, 3, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 24, 1, false, true, null, "Modernize IT With CX As The Core Design Principle", 1, "Agencies need to improve the performance of technology to comply with FITARA and the MGT Act and to fuel mission success. To do this, you need to address corrosive technical debt — spending precious money on systems that add marginal value at outsized costs — and likely organizational inertia.The goal is to place the user — whether a citizen or an employee — at the center of design and modernize to shift debt to leverage. Reverse technical debt to empower the mission." }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "TagId", "ReportId" },
                values: new object[,]
                {
                    { 16, 4 },
                    { 6, 5 },
                    { 5, 5 },
                    { 4, 5 },
                    { 21, 6 },
                    { 22, 6 },
                    { 18, 6 },
                    { 2, 13 },
                    { 4, 19 },
                    { 1, 3 },
                    { 2, 3 },
                    { 4, 22 },
                    { 7, 3 },
                    { 3, 10 },
                    { 14, 10 },
                    { 12, 12 },
                    { 16, 18 },
                    { 3, 18 },
                    { 8, 2 },
                    { 18, 2 },
                    { 10, 2 },
                    { 17, 11 },
                    { 7, 11 },
                    { 2, 10 },
                    { 3, 11 },
                    { 12, 22 },
                    { 12, 21 },
                    { 19, 4 },
                    { 19, 9 },
                    { 24, 9 },
                    { 2, 9 },
                    { 10, 16 },
                    { 8, 16 },
                    { 6, 16 },
                    { 12, 23 },
                    { 13, 23 },
                    { 2, 1 },
                    { 23, 21 },
                    { 11, 1 },
                    { 13, 1 },
                    { 9, 8 },
                    { 26, 8 },
                    { 25, 15 },
                    { 11, 15 },
                    { 4, 20 },
                    { 1, 20 },
                    { 2, 7 },
                    { 11, 7 },
                    { 19, 14 },
                    { 1, 1 },
                    { 6, 17 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_DownloadedReports_ReportId",
                table: "DownloadedReports",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AuthorId",
                table: "Reports",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_IndustryId",
                table: "Reports",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportTags_ReportId",
                table: "ReportTags",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_IndustryId",
                table: "Subscriptions",
                column: "IndustryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DownloadedReports");

            migrationBuilder.DropTable(
                name: "ReportTags");

            migrationBuilder.DropTable(
                name: "Subscriptions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Industries");
        }
    }
}
