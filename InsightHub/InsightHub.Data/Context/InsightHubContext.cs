﻿using InsightHub.Data.EntityModels;
using InsightHub.Data.Seeder;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace InsightHub.Data.Context
{
    public class InsightHubContext : IdentityDbContext<User, Role, int>
    {
        public InsightHubContext(DbContextOptions<InsightHubContext> options)
          : base(options)
        {

        }

        public DbSet<Tag> Tags { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportTags> ReportTags { get; set; }
        public DbSet<Subscriptions> Subscriptions { get; set; }
        public DbSet<DownloadedReports> DownloadedReports { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //SeedingIndustries
            builder.Seeder();

            //SeedingRoles
            builder.Entity<Role>().HasData(new Role
            {
                Id = 1,
                Name = "customer",
                NormalizedName = "CUSTOMER"
            });

            builder.Entity<Role>().HasData(new Role
            {
                Id = 2,
                Name = "admin",
                NormalizedName = "ADMIN"
            });

            builder.Entity<Role>().HasData(new Role
            {
                Id = 3,
                Name = "author",
                NormalizedName = "AUTHOR"
            });

            builder.Entity<DownloadedReports>().HasKey(k => new
            {
                k.UserId,
                k.ReportId
            });

            builder.Entity<ReportTags>().HasKey(k => new
            {
                k.TagId,
                k.ReportId
            });

            builder.Entity<Subscriptions>().HasKey(k => new
            {
                k.UserId,
                k.IndustryId
            });

            var cascadeFKs = builder.Model.GetEntityTypes()
                            .SelectMany(t => t.GetForeignKeys())
                            .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(builder);
        }
    }
}
