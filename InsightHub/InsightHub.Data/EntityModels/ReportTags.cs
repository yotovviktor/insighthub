﻿namespace InsightHub.Data.EntityModels
{
    public class ReportTags
    {
        public int ReportId { get; set; }

        public virtual Report Report { get; set; }

        public int TagId { get; set; }

        public virtual Tag Tag { get; set; }
    }
}