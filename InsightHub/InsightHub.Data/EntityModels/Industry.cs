﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Data.EntityModels
{
    public class Industry
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Report> Reports { get; set; } = new HashSet<Report>();

        public virtual ICollection<Subscriptions> Subscriptions { get; set; } = new HashSet<Subscriptions>();
    }
}
