﻿using InsightHub.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsightHub.Data.EntityModels
{
    public class Report
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Summary { get; set; }

        public int AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public virtual User Author { get; set; }

        public int IndustryId { get; set; }

        [ForeignKey("IndustryId")]
        public virtual Industry Industry { get; set; }

        public Status Status { get; set; }
        
        public string BlobPath { get; set; }

        public int DownloadCount { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFeatured { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public virtual ICollection<ReportTags> Tags { get; set; } = new HashSet<ReportTags>();

        public virtual ICollection<DownloadedReports> Downloads { get; set; } = new HashSet<DownloadedReports>();
    }
}