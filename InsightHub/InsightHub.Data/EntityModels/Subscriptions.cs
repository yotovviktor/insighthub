﻿namespace InsightHub.Data.EntityModels
{
    public class Subscriptions
    {
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public int IndustryId { get; set; }

        public virtual Industry Industry { get; set; }      
    }
}