﻿namespace InsightHub.Data.EntityModels
{
    public class DownloadedReports
    {
        public int ReportId { get; set; }

        public virtual Report Report { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}