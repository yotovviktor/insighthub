﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InsightHub.Data.EntityModels
{
    public class Tag
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<ReportTags> Reports { get; set; } = new HashSet<ReportTags>();
    }
}