﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InsightHub.Data.Enums;
using Microsoft.AspNetCore.Identity;

namespace InsightHub.Data.EntityModels
{
    public class User :IdentityUser<int>
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime RegisteredOn { get; set; }

        [Required]
        public Status Status { get; set; }

        [Required]
        public Data.Enums.Role Role { get; set; }

        public DateTimeOffset? LastLogedIn { get; set; }
         

        public virtual ICollection<DownloadedReports> Reports { get; set; } = new HashSet<DownloadedReports>();
        
        public virtual ICollection<Subscriptions> Subscriptions { get; set; } = new HashSet<Subscriptions>();
    }
}
