﻿namespace InsightHub.Common.DtoModels
{
    public class IndustryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }
    }
}
