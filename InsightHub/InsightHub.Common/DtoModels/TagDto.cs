﻿namespace InsightHub.Common.DtoModels
{
    public class TagDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
