﻿namespace InsightHub.Common.DtoModels
{
    public class SubscriptionDto
    {
        public int UserId { get; set; }

        public int IndustryId { get; set; } 

        public  IndustryDto Industry { get; set; }
    }
}
