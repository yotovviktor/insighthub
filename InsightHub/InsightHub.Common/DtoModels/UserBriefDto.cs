﻿using System;
using System.Collections.Generic;
using System.Text;
using InsightHub.Data.Enums;

namespace InsightHub.Common.DtoModels
{
    public class UserBriefDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public Status Status { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
