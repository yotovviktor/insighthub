﻿using System;
using System.Collections.Generic;
using InsightHub.Data.Enums;

namespace InsightHub.Common.DtoModels
{
    /// <summary>
    /// Properties: Id, IsDisabled,LockoutEnd, FirstName, LastName, RegisteredOn, Password, Status, Email, Reports, Subscriptions
    /// </summary>
    public class UserDto
    {
        public int Id { get; set; }

        public bool IsDisabled { get; set; } = false;

        public DateTimeOffset? LockoutEnd { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime RegisteredOn { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Status Status { get; set; }

        public Data.Enums.Role Role { get; set; }
    }
}
