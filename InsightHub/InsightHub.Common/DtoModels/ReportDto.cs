﻿using InsightHub.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;

namespace InsightHub.Common.DtoModels
{
    public class ReportDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Summary { get; set; }

        public int AuthorId { get; set; }

        public UserBriefDto Author { get; set; }

        public int IndustryId { get; set; }

        public IndustryDto Industry { get; set; }

        public Status Status { get; set; }

        public int DownloadCount { get; set; }

        public Stream Content { get; set; }

        public string ContentType { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFeatured { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public DateTime? DeletedOn { get; set; }

        public ICollection<int> TagIds { get; set; } = new List<int>();

        public ICollection<TagDto> Tags { get; set; } = new HashSet<TagDto>();
    }
}
