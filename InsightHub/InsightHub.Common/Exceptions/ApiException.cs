﻿using System;
using System.Net;

namespace InsightHub.Common.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException()
        {
        }

        public ApiException(HttpStatusCode statusCode, string message)
            :base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; set; }
        
    }
}
