﻿namespace InsightHub.Common.Enums
{
    public enum ReportSortBy
    {
        Name,
        Downloads,
        Date
    }
}
