﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Common.Enums
{
    public enum UserSortBy
    {
        FirstName,
        LastName,
        RegisteredOn
    }
}
