﻿namespace InsightHub.Common.Enums
{
    public enum Order
    {
        Asc,
        Desc
    }
}
