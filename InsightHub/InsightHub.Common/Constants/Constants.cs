﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Common.Constants
{
    public static class Constants
    {
        public static string Notification(string reportName, string industryName, string lastName)
        {
            var result = $"Dear Mr/Mrs {lastName}," + Environment.NewLine
                + $"We are glad to inform you that a new report : \"{reportName}\" has been uploaded to the {industryName} industry." + Environment.NewLine
                + "Stay informed, " + Environment.NewLine
                + "InsightHub";

            return result;
        }

        public static string ConformationEmail(string lastName, string redirect)
        {
            var result = $"Dear Mr/Mrs {lastName}," + Environment.NewLine
                 + "Please confirm your email address by clicking on the following link : " + Environment.NewLine
                 + redirect + Environment.NewLine
                 + "InsightHub";

            return result;
        }

        public const string DefaultErrorMessage = "Opps! Something went wrong..."; 
    }
}
