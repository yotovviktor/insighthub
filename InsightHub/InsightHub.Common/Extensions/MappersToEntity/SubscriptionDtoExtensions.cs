﻿using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Data.EntityModels;

namespace InsightHub.Common.Extensions.MappersToEntity
{
    public static class SubscriptionDtoExtensions
    {
        public static Subscriptions ToEntity(this SubscriptionDto subscriptionDto)
        {
            if (subscriptionDto == null) return null;
            return new Subscriptions
            {
                UserId = subscriptionDto.UserId,
                IndustryId = subscriptionDto.IndustryId
            };
        }
        public static ICollection<Subscriptions> ToEntities(this IEnumerable<SubscriptionDto> subscriptionsDtos)
        {
            if (subscriptionsDtos == null) return null;
            return subscriptionsDtos.Select(x => x.ToEntity()).ToList();
        }
    }
}
