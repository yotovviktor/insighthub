﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System.Net;

namespace InsightHub.Common.Extensions.MappersToEntity
{
    public static class IndustryDtoExtentions
    {
        public static Industry ToEntity(this IndustryDto industryDto)
        {
            if (industryDto == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) industryDto");
            }

            return new Industry
            {
                Id = industryDto.Id,
                Name = industryDto.Name,
                ImageUrl = industryDto.ImageUrl,
                Description = industryDto.Description
            };
        }
    }
}
