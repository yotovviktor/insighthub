﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace InsightHub.Common.Extensions.MappersToEntity
{
    public static class ReportDtoExtentions
    {
        public static Report ToEntity(this ReportDto reportDto)
        {
            if (reportDto == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) reportDto");
            }

            return new Report
            {
                Id = reportDto.Id,
                Name = reportDto.Name,
                Summary = reportDto.Summary,
                AuthorId = reportDto.AuthorId,
                IndustryId = reportDto.IndustryId,
                Industry = reportDto.Industry != null ? reportDto.Industry.ToEntity() : null,
                Status = reportDto.Status,
                DownloadCount = reportDto.DownloadCount,
                IsDeleted = reportDto.IsDeleted,
                IsFeatured = reportDto.IsFeatured,
                CreatedOn = reportDto.CreatedOn,
                ModifiedOn = reportDto.ModifiedOn,
                DeletedOn = reportDto.DeletedOn,
            };
        }

        public static ICollection<Report> ToEntities(this IEnumerable<ReportDto> reportsDtos)
        {
            return reportsDtos.Select(reportDto => reportDto.ToEntity()).ToList();
        }
    }       
}
