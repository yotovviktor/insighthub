﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace InsightHub.Common.Extensions.MappersToEntity
{
    public static class TagDtoExtentions
    {
        public static Tag ToEntity(this TagDto tagDto)
        {
            if (tagDto == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) tagDto");
            }

            return new Tag
            {
                Id = tagDto.Id,
                Name = tagDto.Name,
            };
        }

        public static ICollection<Tag> ToEntities(this IEnumerable<TagDto> tagDtos)
        {
            return tagDtos.Select(tagDto => tagDto.ToEntity()).ToList();
        }
    }
}
