﻿using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Data.EntityModels;

namespace InsightHub.Common.Extensions.MappersToEntity
{
    public static class UserDtoExtensions
    {
        public static User ToEntity(this UserDto userDto)
        {
            if (userDto == null) return null;
            return new User
            {
               Id = userDto.Id,
               FirstName = userDto.FirstName,
               LastName = userDto.LastName,
               Email = userDto.Email,
               LockoutEnd = userDto.LockoutEnd,
               PhoneNumber = userDto.PhoneNumber,
               LockoutEnabled = userDto.IsDisabled,
               Status = userDto.Status,
               RegisteredOn = userDto.RegisteredOn,
               
            };
        }
        public static ICollection<User> ToEntities(this IEnumerable<UserDto> userDtos)
        {
            if (userDtos == null) return null;
            return userDtos.Select(x => x.ToEntity()).ToList();
        }
    }
}
