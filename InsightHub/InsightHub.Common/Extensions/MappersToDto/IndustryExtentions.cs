﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System.Net;

namespace InsightHub.Common.Extensions.MappersToDto
{
    public static class IndustryExtentions
    {
        public static IndustryDto ToDto(this Industry industry)
        {
            if (industry == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) industry");
            }

            return new IndustryDto
            {
                Id = industry.Id,
                Name = industry.Name,
                ImageUrl = industry.ImageUrl,
                Description = industry.Description
            };
        }
    }
}
