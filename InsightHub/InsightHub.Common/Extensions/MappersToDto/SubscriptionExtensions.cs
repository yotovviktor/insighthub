﻿using System.Collections.Generic;
using System.Linq;
using InsightHub.Common.DtoModels;
using InsightHub.Data.EntityModels;

namespace InsightHub.Common.Extensions.MappersToDto
{
    public static class SubscriptionExtensions
    {
        public static SubscriptionDto ToDto(this Subscriptions subscriptionEntity)
        {
            if (subscriptionEntity == null) return null;
            return new SubscriptionDto
            {
                UserId = subscriptionEntity.UserId,
                IndustryId = subscriptionEntity.IndustryId,
               
            };
        }
        public static ICollection<SubscriptionDto> ToDtos(this IEnumerable<Subscriptions> subscriptions)
        {
            if (subscriptions == null) return null;
            return subscriptions.Select(x => x.ToDto()).ToList();
        }
    }
}
