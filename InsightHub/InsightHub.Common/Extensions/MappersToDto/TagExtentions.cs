﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace InsightHub.Common.Extensions.MappersToDto
{
    public static class TagExtentions
    {
        public static TagDto ToDto(this Tag tag)
        {
            if (tag == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) tag");
            }

            return new TagDto
            {
                Id = tag.Id,
                Name = tag.Name,
            };
        }

        public static ICollection<TagDto> ToDtos(this IEnumerable<Tag> tags)
        {
            return tags.Select(tag => tag.ToDto()).ToList();
        }
    }
}
