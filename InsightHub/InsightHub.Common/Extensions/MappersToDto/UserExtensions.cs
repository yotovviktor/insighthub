﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;

namespace InsightHub.Common.Extensions.MappersToDto
{
    public static class UserExtensions
    {
        /// <summary>
        /// Mapes UserEntity to UserDto
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        public static UserDto ToDto(this User userEntity)
        {
            if (userEntity == null)
            {
                return null;
            }
            return new UserDto
            {
                Id = userEntity.Id,
                IsDisabled = userEntity.LockoutEnabled,
                LockoutEnd = userEntity.LockoutEnd,
                FirstName = userEntity.FirstName,
                LastName = userEntity.LastName,
                Email = userEntity.Email,
                RegisteredOn = userEntity.RegisteredOn,
                Status = userEntity.Status,
                PhoneNumber = userEntity.PhoneNumber,
                Role = userEntity.Role
            };
        }
        /// <summary>
        /// Maps Collection of Users to UserDtos
        /// </summary>
        /// <param name="userEntities"></param>
        /// <returns></returns>
        public static ICollection<UserDto> ToDtos(this IEnumerable<User> userEntities)
        {
            if (userEntities == null) return null;
            return userEntities.Select(x => x.ToDto()).ToList();
        }

        public static UserBriefDto ToBriefDto(this User userEntity)
        {

            if (userEntity == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"Invalid (null) argument {nameof(userEntity)}");
            }

            return new UserBriefDto
            {
                Id = userEntity.Id,
                FirstName = userEntity.FirstName,
                LastName = userEntity.LastName,
                Email = userEntity.Email,
                Status = userEntity.Status,
                CreatedOn = userEntity.RegisteredOn
            };
        }

        public static ICollection<UserBriefDto> ToBriefDtos(this IEnumerable<User> userEntities)
        {
            if (userEntities == null) throw new ApiException(HttpStatusCode.NotFound, $"Invalid (null) argument {nameof(userEntities)}");
            
            return userEntities.Select(x => x.ToBriefDto()).ToList();
        }
    }

}
