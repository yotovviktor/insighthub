﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.EntityModels;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace InsightHub.Common.Extensions.MappersToDto
{
    public static class ReportExtentions
    {
        public static ReportDto ToDto(this Report report)
        {
            if (report == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) report");
            }

            return new ReportDto
            {
                Id = report.Id,
                Name = report.Name,
                Summary = report.Summary,
                AuthorId = report.AuthorId,
                IndustryId = report.IndustryId,
                Industry = report.Industry != null ? report.Industry.ToDto() : null,
                Author = report.Author != null ? report.Author.ToBriefDto() : null,
                Status = report.Status,
                DownloadCount = report.DownloadCount,
                IsDeleted = report.IsDeleted,
                IsFeatured = report.IsFeatured,
                CreatedOn = report.CreatedOn,
                ModifiedOn = report.ModifiedOn,
                DeletedOn = report.DeletedOn,
                TagIds = report.Tags.Select(reportTag => reportTag.Tag.Id).ToList(),
                Tags = report.Tags.Select(reportTag => reportTag.Tag).ToList().ToDtos()
            };
        }

        public static ICollection<ReportDto> ToDtos(this IEnumerable<Report> reports)
        {
            return reports.Select(report => report.ToDto()).ToList();
        }
    }
}
