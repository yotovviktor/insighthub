﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Common.Models
{
    public class Paging
    {
        public int? Limit { get; set; }

        public int? Offset { get; set; }

        public int? TotalCount { get; set; }
    }
}
