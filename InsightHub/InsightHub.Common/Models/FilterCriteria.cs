﻿using InsightHub.Data.Enums;

namespace InsightHub.Common.Models
{
    public class FilterCriteria
    {
        public string Name { get; set; }

        public int? IndustryId { get; set; }

        public int? AuthorId { get; set; }

        public bool? IsFeatured { get; set; }

        public Status? Status { get; set; }

        public int[] TagIds { get; set; }       
    }
}
