﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InsightHub.Common.Models
{
    public interface IContent
    {
        public Stream Data { get; set; }

        public string ContentType { get; set; }
    }
}
