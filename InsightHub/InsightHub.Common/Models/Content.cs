﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InsightHub.Common.Models
{
    public class Content : IContent
    {
        public Content(string contentType, Stream data)
        {
            this.ContentType = contentType;
            this.Data = data;
        }

        public string ContentType { get; set; }
        public Stream Data { get; set; }
    }
}
