﻿using InsightHub.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IContentService
    {
        /// <summary>
        /// Asynchronously uploads a content
        /// </summary>
        /// <example>
        /// var content = new Content("text/plain", new MemoryStream(Encoding.UTF8.GetBytes("Hello World")));
        /// var provider = new AzureContentService();
        /// var path = await provider.UploadContentAsync(content);
        /// </example>
        /// <returns>
        /// Returns content path
        /// </returns>
        public Task<string> UploadContentAsync(IContent content);

        /// <summary>
        /// Asynchronously download a content
        /// </summary>
        /// <example>
        /// var provider = new AzureContentService();
        /// var content = await provider.DownloadContentAsync("test.txt");
        /// StreamReader reader = new StreamReader(content.Data);
        /// var text = reader.ReadToEnd();
        /// </example>
        public Task<IContent> DownloadContentAsync(string path);

        /// <summary>
        /// Asynchronously delete a content
        /// </summary>
        /// <example>
        /// var provider = new AzureContentService();
        /// await provider.DeleteContentAsync("test.txt");
        /// </example>
        public Task DeleteContentAsync(string path);
    }
}
