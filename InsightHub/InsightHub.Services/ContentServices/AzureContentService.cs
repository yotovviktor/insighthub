﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using HeyRed.Mime;
using InsightHub.Common.Models;
using InsightHub.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace InsightHub.Services.ContentServices
{
    /// <summary>
    /// This class is a concrete implementation of IContentService. 
    /// It takes the advantage of using Azure blob storage for storing content.
    /// The class provides methods for managing content - upload, download and delete.
    /// </summary>
    public class AzureContentService : IContentService
    {
        private const string CONTAINER_NAME = "uploads";

        private readonly BlobContainerClient _containerClient;

        public AzureContentService(BlobServiceClient blobServiceClient)
        {
            _containerClient = blobServiceClient.GetBlobContainerClient(CONTAINER_NAME);
            _containerClient.CreateIfNotExists();
        }
 
        public async Task<IContent> DownloadContentAsync(string path)
        {
            var blobClient = _containerClient.GetBlobClient(path);
            var downloadInfo = await blobClient.DownloadAsync();

            return new Content(downloadInfo.Value.ContentType, downloadInfo.Value.Content);
        }

        public async Task<string> UploadContentAsync(IContent content)
        {
            string path = GenerateUniquePath(content.ContentType);
            var blobClient = _containerClient.GetBlobClient(path);

            var headers = new BlobHttpHeaders { ContentType = content.ContentType };
            await blobClient.UploadAsync(content.Data, headers);

            return path;
        }

        private string GenerateUniquePath(string contentType)
        {
            string extension = MimeTypesMap.GetExtension(contentType);
            string name = Guid.NewGuid().ToString();

            return $"{name}.{extension}";
        }

        public async Task DeleteContentAsync(string path)
        {
            var blobClient = _containerClient.GetBlobClient(path);
            await blobClient.DeleteIfExistsAsync();
        }
    }
}
