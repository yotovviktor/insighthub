﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Services.TagServices
{
    /// <summary>
    /// This class is a concrete implementation of ITagService. 
    /// The class provides methods for managing tags - create, read, search, update and delete.
    /// </summary>
    public class TagService : ITagService
    {
        private readonly InsightHubContext _context;

        public TagService(InsightHubContext context)
        {
            _context = context ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(context));
        }

        public async Task<ICollection<TagDto>> ListTagsAsync()
        {
            return await ListTagsAsync(null);
        }

        public async Task<ICollection<TagDto>> ListTagsAsync(string keyword)
        {
            IQueryable<Tag> query = _context.Tags;

            if (!String.IsNullOrEmpty(keyword))
            {
                query = query.Where(tag => tag.Name.ToLower().Contains(keyword.ToLower()));
            }

            return await query.OrderBy(tag => tag.Name).Select(tag => tag.ToDto()).ToListAsync();
        }

        public async Task<TagDto> CreateTagAsync(TagDto tagDto)
        {
            tagDto = tagDto ?? throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) tag");
            await ValidateTagNameNotExistsAsync(tagDto.Name);

            var tag = tagDto.ToEntity();

            await _context.Tags.AddAsync(tag);
            await _context.SaveChangesAsync();

            return tag.ToDto();
        }

        private async Task ValidateTagNameNotExistsAsync(string tagName)
        {
            bool tagNameExists = await _context.Tags.AnyAsync(tag => tag.Name == tagName);

            if (tagNameExists)
            {
                throw new ApiException(HttpStatusCode.Conflict, $"Tag with name: {tagName} already exists");
            }
        }

        public async Task UpdateTagAsync(TagDto tagDto)
        {
            var tag = await TryGetTagAsync(tagDto.Id);
            await ValidateTagNameNotExistsAsync(tagDto.Name);

            tag.Name = tagDto.Name;

            _context.Tags.Update(tag);
            await _context.SaveChangesAsync();
        }

        private async Task<Tag> TryGetTagAsync(int id)
        {
            var tag = await _context.Tags.FirstOrDefaultAsync(tag => tag.Id == id);
            tag = tag ?? throw new ApiException(HttpStatusCode.NotFound, $"There is no tag with id: {id}");

            return tag;
        }

        public async Task DeleteTagAsync(int id)
        {
            var tag = await TryGetTagAsync(id);
            await ValidateTagIsNotUsed(tag.Id);

            _context.Tags.Remove(tag);
            await _context.SaveChangesAsync();
        }

        private async Task ValidateTagIsNotUsed(int id)
        {
            bool isTagInUse = await _context.ReportTags.AnyAsync(reportTag => reportTag.TagId == id);

            if (isTagInUse)
            {
                throw new ApiException(HttpStatusCode.Conflict, "There is a report with this tag");
            }
        }
    }
}
