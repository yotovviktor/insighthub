﻿using InsightHub.Common.DtoModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.TagServices
{
    public interface ITagService
    {
        /// <summary>
        /// Asynchronously retrieves all tags ordered by tag name
        /// </summary>
        /// <returns>
        /// Returns a collection of objects of TagDto class.
        /// </returns>
        Task<ICollection<TagDto>> ListTagsAsync();

        /// <summary>
        /// Asynchronously retrieves all tags that match the keyword, ordered by tag name
        /// </summary>
        /// <returns>
        /// Returns a collection of objects of TagDto class.
        /// </returns>
        Task<ICollection<TagDto>> ListTagsAsync(string keyword);

        /// <summary>
        /// Asynchronously creates a new Tag object by given object of TagDto class
        /// </summary>
        /// <returns>
        /// Returns an object of TagDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag object is null</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag with same name already exists</exception>
        Task<TagDto> CreateTagAsync(TagDto tag);

        /// <summary>
        /// Asynchronously updates a existing Tag object by given object of TagDto class
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag with that ID is not found</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag with same name already exists</exception>
        Task UpdateTagAsync(TagDto tag);

        /// <summary>
        /// Asynchronously deletes a existing Tag object by given id
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag with that ID is not found</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a tag is currently used by report</exception>
        Task DeleteTagAsync(int id);
    }
}
