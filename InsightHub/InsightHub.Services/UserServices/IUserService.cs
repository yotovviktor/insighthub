﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Enums;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;

namespace InsightHub.Services.UserServices
{
    public interface IUserService
    {
        /// <summary>
        /// Gets a User by given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns cref="UserDto"></returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown wen userId is out of range</exception>
        public Task<UserDto> GetUserByIdAsync(int id);

        /// <summary>
        /// Gets all the users in the Database
        /// </summary>
        /// <returns cref="IEnumerable{UserDto}"></returns>
        public Task<IEnumerable<UserDto>> GetAllUsersAsync();

        /// <summary>
        /// Changes the Email Property to the given new one.
        /// </summary>
        /// <param name="id">The Id of the user</param>
        /// <param name="newEmail">The new Email</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when id is out of range with HttpStatuscode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when email is the same as the old one with HttpStatuscode.Conflict</exception>

        public Task EditEmailAsync(int id, string newEmail);

        /// <summary>
        /// Changes the Password Property to the given new one of user with the given id.
        /// </summary>
        /// <param name="id">The Id of the user</param>
        /// <param name="newPassword">The new password</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when id is out of range with HttpStatuscode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when Password is the same as the old one with HttpStatuscode.Conflict</exception>
        public Task EditPasswordAsync(int id, string newPassword);

        /// <summary>
        /// Changes the FirstName to the given one of the user with the given id 
        /// </summary>
        /// <param name="Id">The id of the user</param>
        /// <param name="NewFirstName">The new FirstName</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when id is out of range with HttpStatuscode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when FirstName is the same as the old one with HttpStatuscode.Conflict</exception> 
        public Task EditFirstNameAsync(int Id, string newFirstName);
        
        /// <summary>
        /// Sets the last loged in date of the user
        /// </summary>
        /// <param name="userId">The id of the user</param>
        public Task EditTheLastLogInDateAsync(int userId);

        /// <summary>
        /// Changes the LastName to the given one of the user with the given id 
        /// </summary>
        /// <param name="Id">The id of the user</param>
        /// <param name="NewFirstName">The new LastName</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when id is out of range with HttpStatuscode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when last is the same as the old one with HttpStatuscode.Conflict</exception>
        public Task EditLastNameAsync(int id, string newLastName);

        /// <summary>
        /// Changes the PhoneNumber to the given one of the user with the given id 
        /// </summary>
        /// <param name="Id">The id of the user</param>
        /// <param name="NewFirstName">The new PhoneNumber</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when id is out of range with HttpStatuscode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when Phonenumber is the same as the old one with HttpStatuscode.Conflict</exception>
        public Task EditPhoneNumberAsync(int id, string newPhoneNumber);


        /// Disables User for a certain period of time
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <param name="LockoutEnd">The end date of the lockout</param>
        ///<exception cref="Common.Exceptions.ApiException">Throws ApiException when user id is out of range with HttpStatusCode.NotFound</exception> 
        ///<exception cref="Common.Exceptions.ApiException">Throws ApiException when LochOutEnd is in the past with HttpStatusCode.Conflict</exception> 
        public Task DisableUserAsync(int userId, DateTimeOffset LockoutEnd);

        /// <summary>
        /// Enables User that has been disabled
        /// </summary>
        /// <param name="userId">The id of the user</param>
        ///<exception cref="Common.Exceptions.ApiException">Throws ApiException when user id is out of range with HttpStatusCode.NotFound</exception> 
        ///<exception cref="Common.Exceptions.ApiException">Throws ApiException when the user is already enabled is in the past with HttpStatusCode.Conflict</exception> 
        public Task EnableUserAsync(int userId);

        /// <summary>
        /// Changes the status of a user by given id
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <param name="status">The new status </param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when user id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the new status is the same as the old one with HttpStatusCode.Conflict</exception>
        public Task ChangeUserStatusAsync(int userId, Status status);


        /// <summary>
        /// Sets the property EmailConfirmed to true for a specific user by id.
        /// </summary>
        /// <param name="userId">The Id of the user</param>
        /// <exception cref="Common.Exceptions.ApiException">Throws ApiException with status code HttpStatusCode.NotFound when user id is out of range</exception>
        /// <exception cref="Common.Exceptions.ApiException">Throws ApiException with status code HttpStatusCode.Conflict when the email has already bbeen confirmed</exception>
        public Task ConfirmEmailAsync(int userId);

        /// <summary>
        ///  Retursn UserDtoModels of the users fitered by the criteria: role, sortBy, keyword, status, orderBy
        /// </summary>
        /// <param name="role">The desired role</param>
        /// <param name="sortBy"> the property that the users will be sort by, default is last name </param>
        /// <param name="keyword"></param>
        /// <param name="status"></param>
        /// <param name="orderBy">asc or dsc,default is dsc</param>
        /// <returns></returns>
        public Task<IEnumerable<UserDto>> FilterAsync(Data.Enums.Role? role, UserSortBy? sortBy, string keyword, Status? status, Order? orderBy);

        /// <summary>
        /// Checks wether or not tge status of the user is approved. 
        /// </summary>
        /// <param name="email">The email of the user (the email is unique for each user)</param>
        /// <returns cref="bool">returns true if the status is approved</returns>
        public Task<bool> IsUserApprovedAsync(string email);
    }
}
