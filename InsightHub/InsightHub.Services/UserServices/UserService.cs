﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Enums;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Data.Enums;
using InsightHub.Services.EmailSenderService;
using InsightHub.Services.SubscriptionServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace InsightHub.Services.UserServices
{
    public class UserService : IUserService
    {
        public UserService(InsightHubContext insightHubContext, ISubscriptionService subscriptionService, IEmailSender emailSender)
        {
            InsightHubContext = insightHubContext ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) context");
            SubscriptionService = subscriptionService ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) subscription service");
            EmailSender = emailSender ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) emailSender");
        }


        private InsightHubContext InsightHubContext { get; set; }

        private ISubscriptionService SubscriptionService { get; set; }

        private IEmailSender EmailSender { get; set; }

        public async Task EditEmailAsync(int id, string newEmail)
        {
            var userToBeEdited = await TryGetUserAsync(id);

            if (userToBeEdited.Email != newEmail)
            {
                if (await this.InsightHubContext.Users.AnyAsync(x => x.Email == newEmail))
                {
                    throw new ApiException(HttpStatusCode.Conflict, $"There is already a user registered with email {newEmail}");
                }

                userToBeEdited.Email = newEmail;
                this.InsightHubContext.Users.Update(userToBeEdited);
                await this.InsightHubContext.SaveChangesAsync();
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, $"Email is the same as the old one");
            }
        }

        public async Task EditFirstNameAsync(int id, string newFirstName)
        {
            var userToBeEdited = await TryGetUserAsync(id);

            if (userToBeEdited.FirstName != newFirstName)
            {
                userToBeEdited.FirstName = newFirstName;
                this.InsightHubContext.Users.Update(userToBeEdited);
                await this.InsightHubContext.SaveChangesAsync();
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The first name is the same as the old one");
            }
        }

        public async Task EditLastNameAsync(int id, string newLastName)
        {
            var userToBeEdited = await TryGetUserAsync(id);

            if (userToBeEdited.LastName != newLastName)
            {
                userToBeEdited.LastName = newLastName;
                this.InsightHubContext.Users.Update(userToBeEdited);
                await this.InsightHubContext.SaveChangesAsync();
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The Last name is the same as the old one");
            }
        }

        public async Task EditPasswordAsync(int id, string newPassword)
        {
            var userToBeEdited = await TryGetUserAsync(id);

            if (userToBeEdited.PasswordHash != newPassword)
            {
                userToBeEdited.PasswordHash = newPassword;
                this.InsightHubContext.Users.Update(userToBeEdited);
                await this.InsightHubContext.SaveChangesAsync();
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The password is the same as the old one");
            }
        }

        public async Task EditPhoneNumberAsync(int id, string newPhoneNumber)
        {
            var userToBeEdited = await TryGetUserAsync(id);

            if (userToBeEdited.PhoneNumber != newPhoneNumber)
            {
                userToBeEdited.PhoneNumber = newPhoneNumber;
                this.InsightHubContext.Users.Update(userToBeEdited);
                await this.InsightHubContext.SaveChangesAsync();
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The phonenumber is the same as the old one");
            }
        }

        public async Task<IEnumerable<UserDto>> GetAllUsersAsync()
        {
            return (await this.InsightHubContext.Users.ToListAsync()).ToDtos();
        }

        public async Task<UserDto> GetUserByIdAsync(int id)
        {
            var user = await TryGetUserAsync(id);

            return user.ToDto();

        }

        public async Task DisableUserAsync(int userId, DateTimeOffset lockoutEnd)
        {
            var userToBeDisabled = await TryGetUserAsync(userId);
            userToBeDisabled.LockoutEnabled = true;
            //Setting the LockOutEndDate
            if (lockoutEnd > DateTimeOffset.Now)
            {
                userToBeDisabled.LockoutEnd = lockoutEnd;
            }
            else
            {
                throw new ApiException(HttpStatusCode.Conflict, "The date is in the past");
            }
        }

        public async Task EnableUserAsync(int userId)
        {
            var userToBeEnabled = await TryGetUserAsync(userId);

            //Checks if the user is already Enabled
            if (userToBeEnabled.LockoutEnabled == false)
            {
                throw new ApiException(HttpStatusCode.Conflict, "The user is not Disabled");
            }

            //Enables User
            userToBeEnabled.LockoutEnabled = false;
            userToBeEnabled.LockoutEnd = default;

            this.InsightHubContext.Update(userToBeEnabled);
            await this.InsightHubContext.SaveChangesAsync();
        }

        public async Task ChangeUserStatusAsync(int userId, Status status)
        {
            var userToBeEdited = await TryGetUserAsync(userId);

            //Checks if the status is the same as the old one
            if (userToBeEdited.Status == status)
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The user is already {status}");
            }

            //Changes the status 
            userToBeEdited.Status = status;
            InsightHubContext.Update(userToBeEdited);
            await InsightHubContext.SaveChangesAsync();

        }


        public async Task ConfirmEmailAsync(int userId)
        {
            var usertoBeConfirmed = await TryGetUserAsync(userId);

            if (usertoBeConfirmed.EmailConfirmed)
            {
                throw new ApiException(HttpStatusCode.Conflict, "The email has already been confirmed");
            }

            usertoBeConfirmed.EmailConfirmed = true;

            this.InsightHubContext.Users.Update(usertoBeConfirmed);
            await this.InsightHubContext.SaveChangesAsync();
        }

        private async Task<User> TryGetUserAsync(int userId)
        {
            var user = await this.InsightHubContext.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no user with id: {userId}");
            }

            return user;
        }

        public async Task<IEnumerable<UserDto>> FilterAsync(Data.Enums.Role? role, UserSortBy? sortBy, string keyword, Status? status, Order? orderBy)
        {
            IQueryable<User> query = this.InsightHubContext.Users;
            query = GetOrder(query, orderBy, sortBy);

            if (status != null)
            {
                query = query.Where(x => x.Status == status);
            }

            if (keyword != null)
            {
                query = SearchByKeyword(query, keyword);
            }



            if (role != null)
            {
                query = GetUsersInRoleAsync(query, role);
            }

            var result = await Task.FromResult(query.ToList());
            return result.ToDtos();

        }

        private IQueryable<User> GetUsersInRoleAsync(IQueryable<User> filteredUsers, Data.Enums.Role? role)
        {
            return filteredUsers.Where(x => x.Role == role);
        }

        private IQueryable<User> GetOrder(IQueryable<User> query, Order? orderBy, UserSortBy? sortBy)
        {
            if (sortBy == null) sortBy = UserSortBy.FirstName;

            if (orderBy == null) orderBy = Order.Desc;

            switch (orderBy)
            {
                case Order.Asc:
                    return query.OrderBy(GetSorter(sortBy)).AsQueryable();
                default:
                    return query.OrderByDescending(GetSorter(sortBy)).AsQueryable();
            }
        }

        private Func<User, object> GetSorter(UserSortBy? sortBy)
        {
            return (user) =>
            {
                switch (sortBy)
                {
                    case UserSortBy.FirstName:
                        return user.FirstName;

                    case UserSortBy.LastName:
                        return user.LastName;

                    case UserSortBy.RegisteredOn:
                        return user.RegisteredOn;

                    default:
                        return user.Id;
                }
            };
        }

        private IQueryable<User> SearchByKeyword(IQueryable<User> query, string keyword)
        {
            var result = query
                  .Where(u =>
                    u.FirstName.ToLower().Contains(keyword.ToLower()) == true ||
                    u.LastName.ToLower().Contains(keyword.ToLower()) == true ||
                    u.Email.ToLower().Contains(keyword.ToLower()) == true)
                  .AsQueryable();

            return result;
        }

        public async Task<bool> IsUserApprovedAsync(string email)
        {
            var user = await this.InsightHubContext.Users
                .FirstOrDefaultAsync(x => x.Email == email);
           
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no user with email:{email}");
            }
            else if (user.Status == Status.Approved)
            {
                return true;
            }

            return false;

        }

        public async Task EditTheLastLogInDateAsync(int userId)
        {
            var user = await TryGetUserAsync(userId);
            user.LastLogedIn = DateTimeOffset.UtcNow;

            this.InsightHubContext.Users.Update(user);
            await this.InsightHubContext.SaveChangesAsync();
        }
    }
}
