﻿using InsightHub.Common.DtoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.IndustryServices
{
    public interface IIndustryService
    {
        /// <summary>
        /// Asynchronously finds the industry with the given id
        /// </summary>
        /// <returns>
        /// Returns an object of IndustryDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry with that ID is not found</exception>
        Task<IndustryDto> GetIndustryAsync(int id);

        /// <summary>
        /// Asynchronously retrieves a collection of all industries
        /// </summary>
        /// <returns>
        /// Returns a collection of objects of IndustryDto class.
        /// </returns>
        Task<ICollection<IndustryDto>> ListIndustriesAsync();

        /// <summary>
        /// Asynchronously creates a new Industry object by given object of IndustryDto class
        /// </summary>
        /// <returns>
        /// Returns an object of IndustryDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry object is null</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry with same name already exists</exception>
        Task<IndustryDto> CreateIndustryAsync(IndustryDto industry);

        /// <summary>
        /// Asynchronously updates a existing Industry object by given object of IndustryDto class
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry with that ID is not found</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry with same name already exists</exception>
        Task UpdateIndustryAsync(IndustryDto industry);

        /// <summary>
        /// Asynchronously deletes a existing Industry object by given id
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry with that ID is not found</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a industry is currently used by report</exception>
        Task DeleteIndustryAsync(int id);
    }
}
