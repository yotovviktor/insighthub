﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Services.IndustryServices
{
    /// <summary>
    /// This class is a concrete implementation of IIndustryService. 
    /// The class provides methods for managing industry - create, read, update and delete.
    /// </summary>
    public class IndustryService : IIndustryService
    {
        private readonly InsightHubContext _context;

        public IndustryService(InsightHubContext context)
        {
            _context = context ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(context));
        }

        public async Task<IndustryDto> GetIndustryAsync(int id)
        {
            var industry = await TryGetIndustryAsync(id);

            return industry.ToDto();
        }

        public async Task<ICollection<IndustryDto>> ListIndustriesAsync()
        {
            return await _context.Industries.OrderBy(industry => industry.Name).Select(industry => industry.ToDto()).ToListAsync();
        }

        public async Task<IndustryDto> CreateIndustryAsync(IndustryDto industryDto)
        {
            industryDto = industryDto ?? throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) industry");
            await ValidateIndustryNameNotExistsAsync(industryDto.Name);

            var industry = industryDto.ToEntity();

            await _context.Industries.AddAsync(industry);
            await _context.SaveChangesAsync();

            return industry.ToDto();
        }

        public async Task UpdateIndustryAsync(IndustryDto industryDto)
        {
            var industry = await TryGetIndustryAsync(industryDto.Id);
            await ValidateIndustryNameNotExistsAsync(industryDto.Name);

            industry.Name = industryDto.Name;

            _context.Industries.Update(industry);
            await _context.SaveChangesAsync();
        }

        private async Task<Industry> TryGetIndustryAsync(int id)
        {
            var industry = await _context.Industries.FirstOrDefaultAsync(industry => industry.Id == id);
            industry = industry ?? throw new ApiException(HttpStatusCode.NotFound, $"There is no industry with id: {id}");

            return industry;
        }

        private async Task ValidateIndustryNameNotExistsAsync(string industryName)
        {
            bool industryNameExists = await _context.Industries.AnyAsync(industry => industry.Name == industryName);

            if (industryNameExists)
            {
                throw new ApiException(HttpStatusCode.Conflict, $"Industry with name: {industryName} already exists");
            }
        }

        public async Task DeleteIndustryAsync(int id)
        {
            var industry = await TryGetIndustryAsync(id);
            await ValidateIndustryIsNotUsed(industry.Id);

            _context.Industries.Remove(industry);
            await _context.SaveChangesAsync();
        }

        private async Task ValidateIndustryIsNotUsed(int id)
        {
            bool isIndustryInUse = await _context.Reports.AnyAsync(report => report.IndustryId == id);

            if (isIndustryInUse)
            {
                throw new ApiException(HttpStatusCode.Conflict, "This industry is currently used by a report");
            }
        }
    }
}
