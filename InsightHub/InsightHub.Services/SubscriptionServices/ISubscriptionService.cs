﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;

namespace InsightHub.Services.SubscriptionServices
{
    public interface ISubscriptionService
    {
        /// <summary>
        /// Adds Subscription to the Subscriptions list of the User
        /// </summary>
        /// <param name="userId">The Id of the user</param>
        /// <param name="subscriptionId">The id of the industry</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when user id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the subscription has already been made with HttpStatusCode.Conflict</exception>
        public Task SubscribeForIndustryAsync(int userId, int subscriptionId);

        /// <summary>
        /// Deletes subscription by given User Id and Industry Id
        /// </summary>
        /// <param name="userId">The Id of the user</param>
        /// <param name="subscriptionId">The id of the industry</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when user id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the subscription does not exist HttpStatusCode.NotFound</exception>
        public Task DeleteSubscriptionAsync(int userId, int subscriptionId);

        /// <summary>
        /// Calls email sender with all the subscribers` ids
        /// </summary>
        /// <param name="industryId">The id of the undustry</param>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when IndustryId is out of range with status code HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when ReportId is out of range with status code HttpStatusCode.NotFound</exception>
        public Task NotifySubscribers(int industryId, int reportId);

        /// <summary>
        /// Lists all the subscriptions for a specific user
        /// </summary>
        /// <param name="userId">The subscriber`s Id</param>
        /// <exception cref="Common.Exceptions.ApiException">Throwen when user id is out of range</exception>
        public Task<IEnumerable<IndustryDto>> ListSubsciptionsOfUserAsync(int userId);

        /// <summary>
        /// Checks wether or not a user has already subscribed for a specific industry
        /// </summary>
        /// <param name="userId">The id of the user</param>
        /// <param name="industryId">The id of the industry</param>
        /// <returns cref="bool">Return</returns>
        public Task<bool> IsTheUserSubscribedForIndustryAsync(int userId, int industryId);
       
    }
}
