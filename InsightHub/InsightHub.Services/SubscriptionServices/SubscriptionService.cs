﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Services.EmailSenderService;
using Microsoft.EntityFrameworkCore;

namespace InsightHub.Services.SubscriptionServices
{
    /// <summary>
    /// This class is a concrete implementation of IContentService. 
    /// The class provides methods for managing subsciptions - subscibe, delete and notify subscribers.
    /// </summary>

    public class SubscriptionService : ISubscriptionService
    {
        public SubscriptionService(InsightHubContext insightHubContext)
        {
            InsightHubContext = insightHubContext ?? throw new ApiException(HttpStatusCode.InternalServerError,"Invalid (null) context");
        }

        public SubscriptionService(InsightHubContext insightHubContext, IEmailSender emailSender) : this(insightHubContext)
        {
            EmailSender = emailSender ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) EmailService");
        }

        private InsightHubContext InsightHubContext { get; set; }

        private IEmailSender EmailSender { get; set; }

        public async Task SubscribeForIndustryAsync(int userId, int industryId)
        {
            var subscriber = await TryGetUserAsync(userId);

            var industryToBeSubscribedFor = await TryGetIndustryAsync(industryId);

            if(await this.InsightHubContext.Subscriptions.AnyAsync(s=>s.UserId == userId && s.IndustryId == industryId))
            {
                throw new ApiException(HttpStatusCode.Conflict, "This user is already subscribed fot this industry");
            }

            await this.InsightHubContext.Subscriptions.AddAsync(new Subscriptions
            {
                UserId = userId,
                IndustryId = industryId
            });
            await this.InsightHubContext.SaveChangesAsync();


        }

        public async Task DeleteSubscriptionAsync(int userId, int industryId)
        {
            //NullChecks
            await TryGetUserAsync(userId);
            await TryGetIndustryAsync(industryId);

            //Gets the subscription if existing
            var subscriptionToBeDeleted = await TryGetSubscriptionAsync(userId, industryId);

            //Deletes the subscription
            this.InsightHubContext.Subscriptions.Remove(subscriptionToBeDeleted);
            await this.InsightHubContext.SaveChangesAsync();

        }

        private async Task<User> TryGetUserAsync(int userId)
        {
            var user = await this.InsightHubContext.Users
                 .Include(u => u.Subscriptions)
                 .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no user with id: {userId}");
            }

            return user;
        }

        private async Task<Industry> TryGetIndustryAsync(int industryId)
        {
            var industry = await this.InsightHubContext.Industries
                 .Include(u => u.Subscriptions)
                 .FirstOrDefaultAsync(u => u.Id == industryId);

            if (industry == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no industry with id: {industryId}");
            }
            return industry;
        }

        private async Task<Subscriptions> TryGetSubscriptionAsync(int userId, int industryId)
        {
            var subscription = await this.InsightHubContext.Subscriptions
                 .FirstOrDefaultAsync(s => s.UserId == userId && s.IndustryId == industryId);

            if (subscription == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"The user {userId} is not subscribed for industry {industryId}");
            }
            return subscription;
        }

        public async Task NotifySubscribers(int industryId, int reportId)
        {
            //Nullcheck
            await TryGetIndustryAsync(industryId);

            if(!(await this.InsightHubContext.Reports.AnyAsync(r=>r.Id == reportId)))
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no report with id: {reportId}");
            }

            //Gets the subscribers` ids
            var subscribersIds = await this.InsightHubContext.Subscriptions
                .Where(s => s.IndustryId == industryId)
                .Select(s => s.UserId)
                .ToListAsync();

            await EmailSender.NotifyEmailProvider(subscribersIds, reportId);
        }

        public async Task<IEnumerable<IndustryDto>> ListSubsciptionsOfUserAsync(int userId)
        {
            //Nulcheck
            await TryGetUserAsync(userId);

            return await this.InsightHubContext.Subscriptions
                .Include(s => s.Industry)
                .Where(s => s.UserId == userId)
                .Select(s => s.Industry.ToDto())
                .ToListAsync();     
        }

        public async Task<bool> IsTheUserSubscribedForIndustryAsync(int userId, int industryId)
        {
            await TryGetUserAsync(userId);
            await TryGetIndustryAsync(industryId);
            return await this.InsightHubContext.Subscriptions
                .AnyAsync(x => x.UserId == userId && x.IndustryId == industryId);
        }
    }
}
