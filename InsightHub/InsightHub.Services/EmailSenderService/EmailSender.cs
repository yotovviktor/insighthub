﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Common.Constants;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using Microsoft.EntityFrameworkCore;

namespace InsightHub.Services.EmailSenderService
{
    /// <summary>
    /// This is a default implementation of the IEmailSender interface.
    /// It Contains methods to send emails to notify users for new report and to send confirmation emails
    /// </summary>
    public class EmailSender : IEmailSender
    {
        public EmailSender(InsightHubContext insightHubContext, SmtpClient smtpClient)
        {
            InsightHubContext = insightHubContext ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) context");
            SmtpClient = smtpClient ?? throw new ApiException(HttpStatusCode.InternalServerError, "Invalid (null) Smtp Client");
        }

        private InsightHubContext InsightHubContext { get; set; }

        private SmtpClient SmtpClient { get; set; }

        public async Task SendEmailToNotify(string lastName, string reportName, string industryName, string emailAddress)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.To.Add(new MailAddress(emailAddress));
                email.From = new MailAddress("info.insighthub@gmail.com");
                email.Subject = "New Report";
                email.Body = Constants.Notification(reportName, industryName, lastName);

                await SmtpClient.SendMailAsync(email);
            }
            catch (Exception ex)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private async Task<User> TryGetUser(int userId)
        {
            var user = await this.InsightHubContext.Users
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "Invalid (null) user");
            }

            return user;
        }

        private async Task<Report> TryGetReport(int reportId)
        {
            var report = await this.InsightHubContext.Reports
                .Include(r => r.Industry)
                .FirstOrDefaultAsync(r => r.Id == reportId);

            if (report == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no report with id: {reportId}");
            }

            return report;
        }

        public async Task NotifyEmailProvider(IEnumerable<int> subscribersIds, int reportId)
        {
            var report = await TryGetReport(reportId);

            foreach (var subscriberId in subscribersIds)
            {
                var user = await TryGetUser(subscriberId);

                var email = user.Email;
                var lastName = user.LastName;
                var reportName = report.Name;
                var industryName = report.Industry.Name;

                await SendEmailToNotify(lastName, reportName, industryName, email);
            }
        }

        public async Task ConfirmationEmailProvider(int userId, string confirmationLink)
        {
            var user = await TryGetUser(userId);

            var email = user.Email;
            var lastName = user.LastName;

            await SendConfirmationEmail(lastName, email, confirmationLink);
        }

        public async Task SendConfirmationEmail(string lastName, string emailAddress, string confirmationLink)
        {
            try
            {
                MailMessage email = new MailMessage();

                email.To.Add(new MailAddress(emailAddress));
                email.From = new MailAddress("info.insighthub@gmail.com");
                email.Subject = "Confirm";
                email.Body = Constants.ConformationEmail(lastName, confirmationLink);
                
                await SmtpClient.SendMailAsync(email);
            }
            catch (Exception ex)
            {
                throw new ApiException(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
