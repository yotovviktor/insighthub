﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.EmailSenderService
{
    public interface IEmailSender
    {
       /// <summary>
       /// Provides the necessary infromation for Sendig a NotificationEmail.
       /// This method calls the SendEmailToNotify and it is the only secure method to call it. 
       /// </summary>
       /// <param name="subscribersIds">Ids of the subscribers</param>
       /// <param name="reportId">The id of the new report</param>
       ///<exception cref="Common.Exceptions.ApiException">When userId is out of range</exception> 
       ///<exception cref="Common.Exceptions.ApiException">When report is out of range</exception> 
        public Task NotifyEmailProvider(IEnumerable<int> subscribersIds, int reportId);

        /// <summary>
        /// Send email to notify user for a new report using SMTP. Do not call this method directly!!! 
        /// Call it via NotifyEmailProvider
        /// </summary>
        /// <param name="lastName"> last name of the user</param>
        /// <param name="reportName"> new report name</param>
        /// <param name="industryName"> industry that the report is in</param>
        /// <param name="emailAddress">the email adres of the recipient</param>
        public Task SendEmailToNotify(string lastName, string reportName, string industryName, string emailAddress);

        /// <summary>
        /// Provides the necessary information for Sendig a Confirmational Email.
        /// This method calls the SendConfirmationEmail method and it is the only secure method to call it. 
        /// </summary>
        /// <param name="userId">Ids of the subscribers</param>
        /// <param name="conformationLink">A link that has the ecrypted EmailConfirmationToken and the redirekt in it</param>
        ///<exception cref="Common.Exceptions.ApiException">When userId is out of range</exception> 
        public Task ConfirmationEmailProvider(int userId, string conformationLink);

        /// <summary>
        /// Sends email to confirm the email validity using SMTP. Please do not call this method directly!!! 
        /// Call it via ConfirmationEmailProvider 
        /// </summary>
        /// <param name="lastName">The last name of the user</param>
        /// <param name="emailAddress">The email of the recipient</param>
        /// <param name="conformationLink">A link that has the ecrypted EmailConfirmationToken and the redirekt in it</param>
        public Task SendConfirmationEmail(string lastName, string emailAddress, string conformationLink);
    }
}
