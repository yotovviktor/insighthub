﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Enums;
using InsightHub.Common.Models;
using InsightHub.Data.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InsightHub.Services.ReportServices
{
    public interface IReportService
    {
        /// <summary>
        /// Asynchronously finds the report with the given ID
        /// </summary>
        /// <returns>
        /// Returns an object of ReportDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        public Task<ReportDto> GetReportAsync(int reportId);

        /// <summary>
        /// Asynchronously creates and upload the report by given object of ReportDto class
        /// </summary>
        /// <returns>
        /// Returns an object of ReportDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a ReportDto is null with HttpStatusCode.BadRequest</exception
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.Unauthorized</exception
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a user with that ID is not approved with HttpStatusCode.Unauthorized</exception
        public Task<ReportDto> CreateReportAsync(ReportDto reportDto);

        /// <summary>
        /// Asynchronously downloads the report by given report ID and user ID
        /// </summary>
        /// <returns>
        /// Returns an object of ReportDto class.
        /// </returns>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.Unauthorized</exception
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a user with that ID is not approved with HttpStatusCode.Unauthorized</exceptionn
        public Task<ReportDto> DownloadReportAsync(int reportId, int userId);

        /// <summary>
        /// Asynchronously marks the report as downloaded by given report ID and user ID
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.Unauthorized</exception
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a user with that ID is not approved with HttpStatusCode.Unauthorized</exceptionn
        public Task MarkReportAsDownloadedAsync(int reportId, int userId);


        /// <summary>
        /// Asynchronously deletes the report with the given ID
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the report is already deleted with HttpStatusCode.NotFound</exception>
        public Task DeleteReportAsync(int reportId);

        /// <summary>
        /// Asynchronously change the status of  the report and if it is "approved", calls the SubscriptionService
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when report id is out of range with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when status is the same as old one with HttpStatusCode.Conflict</exception>
        public Task ChangeReportStatusAsync(int reportId, Status status);

        /// <summary>
        /// Asynchronously updates the report by given object of ReportDto class
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when a ReportDto is null with HttpStatusCode.BadRequest</exception
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the report is not found with HttpStatusCode.NotFound</exception>
        public Task UpdateReportAsync(ReportDto reportDto);

        /// <summary>
        /// Asynchronously retrieves all downloaded reports by user with id userID, filtered, sorted and ordered by criteria
        /// </summary>
        /// <returns>
        /// Returns a collection of objects of ReportDto class.
        /// </returns>
        public Task<ICollection<ReportDto>> ListUserDownloadsAsync(int userID, FilterCriteria filter, ReportSortBy? sortBy, Order? order, Paging paging);

        /// <summary>
        /// Asynchronously change featured status on the report by give state and report id
        /// </summary>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the report is not found with HttpStatusCode.NotFound</exception>
        /// <exception cref="Common.Exceptions.ApiException">Thrown when the report already has the same state with HttpStatusCode.Conflict</exception>
        public Task ChangeIsFeaturedAsync(int reportId, bool state);

        /// <summary>
        /// Asynchronously retrieves a collection of reports filtered, sorted and ordered by criteria
        /// </summary>
        /// <param name="filter">filter criteria - report name, indursty id, author, isFeature state, status or tags </param>
        /// <param name="sortBy">sort criteria - report name, downloads or upload date</param>
        /// <param name="order">order criteria - ascending(default) or descending</param>
        /// <returns>Returns a collection of objects of ReportDto class.</returns>
        public Task<ICollection<ReportDto>> ListReportsAsync(FilterCriteria filter, ReportSortBy? sortBy, Order? order, Paging paging);

        /// <summary>
        /// Asynchronously validates the ownership of the report by given user id and report id
        /// </summary>
        public Task<bool> IsValidOwnershipAsync(int userId, int reportId);
    }
}
