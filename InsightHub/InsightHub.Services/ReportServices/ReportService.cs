﻿using InsightHub.Common.DtoModels;
using InsightHub.Common.Exceptions;
using InsightHub.Data.Context;
using InsightHub.Data.EntityModels;
using InsightHub.Common.Enums;
using InsightHub.Common.Extensions.MappersToDto;
using InsightHub.Common.Extensions.MappersToEntity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Common.Models;
using System.Collections.Generic;
using System;
using InsightHub.Services.SubscriptionServices;
using InsightHub.Data.Enums;
using System.IO;

namespace InsightHub.Services.ReportServices
{
    /// <summary>
    /// This class is a concrete implementation of IReportService. 
    /// The class provides methods for managing reports - create, read, list, search, filter, download, update and delete.
    /// </summary>
    public class ReportService : IReportService
    {
        private readonly InsightHubContext _context;
        private readonly IContentService _azureContentService;
        private readonly ISubscriptionService _subscriptionService;

        public ReportService(InsightHubContext context, IContentService azureContentService, ISubscriptionService subscriptionService)
        {
            _context = context ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(context));
            _azureContentService = azureContentService ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(azureContentService));
            _subscriptionService = subscriptionService ?? throw new ApiException(HttpStatusCode.InternalServerError, nameof(subscriptionService));
        }

        private IQueryable<Report> ReportsQuery
        {
            get => _context.Reports
                .Where(report => report.IsDeleted == false);
        }

        private IQueryable<Report> ReportsRelationsQuery
        {
            get => ReportsQuery
                .Include(report => report.Industry)
                .Include(report => report.Tags)
                .ThenInclude(reportTag => reportTag.Tag);
        }

        public async Task<ReportDto> GetReportAsync(int reportId)
        {
            var report = await ReportsRelationsQuery
                .Include(report => report.Author)
                .FirstOrDefaultAsync(report => report.Id == reportId);

            report = report ?? throw new ApiException(HttpStatusCode.NotFound, $"There is no report with id: {reportId}");
            return report.ToDto();
        }

        public async Task<ReportDto> CreateReportAsync(ReportDto reportDto)
        {
            reportDto = reportDto ?? throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) report");

            await ValidateUserIsAuthorizedAsync(reportDto.AuthorId);

            var industry = await TryGetIndustryAsync(reportDto.IndustryId);
            var tags = await TryGetTagsAsync(reportDto.TagIds);

            ValidateReportContent(reportDto.ContentType, reportDto.Content);
            var content = new Content(reportDto.ContentType, reportDto.Content);
            var path = await _azureContentService.UploadContentAsync(content);

            var report = reportDto.ToEntity();
            report.BlobPath = path;
            report.CreatedOn = DateTime.UtcNow;
            report.Status = Status.Pending;

            report.Tags = TagReport(report.Id, tags);
            report.Industry = industry;

            await _context.Reports.AddAsync(report);
            await _context.SaveChangesAsync();

            return report.ToDto();
        }

        private async Task<ICollection<Tag>> TryGetTagsAsync(ICollection<int> tagIds)
        {
            if (tagIds == null || !tagIds.Any())
            {
                return new List<Tag>();
            }

            var fetchedTags = await _context.Tags.Where(tag => tagIds.Contains(tag.Id)).ToListAsync();
            var fetchedTagIds = fetchedTags.Select(tag => tag.Id);

            var missingTagIds = new HashSet<int>(tagIds).Except(fetchedTagIds);

            if (missingTagIds.Any())
            {
                throw new ApiException(HttpStatusCode.NotFound, $"Tag/Tags with id: {string.Join(", ", missingTagIds)} is/are not found.");
            }

            return fetchedTags;
        }

        private async Task<Industry> TryGetIndustryAsync(int industryId)
        {
            var industry = await _context.Industries.FirstOrDefaultAsync(industry => industry.Id == industryId);

            industry = industry ?? throw new ApiException(HttpStatusCode.NotFound, $"There is no industry with id: {industryId}");
           
            return industry;
        }

        private ICollection<ReportTags> TagReport(int reportId, ICollection<Tag> tags)
        {
            var reportTags = tags.Select(tag => new ReportTags { ReportId = reportId, Tag = tag, TagId = tag.Id }).ToList();

            return reportTags;
        }

        private void ValidateReportContent(string contentType, Stream content)
        {
            if (contentType == null || content == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "The content is not valid");
            }            
        }

        private async Task ValidateUserIsAuthorizedAsync(int userId)
        {
            bool isAuthorized = await _context.Users.AnyAsync(user => user.Id == userId && user.Status == Status.Approved);

            if (!isAuthorized)
            {
                throw new ApiException(HttpStatusCode.Unauthorized, "This user is not allowed on this operation");
            }
        }

        public async Task<bool> IsValidOwnershipAsync(int userId, int reportId)
        {
           bool isValidOwnership = await _context.Reports
                .AnyAsync(report => report.Id == reportId && report.AuthorId == userId);

           return isValidOwnership;
        }

        public async Task<ReportDto> DownloadReportAsync(int reportId, int userId)
        {
            var report = await TryGetReportAsync(reportId);
            await ValidateUserIsAuthorizedAsync(userId);

            var content = await _azureContentService.DownloadContentAsync(report.BlobPath);

            var reportDto = report.ToDto();
            reportDto.Content = content.Data;
            reportDto.ContentType = content.ContentType;

            return reportDto;
        }

        public async Task MarkReportAsDownloadedAsync(int reportId, int userId)
        {
            var report = await TryGetReportAsync(reportId);
            await ValidateUserIsAuthorizedAsync(userId);

            bool isDownloadedByTheUser = await _context.DownloadedReports
                                .AnyAsync(dr => dr.UserId == userId && dr.ReportId == reportId);

            if (!isDownloadedByTheUser)
            {
                var downloadedReport = new DownloadedReports
                {
                    UserId = userId,
                    ReportId = reportId
                };

                report.DownloadCount++;
                _context.Reports.Update(report);

                await _context.DownloadedReports.AddAsync(downloadedReport);
                await _context.SaveChangesAsync();
            }
        }

        private async Task<Report> TryGetReportAsync(int id, bool includeRelations = false)
        {
            var query = includeRelations ? ReportsRelationsQuery : ReportsQuery;

            var report = await query.FirstOrDefaultAsync(report => report.Id == id);
            report = report ?? throw new ApiException(HttpStatusCode.NotFound, $"There is no report with id: {id}");

            return report;
        }

        public async Task DeleteReportAsync(int reportId)
        {
            var report = await TryGetReportAsync(reportId);

            report.IsDeleted = true;
            report.DeletedOn = DateTime.UtcNow;

            _context.Reports.Update(report);
            await _context.SaveChangesAsync();
        }

        public async Task ChangeReportStatusAsync(int reportId, Status status)
        {
            var report = await TryGetReportAsync(reportId);

            if (report.Status == status)
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The report is already {status}");
            }

            report.Status = status;

            _context.Reports.Update(report);
            await _context.SaveChangesAsync();

            if (status == Status.Approved)
            {
                try
                {
                    await _subscriptionService.NotifySubscribers(report.IndustryId, report.Id);
                }
                catch (Exception)
                {
                    Console.WriteLine("Notifying subscribers failed.");
                }                
            }
        }

        public async Task UpdateReportAsync(ReportDto reportDto)
        {
            reportDto = reportDto ?? throw new ApiException(HttpStatusCode.BadRequest, "Invalid (null) report");
            var report = await TryGetReportAsync(reportDto.Id, true);
            var oldPath = report.BlobPath;

            //update report name, summary, industry and content
            await CopyChangedAttributesAsync(reportDto, report);
            report.ModifiedOn = DateTime.UtcNow;
            report.Status = Status.Pending;
            _context.Reports.Update(report);

            //update report tags
            if (reportDto.TagIds != null)
            {
                await TryGetTagsAsync(reportDto.TagIds);

                var (tagsToAdd, tagsToRemove) = FindReportTagChanges(reportDto, report);
                await _context.ReportTags.AddRangeAsync(tagsToAdd);
                _context.ReportTags.RemoveRange(tagsToRemove);
            }

            await _context.SaveChangesAsync();

            if (report.BlobPath != oldPath)
            {
                await _azureContentService.DeleteContentAsync(oldPath);
            }
        }

        private async Task CopyChangedAttributesAsync(ReportDto reportDto, Report report)
        {
            if (reportDto.Name != default & reportDto.Name != report.Name)
            {
                report.Name = reportDto.Name;
            }

            if (reportDto.Summary != default & reportDto.Summary != report.Summary)
            {
                report.Summary = reportDto.Summary;
            }

            if (reportDto.IndustryId != default & reportDto.IndustryId != report.IndustryId)
            {
                var industry = await TryGetIndustryAsync(reportDto.IndustryId);
                report.IndustryId = industry.Id;
            }

            if (reportDto.Content != null && reportDto.ContentType != null)
            {
                var content = new Content(reportDto.ContentType, reportDto.Content);
                report.BlobPath = await _azureContentService.UploadContentAsync(content);
            }
        }

        private (List<ReportTags> tagsToAdd, List<ReportTags> tagsToRemove) FindReportTagChanges(ReportDto reportDto, Report report)
        {
            var currentTagIds = new HashSet<int>(report.Tags.Select(t => t.TagId).ToList());
            var newTagIds = new HashSet<int>(reportDto.TagIds);

            var tagIdsToAdd = newTagIds.Except(currentTagIds).ToList();
            var tagsToAdd = tagIdsToAdd.Select(tagId => new ReportTags { TagId = tagId, ReportId = report.Id }).ToList();

            var tagsIdsRemove = currentTagIds.Except(newTagIds).ToList();
            var tagsToRemove = report.Tags.Where(tag => tagsIdsRemove.Contains(tag.TagId)).ToList();

            return (tagsToAdd, tagsToRemove);
        }

        public async Task<ICollection<ReportDto>> ListUserDownloadsAsync(int userId, FilterCriteria filter = null,
            ReportSortBy? sortBy = null, Order? order = null, Paging paging = null)
        {
            await ValidateUserExistAsync(userId);

            var query = ReportsRelationsQuery
                            .Include(report => report.Downloads)
                            .Where(report => report.Downloads.Any(download => download.UserId == userId));

            return await ListReportsAsync(query, filter, sortBy, order, paging);
        }

        private async Task ValidateUserExistAsync(int userId)
        {
            bool userExists = await _context.Users.AnyAsync(user => user.Id == userId);

            if (!userExists)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"There is no user with id: {userId}");
            }
        }

        public async Task ChangeIsFeaturedAsync(int reportId, bool state)
        {
            var report = await TryGetReportAsync(reportId);

            if (report.IsFeatured == state)
            {
                throw new ApiException(HttpStatusCode.Conflict, $"The report already has state featured: {state}");
            }

            report.IsFeatured = state;

            _context.Reports.Update(report);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<ReportDto>> ListReportsAsync(FilterCriteria filter = null, ReportSortBy? sortBy = null,
            Order? order = null, Paging paging = null)
        {
            var query = ReportsRelationsQuery;

            return await ListReportsAsync(query, filter, sortBy, order, paging);
        }

        private async Task<ICollection<ReportDto>> ListReportsAsync(IQueryable<Report> query, FilterCriteria filter = null,
            ReportSortBy? sortBy = null, Order? order = null, Paging paging = null)
        {
            if (filter != null)
            {
                query = FilterReports(query, filter);
            }

            query = SortReports(query, sortBy, order);

            if (paging != null)
            {
                query = PageReportsAsync(query, paging);
            }

            return (await Task.FromResult(query.ToList())).ToDtos();
        }

        private IQueryable<Report> PageReportsAsync(IQueryable<Report> query, Paging paging)
        {
            paging.TotalCount = query.Count();

            if (paging.Offset != null)
            {
                query = query.Skip(paging.Offset.Value);
            }

            if (paging.Limit != null)
            {
                query = query.Take(paging.Limit.Value);
            }

            return query;
        }

        private IQueryable<Report> SortReports(IQueryable<Report> query, ReportSortBy? sortBy, Order? order)
        {
            var sorter = GetSorter(sortBy);

            if (sortBy != null && order == null)
            {
                order = Common.Enums.Order.Asc;
            }

            if (order == Order.Asc)
            {
                return query.OrderBy(sorter).AsQueryable();
            }
            else if (order == Common.Enums.Order.Desc)
            {
                return query.OrderByDescending(sorter).AsQueryable();
            }
            else
            {
                return query;
            }
        }

        private Func<Report, object> GetSorter(ReportSortBy? sortBy)
        {
            return (report) =>
            {
                return sortBy switch
                {
                    ReportSortBy.Name => report.Name,
                    ReportSortBy.Downloads => report.DownloadCount,
                    ReportSortBy.Date => report.CreatedOn,
                    _ => report.Id,
                };
            };
        }

        private IQueryable<Report> FilterReports(IQueryable<Report> query, FilterCriteria filter)
        {
            if (!string.IsNullOrEmpty(filter.Name))
            {
                query =  query.Where(report => report.Name.ToLower().Contains(filter.Name.ToLower()));
            }

            if (filter.IndustryId != null)
            {
                query = query.Where(report => report.IndustryId == filter.IndustryId);
            }

            if (filter.AuthorId != null)
            {
                query = query.Where(report => report.AuthorId == filter.AuthorId);
            }

            if (filter.Status != null)
            {
                query = query.Where(report => report.Status == filter.Status);
            }

            if (filter.IsFeatured != null)
            {
                query = query.Where(report => report.IsFeatured == true);
            }

            if (filter.TagIds != null && filter.TagIds.Any())
            {
                query = query.Where(report => 
                    report.Tags.Where(reportTag => filter.TagIds.Contains(reportTag.TagId)).Count() == filter.TagIds.Length);
            }

            return query;
        }
    }
}
