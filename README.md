![](/Assets/Images/Intro.gif)
---

# InsightHub
> InsightHub is a Reports Portal web application.
>Allows businesses to access reports and insights in major trends. Possible actions are publishing numerous reports, browsing reports based on an industry or tags, downloading reports and subscribing for email notifications when new reports are available.
---
![](/Assets/Images/ProjectIntro.gif)

---

## Table of Contents

- [Installation](#installation)
- [Software Stack](#software-stack)
- [Features](#features)
- [Contributing](#contributing)
- [Team](#team)
- [License](#license)

## Installation

### Prerequisites
The following list of software should be installed:
- [SQL Server 2017](https://www.microsoft.com/en-us/download/details.aspx?id=55994)
- [.NET Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- [Node.js](https://nodejs.org/en/download/)

### Clone
- Clone or download the project to your local machine using `https://gitlab.com/yotovviktor/insighthub.git`
- All the `code` required to get started

### Setup

#### Setup REST API
- Navigate to the InsightHub folder (e.g. cd /insighthub/InsightHub)
> restore the project and install its dependencies
```Dos
dotnet restore
```
- Database
> updates the database to the last migration
```Dos
dotnet tool install --global dotnet-ef
dotnet ef database update --project InsightHub.Web
```

- Set environment variables:
> set the environment variable for the Azure blob storage connection string
```Dos
setx ConnectionStrings__AzureBlobStorageConnection "<yourconnectionstring>"
```
> set the environment variables for the SMTP server
```Dos
setx Smtp__Host "<yourhost>"
setx Smtp__Port "<port>"
setx Smtp__Username "<youremail>"
setx Smtp__Password "<yourpassword>"
```
> open **new** terminal and run the project
```Dos
dotnet run --project InsightHub.Web
```

#### Setup SPA
- Navigate to the ClientApp folder (e.g. cd InsightHub/InsightHub.Web/ClientApp)
```Dos
npm install
npm run start
```
- Open http://localhost:5000 in your browser

## Technologies

- ASP.NET Core
- ASP.NET Identity
- Entity Framework Core
- MS SQL Server
- Angular
- Angular Material
- HTML
- CSS
- Bootstrap

## Features

#### Authors see all reports that are owned and can publish numerous reports

![](/Assets/Images/Author.gif)
---
<br/><br/>

#### Customers can browse reports based on an industry, download them and subscribe for email notifications when reports are available

![](/Assets/Images/Customer.gif)
---
<br/><br/>

#### Admins are able to manage user accounts and reports

![](/Assets/Images/Admin.gif)
---
<br/><br/>

## Documentation

- [InsightHub API Documentation](https://app.swaggerhub.com/apis-docs/insighthub/insight-hub_api/v1)


## Contributing
InsightHub is an open source project and we are very happy to accept community contributions. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## Team
* Viktor Yotov - [GitLab](https://gitlab.com/yotovviktor)
* Greta Pavlova - [GitLab](https://gitlab.com/greta.pavlova)

## License
[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)
See the License file for licensing information as it pertains to
files in this repository
- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2020 InsightHub
